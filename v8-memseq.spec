-----------------------------------------------------------------------
-- This is an extension of:					     --	
-- 								     --
-- Formal Specification of the ARMv8-A instruction set architecture  --
-- (c) Anthony Fox, University of Cambridge                          --
-- 								     --
-- for introducing system level functionality 			     --
-- and a memory system interface				     --
-- Author: Christoph Baumann, KTH CSC Stockholm			     --
-----------------------------------------------------------------------


-- Notes:
-- 2014-11-05	moved to separate file
-- 2014-12-11   new memory interface functions

{-

val () = Runtime.LoadF "v8-base.spec, v8-id.spec, v8-mem_types.spec, v8-mem.spec";

-}


--------------------------------------------------------------
-- Core -> Memory interface for reads, writes, and address translation
--------------------------------------------------------------

-- we use virtual memory here
declare MEM :: VA -> byte 

-- hook for simple address translation function, address option (if None -> translation fault), PXN:XN:R:W:U
-- discard this idea, sequential model for virtual memory
-- assumptions: no aliasing, all used addresses mapped, no permission faults, no data aborts in general
-- declare MEM_TransAddr :: VA -> ADR option * bits(5)

--exception PERMISSION_FAULT
-- exception TRANSLATION_FAULT

-- assumed to be aligned
{-
bool list MemSingle (address::VA, size::nat, acctype::AccType, data :: (bool list) option) = 
      -- TODO: ClearExclusive, p. 5145

      write = IsSome(data);

      pao, perm = MEM_TransAddr(address);
      var pa;
      
      if IsSome(pa) then pa <- ValOf(pao) else #TRANSLATION_FAULT;
      
      var value = Nil :: bool list;

      when acctype == AccType_IFETCH and (if C.PSTATE.EL == '01' then !perm<4> else !perm<3>) do #PERMISSION_FAULT;
      
      if write then {
	  when !perm<1> or C.PSTATE.EL == 0 and !perm<0> do #PERMISSION_FAULT;
	  d = BER(ValOf(data));    
	  when Length(d) < size*8 do #ASSERT("Value for write too short!");    
	  for i in size - 1 .. 0 do MEM(pa + [i]) <- [Take(8,d)]
      }
      else {
	  when !perm<2> or C.PSTATE.EL == 0 and !perm<0> do #PERMISSION_FAULT;
	  for i in size - 1 .. 0 do value <- value : [MEM(pa + [i])]
      };

      return BER(value)
}
-}

bool list MemSingle (va::VA, size::nat, acctype::AccType, data :: (bool list) option) = 
{
    -- TODO: ClearExclusive, p. 5145

      write = IsSome(data);

      var value = Nil :: bool list;

      if write then {
	  d = BER(ValOf(data));    
	  when Length(d) < size*8 do #ASSERT("Value for write too short!");    
	  for i in size - 1 .. 0 do MEM(va + [i]) <- [Take(8,d)]
      }
      else {
	  for i in size - 1 .. 0 do value <- value : [MEM(va + [i])]
      };

      return BER(value)
}



(bool list) handle_mem_access (address :: VA, size :: nat, acctype :: AccType, data :: (bool list) option) = 
{
    write = IsSome(data);
    C.CURR_FAULT <- NoFault;
    aligned = CheckAlignment (address, size, acctype, write);
    
    var value :: bool list;
       
    when C.CURR_FAULT.typ == Fault_None do value <- MemSingle(address, size, acctype, data);

    return value
}

unit drop_result(data :: bool list) = nothing

component Mem (address::dword, size::nat, acctype::AccType) :: bool list  
{
    value = handle_mem_access(address, size, acctype, None)
    assign value = drop_result(handle_mem_access(address, size, acctype, Some(value)))
}
	
unit MemBarrier (op :: MemBarrierOp, domain :: MBReqDomain, t :: MBReqTypes) = nothing

-- caches and tlb invisible in sequential model assuming that we do not use uncacheable aliases / update our own address translation
unit handle_cachetlb(req :: MEM_REQUEST) = nothing


-- AT not modeled for own translation at the moment, havoc PAR_EL1 for all other cases
unit handle_at(req :: MEM_REQUEST) =
{
    match req.sopar {
	case ATrans(_, s2, el , _) => if !s2 and el == req.EL then #NOT_MODELED( "AT for own translation schemes" )
	                              else C.&PAR_EL1 <- UNKNOWN
	case _ => #SYSOP_SCHEDULE_FAULT
    }     
}

unit handle_sysop(sys_op1::bits(3), sys_op2::bits(3), sys_crn::bits(4), sys_crm::bits(4), has_result::bool, t::reg) =
{
    req = decode_sysop(sys_op1,sys_op2,sys_crn,sys_crm,has_result,t);
    trp = trap_sysop(req.sopar);
    when trp > 0 do {if trp == 4 then #TRAP_TO_EL2 else #UNDEFINED_FAULT ("InsufficientPrivileges")};
    
    match req.acctype {
	case AccType_DC or AccType_IC or AccType_TLB => handle_cachetlb(req)
	case AccType_AT => handle_at(req)
	case _ => #SYSOP_SCHEDULE_FAULT
    }	  
}	 



-- TODO: PC wrap around issue 
-- TODO: sequential semantics for DC, AT, TLBI, barriers (basic transation and noop)

