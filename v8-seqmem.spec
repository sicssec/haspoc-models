-----------------------------------------------------------
-- ARMv8 minimal sequential memory model                 --
-----------------------------------------------------------

-- Runtime.LoadF "v8-base_types.spec, v8-mem_types.spec, v8-seqmem.spec";

construct MEM_STATE { mem_wait mem_serve }
construct MEM_RCVR {rcvr_mmu, rcvr_core}

record MEM_CONFIG {
       curr :: MEM_REQUEST
       rcvr :: MEM_RCVR
       S :: MEM_STATE
       W :: bits(45) list
}

type MEM_ABS = ADR -> bits(8)
type MEM_TZC = bits(36) -> TrustZonePermission

-- need large maps as top level objects to speed up simulation
declare MC :: MEM_CONFIG
declare PM :: MEM_ABS
declare TZC :: MEM_TZC

construct MEM_FAULT {sync_parity, sync_external, async_parity, async_external, nofault}

-- "local" oracle returns input request for current request and state (defined in HOL/SML using input channel representation)
-- cannot depend on non-equality types, e.g., sets and maps
type MEM_ORACLE = (MEM_REQUEST * MEM_STATE) -> (MEM_REQUEST option * MEM_FAULT)

unit rcv(q :: MEM_REQUEST) = {
     MC.curr <- q;
     MC.rcvr <- if q.acctype == AccType_PTW then rcvr_mmu else rcvr_core;
     MC.S <- mem_serve
}

unit write_mem(size :: bits(N), ad :: ADR, data :: bool list) with N in 8, 16, 32, 64, 128 = 
{
    n = N div 8;
    var d = data;

    for i in n-1 .. 0 do {
	PM(ad+[i]) <- [Take(8,d)];
	d <- Drop(8,d)
    };
    
    -- update write list, 64-bit granularity
    MC.W <- ad<47:3> @ MC.W;
    when N > 64 do MC.W <- (ad<47:3> + 1) @ MC.W
}

bool list read_mem(size :: bits(N), ad :: ADR) with N in 8, 16, 32, 64, 128 = 
{
    n = N div 8;
    var d = Nil :: bool list;

    -- convert to little endian bool list
    for i in 0 .. n-1 do d <- [PM(ad+[i])] : d;

    return d
}

bool TZ_check(q :: MEM_REQUEST) =
{
    p = TZC(q.desc.paddress<47:12>);

    allowed = match q.ns, q.write
    {
	case true,  false => p.SR
	case true,  true  => p.SW
	case false, false => p.NSR
	case false, true  => p.NSW
    };

    return allowed
}


MEM_REQUEST reply(fault :: MEM_FAULT) = {
     var q = MC.curr;
     
     adr = q.desc.paddress;
     
     if q.write then match q.bytesize {
	 case 1 => write_mem(0`8, [adr], q.data) --`
	 case 2 => write_mem(0`16, [adr], q.data) --`
	 case 4 => write_mem(0`32, [adr], q.data) --`
	 case 8 => write_mem(0`64, [adr], q.data) --`
	 case _ => write_mem(0`128, [adr], q.data) --`		 
     }	
     else match q.bytesize {
	 case 1 => q.data <- read_mem(0`8, [adr]) --`
	 case 2 => q.data <- read_mem(0`16, [adr]) --`
	 case 4 => q.data <- read_mem(0`32, [adr]) --`
	 case 8 => q.data <- read_mem(0`64, [adr]) --`
	 case _ => q.data <- read_mem(0`128, [adr]) --`
     };

     -- add TrustZone check, overruns other bus faults
     f = if !TZ_check(q) then sync_external else fault;
	      
     q.desc.fault.typ <- match f {
	 case sync_parity    => if q.acctype == AccType_PTW then Fault_SyncParityOnWalk else Fault_SyncParity
	 case sync_external  => if q.acctype == AccType_PTW then Fault_SyncExternalOnWalk else Fault_SyncExternal
 	 case async_parity   => Fault_AsyncParity
 	 case async_external => Fault_AsyncExternal
	 case nofault        => Fault_None
     };

     MC.S <- mem_wait;

     return q
}
     

-- internal scheduler returns new output action, triggers automaton step
-- S=1 -> receive
-- S=2 -> reply
(MEM_REQUEST option) * bool mem_sched (req :: (MEM_REQUEST option), fault :: MEM_FAULT) =
{
	var out = None;
	var consumed = false;
	when !(req == None and MC.S == mem_wait) do
	{
		match MC.S
		{
			case mem_wait  => { rcv(ValOf(req)); consumed <- true } 
			case mem_serve => out <- Some(reply(fault))
		}
	};


	return (out, consumed)
}


-- memory interface uses oracle to determine input, returns consumed input and new output
-- this function is called from HOL as an instantiation of a generic local scheduling function (with polymorphic types for oracle and return types
(MEM_REQUEST option) * (MEM_REQUEST option) * MEM_RCVR mem_interface (o :: MEM_ORACLE) =
{
	oracle_in, oracle_fault = o(MC.curr, MC.S);

	var input = oracle_in;

	(out, consumed) = mem_sched(input, oracle_fault);

	when !consumed do input <- None;	

	return (input, out, MC.rcvr)
}


-- TODO: add core_id for multicore interface, to config and interface


-------------------------
-- write list for output
-------------------------

(ADR * dword) list write_list (mc :: MEM_CONFIG, m :: ADR -> bits(8)) = 
{
    var w = mc.W;
 
    var written :: bits(45) -> bool;

    var res = Nil;

    when Length(w) > 0 do {
    for i in 1 .. Length(w) do  
	when !written(Head(w)) do {
	    written(Head(w)) <- true;
	    ad = Head(w):'000';
	    res <- (ad, m(ad+7):m(ad+6):m(ad+5):m(ad+4):m(ad+3):m(ad+2):m(ad+1):m(ad)) @ res;
	    w <- Tail(w)
	}
    };

    return res
}
