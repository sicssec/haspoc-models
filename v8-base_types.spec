-----------------------------------------------------------------------
-- This is an extension of:					     --	
-- 								     --
-- Formal Specification of the ARMv8-A instruction set architecture  --
-- (c) Anthony Fox, University of Cambridge                          --
-- 								     --
-- with system level functionality.				     --
-- Author: Christoph Baumann, KTH CSC Stockholm			     --
-----------------------------------------------------------------------


-- Notes:
-- 2015-01-16	refactoring, move type definitions here

{-

val () = Runtime.LoadF "v8-base_types.spec";

-}

-----------------------------------
-- Word sizes (64-bit architecture)
-----------------------------------

type cond = bits(4)
type reg  = bits(5)
type byte = bits(8)
type half = bits(16)
type word = bits(32)
type dword = bits(64)
type qword = bits(128)

-------------------
-- Model exceptions
-------------------

exception ASSERT :: string
exception UNDEFINED_FAULT :: string
exception NOT_MODELED :: string
exception ALIGNMENT_FAULT
exception SYNCHRONOUS_EXTERNAL_ABORT
exception UNPREDICTABLE
exception TRAP_TO_EL1
exception TRAP_TO_EL2

-----------------------------------------------------------------------
-- Implementation dependant switches ----------------------------------
-----------------------------------------------------------------------

record CORE_IMPL_SWITCHES {
-- cause unpredictable model exception if SBZ values are not zero
IMPL_SBZ_NOT_ZERO_UNPREDICTABLE :: bool
}


---------------------------------------
-- Processor state and system registers
---------------------------------------

record ProcState
{
   N   :: bool    -- Negative condition flag
   Z   :: bool    -- Zero condition flag
   C   :: bool    -- Carry condition flag
   V   :: bool    -- oVerflow condition flag
   D   :: bool    -- Debug mask bit
   A   :: bool    -- Asynchronous abort mask bit
   I   :: bool    -- IRQ mask bit
   F   :: bool    -- FIQ mask bit
   EL  :: bits(2) -- exception level
   SPS :: bool    -- Stack pointer select
}
-- TODO: SS, IL?, nRW

construct CORE_STATE { core_issue, core_fetch, core_dec, core_mem }

register TCR_EL1 :: dword
{
   38 : TBI1	-- top-byte ignored (for address calculation, used as tag) for upper VA region
   37 : TBI0	-- top-byte ignored for lower VA region
   36 : AS	-- ASID size, upper 8 (AS=0) or 16 (AS=1) bits of TTBRx_EL1 not used for ASID matching in TLB
34-32 : IPS	-- defines output size of translation, i.e., size of physical memory (000: 32 bits - 101: 48 bits)
31-30 : TG1	-- granule size for upper VA translation (01: 16 KB, 10: 4KB, 11: 64KB)
29-28 : SH1	-- shareability of upper region page tables (00: non, 10: outer, 11: inner)
27-26 : ORGN1	-- outer cacheability of upper region page tables (00: non, 01: WB WA, 10: WT, 11: WB nWA)
25-24 : IRGN1	-- inner cacheability of upper region page tables (00: non, 01: WB WA, 10: WT, 11: WB nWA)
   23 : EPD1	-- disable walking for of upper region virtual addresses
   22 : A1	-- TTBR(A1)_EL1 defines the ASID
21-16 : T1SZ	-- number of leading zeros for virtual addresses (min 16, max 39, depends on transl. scheme), defines size of upper virtual memory
15-14 : TG0	-- granule size for lower VA translation (00: 4KB, 01: 16 KB, 11: 64KB) !!!different values than for upper VA
13-12 : SH0	-- shareability of lower region page tables (00: non, 10: outer, 11: inner)
11-10 : ORGN0	-- outer cacheability of lower region page tables (00: non, 01: WB WA, 10: WT, 11: WB nWA)
  9-8 : IRGN0	-- inner cacheability of lower region page tables (00: non, 01: WB WA, 10: WT, 11: WB nWA)
    7 : EPD0	-- disable walking for of lower region virtual addresses
  5-0 : T0SZ	-- number of leading zeros for virtual addresses (min 16, max 39, depends on transl. scheme), defines size of lower virtual memory
}

register TCR_EL2_EL3 :: word
{
   20 : TBI	-- top-byte ignored in address calculation, used as tag, tags cleared to zero if PC is written
18-16 : PS	-- physical address size (000: 32 bits - 101: 48 bits)
15-14 : TG0	-- granule size for translation (00: 4KB, 01: 16 KB, 11: 64KB)
13-12 : SH0	-- shareability of lower region page tables (00: non, 10: outer, 11: inner)
11-10 : ORGN0	-- outer cacheability of lower region page tables (00: non, 01: WB WA, 10: WT, 11: WB nWA)
  9-8 : IRGN0	-- inner cacheability of lower region page tables (00: non, 01: WB WA, 10: WT, 11: WB nWA)
    7 : EPD0	-- disable walking for of lower region virtual addresses
  5-0 : T0SZ	-- number of leading zeros for virtual addresses (min 16, max 39, depends on transl. scheme), defines size of lower virtual memory
}

register SCTLRType :: word
{
   26 : UCI     -- EL0 cache (clear and) invalidate instructions enabled
   25 : EE      -- Exception endianness (also for address translation)
   24 : E0E     -- Endianness of explicit data access (1 - big-endian)
   19 : WXN     -- force writable memory regions to XN (execute never), may be cached in TLB
   18 : nTWE    -- not trap WFE (1 - WFE executed normally)
   16 : nTWI    -- not trap WFI (1 - WFI executed normally)
   15 : UCT     -- EL0 access to CTR_EL0
   14 : DZE     -- allow DC ZVA instruction at EL0
   12 : I       -- enable instruction cache at EL0 and EL1
    9 : UMA     -- enale user access to interrupt masks
    8 : SED     -- disable SETEND instruction (set endianness) (1 if no AArch32)
--  7 : ITD     -- disable IT (Thumb If-then) instruction 
--  5 : CP15BEN -- enable memory barrier instructions for AArch32
    4 : SA0     -- Stack alignment check enabled for EL0
    3 : SA      -- Stack alignment check enable
    2 : C       -- Enable data caches at EL0, EL1
    1 : A       -- Aligment check enable
    0 : M	-- Enable address translation
}


register TTBRType :: dword
{
63-48 : ASID    -- Address space identifier
47-0  : BADDR   -- Translation table base address, must be aligned to first level PT size, also upper bits must be padded with zero of physical addresses are less than 48 bits long, else address size fault
}

nat TR(EL :: bits(2)) = if EL <> 0 then [EL] else 1

register PARType :: dword
{
    63-56 : ATTR     -- memory attributes for returned translation
    47-12 : PA       -- physical address for returned translation
       9  : NSST     -- not secure bit for returned translation / stage of returned fault
       8  : SH1PTW   -- upper shareability bit for returned translation / page table walk indicator for returned fault
       7  : SH0      -- lower shareability bit for returned translation
     6-1  : FST      -- fault status code for returned fault
       0  : F        -- fault bit
}

register SCRType :: word
{
       13 : TWE      -- trap WFE instructions at EL2, EL1, EL0
       12 : TWI      -- trap WFI instructions at EL2, EL1, EL0
       11 : ST       -- trap Secure EL1 accesses to secure timer registers (0 = trap)
       10 : RW       -- execution state of lower levels (0 : all AArch32, 1: next lower AArch64)
        9 : SIF      -- Secure instruction fetch (1: only fetch from secure memory)
        8 : HCE      -- hypercall enable (0: disable HVC instruction at all levels)
	7 : SMD      -- disable SMC instruction at all levels and security states
        3 : EA       -- reroute SErrors and external aborts to EL3
	2 : FIQ      -- reroute FIQs to EL3
	1 : IRQ      -- reroute IRQs to EL3
	0 : NS       -- Non-secure bit (0: EL0 and EL1 are running in secure mode)
}

--------------------------
-- Virtualiation registers
--------------------------

register VTCR_EL2 :: word
{
18-16 : PS	-- physical address size (000: 32 bits - 101: 48 bits)
15-14 : TG0	-- granule size for translation (00: 4KB, 01: 64 KB, 10: 16KB)
13-12 : SH0	-- shareability of lower region page tables (00: non, 10: outer, 11: inner)
11-10 : ORGN0	-- outer cacheability of lower region page tables (00: non, 01: WB WA, 10: WT, 11: WB nWA)
  9-8 : IRGN0	-- inner cacheability of lower region page tables (00: non, 01: WB WA, 10: WT, 11: WB nWA)
  7-6 : SL0	-- starting level of Stage 2 page table walk, depending on TG0, skip 2-SL0 levels
  5-0 : T0SZ	-- number of leading zeros for virtual addresses, size offset for region addressed by VTTBR_EL2, TODO: check if different from TCR
}


register HCR_EL2 :: dword
{
   33 : ID      -- Stage 2 instruction cache disable (for EL1 and EL0)
   32 : CD      -- Stage 2 data cache disable (for EL1 and EL0)
   31 : RW      -- Execution state for EL1 and EL0 (0 - both AArch32, 1 - EL1 is AArch64 / EL0 defined by guest)
   30 : TRVM    -- Trap read of virtual memory registers in EL1, i.e., [SCTLR|TTBR0|TTBR1|TCR|ESR|FAR|AFSR0|AFSR1|MAIR|AMAIR|CONTEXTIDR]_EL1
   29 : HCD     -- Disable HVC (hypercall) at EL1, EL2
   28 : TDZ     -- Trap DZ ZVA in guest mode
   27 : TGE     -- Trap General Exceptions
   26 : TVM     -- Trap write of virtual memory registers in EL1, i.e., [SCTLR|TTBR0|TTBR1|TCR|ESR|FAR|AFSR0|AFSR1|MAIR|AMAIR|CONTEXTIDR]_EL1
   25 : TTLB    -- trap TLBI instructions in guest mode
   24 : TPU     -- trap cache "to PoU" instructions in guest mode, i.e., [IC IVAU, IC IALLU, IC IALLUIS, DC CVAU]
   23 : TPC     -- trap data cache "to PoC" instructions in guest mode, i.e., [DC IVAC, DC CIVAC, DC CVAC]
   22 : TSW     -- trap data cache "by Set/Way" instructions in EL1, i.e., [DC ISW, DC CISW, DC CSW]
   21 : TACR    -- trap EL1 accesses to ACTLR_EL1
   20 : TIDCP   -- trap EL1 accesses to implementation dependent coprocessor registers
   19 : TSC     -- trap EL1 accesses to SMC
   18 : TID3    -- trap EL1 accesses to registers from ID group 3
   17 : TID2    -- trap EL1 accesses to registers from ID group 2
   16 : TID1    -- trap EL1 accesses to registers from ID group 1
   15 : TID0    -- trap EL1, EL0 accesses to registers from ID group 0 (none in AArch64)
   14 : TWE     -- trap WFE if execution would be suspended on EL1, EL0
   13 : TWI     -- trap WFI if execution would be suspended on EL1, EL0
   12 : DC      -- default cachable
11-10 : BSU     -- minimum sharaeability domain for guest barriers (00 - none, 01 - inner, 10 - outer, 11 - full)
    9 : FB      -- force broadcast of certain TLBI instructions and IC IALLU on EL1 within Inner shareability domain 
    8 : VSE     -- Virtual System Error/Asynchronous Abort pending (requires AMO)
    7 : VI      -- Virtual IRQ Interrupt pending (requires IMO)
    6 : FI      -- Virtual FIQ Interrupt pending (requires FMO)
    5 : AMO     -- Asynchronous Eternal Abort and SError Interrupt Routing
    4 : IMO     -- Physical IRQ routing
    3 : FMO     -- Physical FIQ routing
    2 : PTW     -- Protect Table Walks, fault for Stage 1 walks on Stage 2 Device memory
    1 : SWIO    -- turn DC ISW in EL1 into DC CISW (redundant in ARMv8???)
    0 : VM	-- Enable Stage 2 address translation
}

-------------------------
-- ID registers
-------------------------

register proc_feat :: dword
{
27-24 : GIC     -- GIC CPU interface support (0000: off, 0001: on)
23-20 : SIMD    -- Advanced SIMD support (0000: on, 1111: off)
19-16 : FP      -- Floating Point support (0000: on, 1111: off)
15-12 : EL3     -- EL3 exception level (0000: off, 0001: on for AArch64, 0010: on for AArch32/64)
 11-8 : EL2     -- EL2 exception level (0000: off, 0001: on for AArch64, 0010: on for AArch32/64)
  7-4 : EL1     -- EL1 exception level (0000: off, 0001: on for AArch64, 0010: on for AArch32/64)
  3-0 : EL0     -- EL0 exception level (0000: off, 0001: on for AArch64, 0010: on for AArch32/64)
}

register mm_feat :: dword
{
31-28 : TGRAN4	-- 4KB granule size support (0000: on, 1111: off)
27-24 : TGRAN64	-- 64KB granule size support (0000: on, 1111: off)
23-20 : TGRAN16	-- 16KB granule size support (0000: on, 1111: off)
19-16 : BEEL0   -- mixed endian support for EL0, SCTLR_EL1.E0E usable (0000: off, 0001: on)
15-12 : SNSM    -- Secure / Not Secure Memory (0000: off, 0001: on)
 11-8 : BE      -- mixed endian support for ELx, x>0, SCTLR_ELx.EE usable (0000: off, 0001: on)
  7-4 : ASIDB   -- number of ASID bits (0000: 8, 0001: 16)
  3-0 : PAR     -- implemented physical address length (0000: 32, 0001: 36, 0010: 40, 0011: 42, 0100: 44, 0101: 48)
}

register isa_feat :: dword
{
19-16 : CRC32   -- CRC32 instructions implemented (0000: off, 0001: on)
15-12 : SHA2    -- SHA2 instructions implemented (0000: off, 0001: on)
 11-8 : SHA1    -- SHA1 instructions implemented (0000: off, 0001: on)
  7-4 : AES     -- AES instructions implemented (0000: off, 0001: on, 0010: on + PMULL and PMULL2 for 64-bit operands)
}

bool TGImplemented(id :: mm_feat, tg :: bits(2) ) = 
{
    match tg 
    {
	case '00' => (id.TGRAN4 == '0000')
	case '01' => (id.TGRAN64 == '0000')
	case '10' => (id.TGRAN16 == '0000')
	case '11' => false
    }
}


register MIDRType :: word
{
    31-24 : IMPL     -- implementer ID
    23-20 : VAR      -- variant number
    19-16 : ARCH     -- architecture ID
    15-4  : PART     -- part number
     3-0  : REV      -- revision number
}


register MPIDRType :: dword
{
    39-32 : AFF3     -- Affinity level 3 field 
       30 : U        -- uniprocessor bit
    23-16 : AFF2     -- Affinity level 2 field 
    15-8  : AFF1     -- Affinity level 1 field 
     7-0  : AFF0     -- Affinity level 0 field
}

-- TODO: how to handle debug and encryption instructions? make sure they do not break security, dummy user instruction?
-- TODO: give default configuration for MM, P and ISA FRs, for Cortex A57?

--------------
-- Interrupts
--------------

register VBARType :: dword
{
    63-11 : VBA   -- vector base address for interrupts (should be 0 for 55:48 and also 63:56 if tagged addresses not used)
}


construct BranchType
{
   BranchType_CALL    BranchType_ERET BranchType_DBGEXIT
   BranchType_RET     BranchType_JMP  BranchType_EXCEPTION
   BranchType_UNKNOWN
}
