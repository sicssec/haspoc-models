-----------------------------------------
-- Testbed for the ARMv8 systems model --
-- (C) 2015 Christoph Baumann, KTH CSC --
-----------------------------------------

{-

val () = Runtime.LoadF "../v8-base_types.spec, ../v8-mem_types.spec, ../v8-base.spec, ../v8-mem.spec, ../v8-mmu_types.spec, ../v8-mmu.spec, ../v8-virt.spec, ../v8-iset.spec, ../v8-seqmem.spec,  ../v8-core.spec, ../v8-testbed.spec";

-}

---------------------------
-- setting system registers
---------------------------

bits(4) encode_PS(PAR :: bits(N)) with N in 32, 36, 40, 42, 44, 48 = match N {
	case 32 => '0000'
	case 36 => '0001'
	case 40 => '0010'
	case 42 => '0011'
	case 44 => '0100'
	case 48 => '0101'
	case _  => UNKNOWN`4 --`
    }
 

unit set_memory_features(support4K :: bool, support16K :: bool, support64K :: bool, supportBE0 :: bool,  supportBE :: bool, supportSM :: bool, ASID16 :: bool, PAR :: bits(N)) with N in 32, 36, 40, 42, 44, 48 = 
{
    -- support for different translation granularities
    C.ID_AA64MMFR0_EL1.TGRAN4  <- if support4K  then '0000' else '1111';
    C.ID_AA64MMFR0_EL1.TGRAN16 <- if support16K then '0000' else '1111';
    C.ID_AA64MMFR0_EL1.TGRAN64 <- if support64K then '0000' else '1111';
    -- mixed endian support
    C.ID_AA64MMFR0_EL1.BE      <- if supportBE  then '0001' else '0000'; 
    C.ID_AA64MMFR0_EL1.BEEL0   <- if !supportBE and supportBE0 then '0001' else '0000';     
    -- Secure Memory support
    C.ID_AA64MMFR0_EL1.SNSM    <- if supportSM  then '0001' else '0000';
    -- number of ASID bits, either 16 or 8
    C.ID_AA64MMFR0_EL1.ASIDB   <- if ASID16     then '0001' else '0000';     
    -- physical address range
    C.ID_AA64MMFR0_EL1.PAR      <- encode_PS(PAR)
}

unit set_input_address_size_ST1(el :: nat, upper :: bool, n :: nat) = {
    TSZ = [64-n]`6; --`
    match el {
	case 3 => C.TCR_EL3.T0SZ <- TSZ
	case 2 => C.TCR_EL2.T0SZ <- TSZ
	case _ => if upper then C.TCR_EL1.T1SZ <- TSZ else C.TCR_EL1.T0SZ <- TSZ
    }
}


unit set_input_address_size_ST2(n :: nat) = {
    TSZ = [64-n]`6; --`
    C.VTCR_EL2.T0SZ <- TSZ
}


unit skip_levels_ST2(l :: nat) = 
{
    sl0 = match l {
	case 0 => 2
	case 1 => 1
	case _ => 0
    };
    C.VTCR_EL2.SL0 <- sl0
}

unit set_output_address_size_ST1(el :: nat, size :: bits(N)) with N in 32, 36, 40, 42, 44, 48 =  
{
    PS = encode_PS(size)<2:0>;
    match el {
	case 3 => C.TCR_EL3.PS <- PS
	case 2 => C.TCR_EL2.PS <- PS
	case _ => C.TCR_EL1.IPS <- PS
    }
}

unit set_output_address_size_ST2(size :: bits(N)) with N in 32, 36, 40, 42, 44, 48 =  
{
    C.VTCR_EL2.PS <- encode_PS(size)<2:0>
}


unit set_granule_size_ST1 (el :: nat, upper :: bool, size :: bits(N)) with N in 4, 16, 64 =
{
    if (el == 1 and upper) then C.TCR_EL1.TG1 <- match N {
	    case 4  => '10'
	    case 16 => '01'
	    case 64 => '11'
	    case _  => UNKNOWN`2 --`
	}
    else {
	tg = match N {
	    case 4  => '00'
	    case 16 => '10'
	    case 64 => '01'
	    case _  => UNKNOWN`2 --`
	};
	match el {
	    case 3 => C.TCR_EL3.TG0 <- tg
	    case 2 => C.TCR_EL2.TG0 <- tg
	    case _ => C.TCR_EL1.TG0 <- tg
	}
    }
}

unit set_granule_size_ST2 (size :: bits(N)) with N in 4, 16, 64 = 
{
    	C.VTCR_EL2.TG0 <- match N {
	    case 4  => '00'
	    case 16 => '10'
	    case 64 => '01'
	    case _  => UNKNOWN`2 --`
	}
}

construct PTCacheType { NonCacheable WriteBack WriteThrough }

unit set_PT_cache_type_ST1 (el :: nat, upper :: bool, outer :: bool, typ :: PTCacheType, WA :: bool) = 
{
    rgn = match typ, WA {
	case NonCacheable, _  => '00' 
	case WriteThrough, _  => '10' 
	case WriteBack, true  => '01' -- write allocate 
	case WriteBack, false => '11' -- write no allocate
    };
    match el, upper, outer {
	case 1 , false , false => C.TCR_EL1.IRGN0 <- rgn
	case 1 , false , true  => C.TCR_EL1.ORGN0 <- rgn
	case 1 , true  , false => C.TCR_EL1.IRGN1 <- rgn	    
	case 1 , true  , true  => C.TCR_EL1.ORGN1 <- rgn
	case 2 , _     , false => C.TCR_EL2.IRGN0 <- rgn
	case 2 , _     , true  => C.TCR_EL2.ORGN0 <- rgn
	case _ , _     , false => C.TCR_EL3.IRGN0 <- rgn
	case _ , _     , true  => C.TCR_EL3.ORGN0 <- rgn
    }
} 

unit set_PT_cache_type_ST2 (outer :: bool, typ :: PTCacheType, WA :: bool) = 
{
    rgn = match typ, WA {
	case NonCacheable, _  => '00' 
	case WriteThrough, _  => '10' 
	case WriteBack, true  => '01' -- write allocate 
	case WriteBack, false => '11' -- write no allocate 
    };
    if outer then C.VTCR_EL2.ORGN0 <- rgn
    else C.VTCR_EL2.IRGN0 <- rgn
} 

construct PTShareability { NonShareable InnerShareable OuterShareable }

unit set_PT_shareability_ST1 (el :: nat, upper :: bool, typ :: PTShareability) = 
{
    sh = match typ {
	case NonShareable => '00' 
	case InnerShareable => '11' 
	case OuterShareable => '10' 
    };
    match el, upper {
	case 1 , false => C.TCR_EL1.SH0 <- sh
	case 1 , true  => C.TCR_EL1.SH1 <- sh	  
	case 2 , _     => C.TCR_EL2.SH0 <- sh
	case _ , _     => C.TCR_EL3.SH0 <- sh
    }
} 

unit set_PT_shareability_ST2 (typ :: PTShareability) = 
{
    C.VTCR_EL2.SH0 <- match typ {
	case NonShareable => '00' 
	case InnerShareable => '11' 
	case OuterShareable => '10'
    }
} 

unit set_MAIR_device (el :: nat, i :: nat, typ :: DeviceType) = 
{
    var x = 0`8; --`
    x<3:2> <- match typ {
	case DeviceType_nGnRnE => '00'
	case DeviceType_nGnRE => '01'
	case DeviceType_nGRE => '10'
	case DeviceType_GRE => '11'
    };
    match el {
	case 1 => C.MAIR_EL1<8*i+7:8*i> <- x
	case 2 => C.MAIR_EL2<8*i+7:8*i> <- x
	case _ => C.MAIR_EL3<8*i+7:8*i> <- x
    }
}

exception BAD_MAIR_SETUP

unit set_MAIR_normal (el :: nat, i :: nat, outer :: bool, CA :: bits(2), RW :: bits(2), trans :: bool) = 
{
    var x = 0`4; --`
    if CA == MemAttr_NC then x <- '0100' 
    else {
	    when (trans and RW == MemHint_No or CA == '01') do #BAD_MAIR_SETUP;
            x<1:0> <- RW;
	    when !trans do x<3> <- true;
	    when CA == MemAttr_WB do x<2> <- false
    };
    match el, outer {
	case 2, false => C.MAIR_EL2<8*i+3:8*i> <- x
	case 2, true  => C.MAIR_EL2<8*i+7:8*i+4> <- x
	case 3, false => C.MAIR_EL3<8*i+3:8*i> <- x
	case 3, true  => C.MAIR_EL3<8*i+7:8*i+4> <- x
	case _, false => C.MAIR_EL1<8*i+3:8*i> <- x
	case _, true  => C.MAIR_EL1<8*i+7:8*i+4> <- x
    }
}

unit set_PTO_ST1 (el :: nat, upper :: bool, baddr :: bits(48), ASID :: bits(16)) = 
{
    match el, upper {
	case 1 , false => { C.TTBR0_EL1.BADDR <- baddr; C.TTBR0_EL1.ASID <- ASID }
	case 1 , true  => { C.TTBR1_EL1.BADDR <- baddr; C.TTBR1_EL1.ASID <- ASID }
	case 2 , _     => { C.TTBR0_EL2.BADDR <- baddr; C.TTBR0_EL2.ASID <- ASID }
	case _ , _     => { C.TTBR0_EL3.BADDR <- baddr; C.TTBR0_EL3.ASID <- ASID }
    }
}

unit set_PTO_ST2 (baddr :: bits(48), VMID :: bits(8)) = 
{
    C.VTTBR_EL2.BADDR <- baddr;
    C.VTTBR_EL2.ASID  <- [VMID]
}

unit enable_caches_ST1(el :: nat, x :: bool) = match el {
	case 3 => {C.SCTLR_EL3.I <- x; C.SCTLR_EL3.C <- x} 
	case 2 => {C.SCTLR_EL2.I <- x; C.SCTLR_EL2.C <- x}
	case _ => {C.SCTLR_EL1.I <- x; C.SCTLR_EL1.C <- x}
}

unit enable_caches_ST2(x :: bool) = 
{
    C.HCR_EL2.ID <- !x;
    C.HCR_EL2.DC <- !x
}


unit enable_MMU_ST1 (el :: nat, epd0 :: bool, epd1 :: bool) = 
{
    match el {
	case 0 or 1 => C.SCTLR_EL1.M <- true
	case 2 => C.SCTLR_EL2.M <- true
	case _ => C.SCTLR_EL3.M <- true
    };
    when el <= 1 do {
	C.TCR_EL1.EPD0 <- epd0;
	C.TCR_EL1.EPD1 <- epd1
    }
}


unit disable_MMU_ST1 (el :: nat) = 
{
    match el {
	case 0 or 1 => C.SCTLR_EL1.M <- false
	case 2 => C.SCTLR_EL2.M <- false
	case _ => C.SCTLR_EL3.M <- false
    };
    when el <= 1 do {
	C.TCR_EL1.EPD0 <- false;
	C.TCR_EL1.EPD1 <- false
    }
}

unit enable_MMU_ST2 (DC :: bool) = 
{
    C.HCR_EL2.VM <- true;
    C.HCR_EL2.DC <- DC
}

unit disable_MMU_ST2 () = 
{
    C.HCR_EL2.VM <- false;
    C.HCR_EL2.DC <- false
}


-------------------------
-- building descriptors
-------------------------

-- ST1 table parameters for next levels of translation
record TableParams {
    secure :: bool
    useraccess :: bool
    readonly :: bool
    executable :: bool       -- holds only for EL0 in EL1/0 translation scheme
    priv_executable :: bool  -- holds only for EL1 in EL1/0 translation scheme, ignored in other ELs
    valid :: bool
}

-- ST1 page/block parameters 
record PBParams_ST1 {
    executable :: bool       -- holds only for EL0 in EL1/0 translation scheme
    priv_executable :: bool  -- holds only for EL1 in EL1/0 translation scheme, ignored in other ELs
    contiguous :: bool
    global :: bool
    accessed :: bool
    shareability :: PTShareability
    useraccess :: bool       -- only defined for EL1/0 translation scheme
    readonly :: bool
    attr_index :: bits(3)
    valid :: bool
}

-- ST2 page/block parameters 
record PBParams_ST2 {
    executable :: bool
    contiguous :: bool
    accessed :: bool
    shareability :: PTShareability
    readable :: bool
    writable :: bool
    memory_type :: MemType
    device_type :: DeviceType
    outer :: PTCacheType
    inner :: PTCacheType
    valid :: bool
}

construct PBParams { ST1Param :: PBParams_ST1, ST2Param :: PBParams_ST2 } 

-- valid ST2 table parameter
TableParams tpar_ST2() = {
    var tpar;

    tpar.secure <- true;
    tpar.useraccess <- true;
    tpar.readonly <- false;
    tpar.executable <- true;
    tpar.priv_executable <- true;
    tpar.valid <- true;

    return tpar
}


-- map type to generate descriptors later on, page table at most 2^13 dwords big
type PAGEMAP = bits(13) -> bits(36)

PBDescriptor Table_descriptor_ST1(addr :: ADR, par :: TableParams) = 
{
    var desc :: PBDescriptor;

    desc.v <- par.valid;
    desc.pte <- true;
    
    desc.adr <- addr<47:12>;

    desc.NST <- !par.secure;
    desc.APT<1> <- par.readonly;
    desc.APT<0> <- !par.useraccess;
    desc.XNT <- !par.executable;
    desc.PXNT <- !par.priv_executable;

    -- checked by PT setup, other page/block fields irrelevant for Table descriptors
    desc.Contiguous <- false;
    
    return desc
}  

PBDescriptor Table_descriptor_ST2(addr :: ADR) = Table_descriptor_ST1(addr, tpar_ST2())
 
PBDescriptor PB_descriptor(addr :: ADR, param :: PBParams, block :: bool) = 
{
    var desc :: PBDescriptor;

    desc.pte <- !block;
    
    desc.adr <- addr<47:12>;

    match param {
	case ST1Param(par) => {
		desc.v <- par.valid;
		desc.XN <- !par.executable;
		desc.PXN <- !par.priv_executable;
		desc.Contiguous <- par.contiguous;
		desc.nG <- !par.global;
		desc.AF <- par.accessed;
		desc.SH <- match par.shareability {
		    case NonShareable => '00' 
		    case InnerShareable => '11' 
		    case OuterShareable => '10'
		};
		desc.AP<1> <- par.readonly;
		desc.AP<0> <- par.useraccess;
		desc.Attr<2:0> <- par.attr_index
	    }
	case ST2Param(par) => {
		desc.v <- par.valid;
		desc.XN <- !par.executable;
		desc.Contiguous <- par.contiguous;
		desc.AF <- par.accessed;
		desc.SH <- match par.shareability {
		    case NonShareable => '00' 
		    case InnerShareable => '11' 
		    case OuterShareable => '10'
		};
		desc.AP<1> <- par.writable;
		desc.AP<0> <- par.readable;
		desc.Attr<3:2> <- match par.memory_type, par.outer {
		    case MemType_Device, _            => '00'
       		    case MemType_Normal, NonCacheable => '01' 
	      	    case MemType_Normal, WriteThrough => '10' 
		    case MemType_Normal, WriteBack    => '11'
		};
		desc.Attr<1:0> <- if par.memory_type == MemType_Device 
		then match par.device_type {
		    case DeviceType_nGnRnE => '00'
		    case DeviceType_nGnRE => '01'
		    case DeviceType_nGRE => '10'
		    case DeviceType_GRE => '11'
		}
		else match par.inner {
    		    case NonCacheable => '01' 
		    case WriteThrough => '10' 
		    case WriteBack    => '11'
		}
	    }
    };

    return desc
}  

TableParams sample_tpar() = {
    var tpar;

    tpar.secure <- false;
    tpar.useraccess <- true;
    tpar.readonly <- false;
    tpar.executable <- true;
    tpar.priv_executable <- true;
    tpar.valid <- true;

    return tpar
}

PBParams_ST1 sample_pbpar_ST1() = {
    var pbpar :: PBParams_ST1;

    pbpar.executable <- true;
    pbpar.priv_executable <- true;
    pbpar.contiguous <- true;
    pbpar.global <- true;
    pbpar.accessed <- true;
    pbpar.shareability <- OuterShareable;
    pbpar.useraccess <- true;
    pbpar.readonly <- false;
    pbpar.attr_index <- '000';
    pbpar.valid <- true;
    
    return pbpar
}


PBParams_ST2 sample_pbpar_ST2() = {
    var pbpar :: PBParams_ST2;

    pbpar.executable <- true;
    pbpar.contiguous <- true;
    pbpar.accessed <- true;
    pbpar.shareability <- OuterShareable;
    pbpar.readable <- true;
    pbpar.writable <- true;
    pbpar.valid <- true;
    pbpar.memory_type <- MemType_Normal;
    pbpar.outer <- WriteThrough;
    pbpar.inner <- WriteBack;
    
    return pbpar
}


dword read_dword (adr :: ADR) = 
{
    a = Align(adr, 8);
    var data = Nil :: bool list;
    for i in 0 .. 7 do data <- [PM(a+[i])]:data;
    return [data]
}


unit write_dword (adr :: ADR, d :: bits(64)) = 
{
    a = Align(adr, 8);
    for j in 0 .. 7 do PM(a + [j]) <- d<8*j+7:8*j>
}


unit set_rights_ST1 (adr :: ADR, ex :: bool, pex :: bool, user :: bool, ro :: bool) = 
{
    d = read_dword(adr);
    var desc = cast_desc(d);

    desc.XN <- !ex;
    desc.PXN <- !pex;
    desc.AP<1> <- ro;
    desc.AP<0> <- user;

    write_dword(adr, &desc)
}


unit set_rights_ST2 (adr :: ADR, ex :: bool, read :: bool, write :: bool) = 
{
    d = read_dword(adr);
    var desc = cast_desc(d);

    desc.XN <- !ex;
    desc.AP<1> <- write;
    desc.AP<0> <- read;

    write_dword(adr, &desc)
}


-------------------------
-- setting up page tables
-------------------------

-- NOTE: only map virtual memories that are sized powers of 2 * 4K for the moment

-- set up a page table in physical memory, starting at pto that will be aligned to 8 bits, with n entries, using sample descriptor desc and adapting the address using mapping
-- n should be <= 2^br, else the table is repeated
unit setup_PT(pto :: ADR, n :: nat, mapping :: PAGEMAP, desc :: PBDescriptor) = 
{
    base = (pto<47:3>):'000';
    var d = desc;

    for i in 0 .. n-1 do {
	d.adr <- mapping([i]);
	for j in 0 .. 7 do PM(pto + [i*8] + [j]) <- &d<8*j+7:8*j>
    }	
}


-- simple linear mapping for different granule sizes and levels, mapped area starts at aligned offset:0^12
-- distinguish mapping to next level or block mapping
PAGEMAP linear_mapping (offset :: bits(36), size :: bits(N), level :: nat, block :: bool) with N in 4, 16, 64 =
{
    -- limit level
    l = if level > 3 then 3 else level;
	
    br = Log2(N)+7;

    -- increment aligned to 12 bits
    incbits = if block then (4-l)*br+3-12 else br+3-12;
    inc = 2**incbits;

    base = Align(offset, inc);

    var mapping;

    for i in 0 .. (2**13 - 1) do mapping([i]) <- base + [i*inc];

    return mapping
}


-- allocate array of page table entries mapping to address space of size m at adr according to sample descriptor
-- place page table(s) at pto that will be aligned according to block/page size (if more than one page table)
-- level l is assumed to be consistent with granule size and <= 3 
-- contiguous bit is set optionally but obeys constraints of different granule sizes
unit setup_PT_array(pto :: ADR, m :: nat, size :: bits(N), l :: nat, d :: PBDescriptor) with N in 4, 16, 64 =
{
    -- bits resolved by each level of translation
    br = Log2(N)+7;

    block = !d.pte;
    physbase = d.adr:0`12; --`
    
    -- number of bits resolved by one page table entry
    res = br*(4-l)+3;

    -- increment for physical page indices in the entries
    inc = if block then res else br+3;

    -- align physical base address to the granule size
    var pbase = if block then Align(physbase,2**Max(m,br+3)) else Align(physbase,2**(br+3));

    var desc = d;

    -- compute required number of page table entries
    npte = if (res >= m) then 1 else 2 ** (m - res);

    ncont = match N {
	case 4  => 16
	case 16 => if l == 3 then 128 else 32
	case _  => 32
    };

    -- only set Contiguous bit if enough ptes are allocated
    when (d.Contiguous and npte < ncont) do desc.Contiguous <- false;

    -- number of page tables, remaining entries that do not fill a whole page table, and contiguous blocks in there
    npt = npte div 2**br;

    -- if less than one complete page table needed fill one with the required amount of entries
    -- else create npt complete page tables
    if (npt == 0) then setup_PT(Align(pto,8), npte, linear_mapping(pbase<47:12>, size, l, block), desc)
    else {
	    -- align pto to page/block size
	    var base = Align(pto,2**(br+3));
	    for i in 0 .. npt-1 do {
		setup_PT(base, 2**br, linear_mapping(pbase<47:12>, size, l, block), desc);
		base <- base + [2**(br+3)];
		pbase <- pbase + [(2**(inc+br))]
	    }
    }
}

-- place linear mapping of address space (m bits) in memory, starting address physbase that will be aligned to m (but at least granule size)
-- place page table at pto that will be aligned according to block/page size
-- page table is later used at level l of the translation as the final translation step
-- NOTE: this allows to set up an unaligned array of page tables if pto aligned to m-inc bits, however this should not produce any faults as long as the PT lies within the implemented physical address space
unit map_address_space (pto :: ADR, physbase :: ADR, m :: nat, size :: bits(N), l :: nat, par :: PBParams) with N in 4, 16, 64 =
{    
    -- limit level according to granule size
    level, block = match l {
	case 0 or 1 => ((if N == 4 then 1 else 2), true)
	case 2      => (2, true) 
	case _      => (3, false)
    };

    -- sample page/block descriptor start mapping to address pbase
    desc = PB_descriptor(physbase, par, block);

    setup_PT_array(pto, m, size, level, desc)
}


-- make indirection / level 0,1,2, redirect to address space of m bits size
-- place at pto, point to contiguous region at nextbase, both aligned to granule size 
unit make_indirection_ST1(pto :: ADR, nextbase :: ADR, m :: nat, size :: bits(N), l :: nat, par :: TableParams) with N in 4, 16, 64 = 
{
    level = if l > 2 then 2 else l;

    desc = Table_descriptor_ST1(nextbase, par);

    setup_PT_array(pto, m, size, level, desc)
}

unit make_indirection_ST2(pto :: ADR, nextbase :: ADR, m :: nat, size :: bits(N), l :: nat) with N in 4, 16, 64 = 
    make_indirection_ST1(pto, nextbase, m, size, l, tpar_ST2())

record PTParams {
    vbase :: VA
    pbase :: ADR
    m :: nat
    n :: nat -- input address size
    pto0 :: ADR
    pto1 :: ADR
    pto2 :: ADR
    pto3 :: ADR
    granule :: nat
    startl :: nat
    lastl :: nat
    pbpar :: PBParams
    tpar :: TableParams 
    st1 :: bool
    conc :: bool
}

-- set up all tables for the given parameters
unit make_tables_(ptp :: PTParams, size :: bits(N)) with N in 4, 16, 64 =
{
    br = Log2(N)+7;
    
    vbase = Align(ptp.vbase,2**ptp.m);
    pto0 = Align(ptp.pto0,2**(br+3));
    pto1 = Align(ptp.pto1,2**(br+3));
    pto2 = Align(ptp.pto2,2**(br+3));
    pto3 = Align(ptp.pto3,2**(br+3));

    conc = ptp.conc and !ptp.st1;

    px0 = [([px(vbase<47:0>, 0, br, ptp.n, false)]::nat) * 8]::ADR;
    px1 = [([px(vbase<47:0>, 1, br, ptp.n, ptp.startl==1 and conc)]::nat)*8]::ADR;
    px2 = [([px(vbase<47:0>, 2, br, ptp.n, ptp.startl==2 and conc)]::nat)*8]::ADR;
    px3 = [([px(vbase<47:0>, 3, br, ptp.n, ptp.startl==3 and conc)]::nat)*8]::ADR;

    match ptp.lastl {
	case 0 or 1 => map_address_space (pto1+px1, ptp.pbase, ptp.m, size, 1, ptp.pbpar) 
	case 2 => map_address_space (pto2+px2, ptp.pbase, ptp.m, size, 2, ptp.pbpar)
	case _ => map_address_space (pto3+px3, ptp.pbase, ptp.m, size, 3, ptp.pbpar)
    };

    
    sl = [ptp.startl]::bits(2);
    ll = [ptp.lastl]::bits(2);
    st = [ptp.st1]::bits(2);
    co = [conc]::bits(2);

{- debug information
    PM(0) <- sl:ll:co:st;
    PM(1) <- px1<7:0>;
    PM(2) <- px1<15:8>;
    PM(3) <- px1<23:16>;
    PM(4) <- px3<7:0>;
    PM(5) <- px3<15:8>;
    PM(6) <- px3<23:16>;
-}
 
    if ptp.st1 then {
	    when ptp.lastl >= 3 and ptp.startl < 3 do make_indirection_ST1(pto2+px2, pto3, ptp.m, size, 2, ptp.tpar);
	    when ptp.startl <= 1 and ptp.lastl >= 2 do make_indirection_ST1(pto1+px1, pto2, ptp.m, size, 1, ptp.tpar);
	    when ptp.startl == 0 do make_indirection_ST1(pto0+px0, pto1, ptp.m, size, 0, ptp.tpar)
	}
    else {
	    when ptp.lastl >= 3 and ptp.startl < 3 do make_indirection_ST2(pto2+px2, pto3, ptp.m, size, 2);
	    when ptp.startl <= 1 and ptp.lastl >= 2 do make_indirection_ST2(pto1+px1, pto2, ptp.m, size, 1);
	    when ptp.startl == 0 do make_indirection_ST2(pto0+px0, pto1, ptp.m, size, 0)
    }
}

unit make_tables(ptp :: PTParams) = match ptp.granule {
    case 4  => make_tables_(ptp :: PTParams, 0`4) --`
    case 16 => make_tables_(ptp :: PTParams, 0`16) --`
    case _  => make_tables_(ptp :: PTParams, 0`64) --`
}

-- standart positioning of page tables, 8MB for each level at both stages, block aligned to 64MB
ADR PTO0_ST1 = 0x04000000
ADR PTO1_ST1 = 0x04800000
ADR PTO2_ST1 = 0x05000000
ADR PTO3_ST1 = 0x05800000
ADR PTO0_ST2 = 0x06000000
ADR PTO1_ST2 = 0x06800000
ADR PTO2_ST2 = 0x07000000
ADR PTO3_ST2 = 0x07800000



------------------------------------
-- setting up the configuration
------------------------------------

PTParams config_pt(g :: nat, m :: nat, n :: nat, coarse :: bool, pbpar :: PBParams, tpar :: TableParams, st1 :: bool) =
{
    var pt;
    pt.m <- m;
    pt.n <- n;
    pt.granule <- g;
    pt.pbpar <- pbpar; 
    pt.tpar <- tpar; 
    pt.st1 <- st1;

    br = match g {
	case 4 => 9
	case 16 => 11
	case _ => 13
    };

    nl = nlu(n, br, false);

    conc = (n - (nl*br+3) <= 4 and n - (nl*br+3) >= 0);
    
    pt.conc <- (coarse or g==16 and n==48) and !st1 and conc;
    
    start = match nl {
	case 0 or 1 => 3
	case 2 => 2
	case 3 => 1
	case _ => 0
    };

    pt.startl <- start + [pt.conc]::nat;
    
    pt.lastl <- match coarse, g {
	case true, 4  => Max(pt.startl, 1) 
	case true, _  => Max(pt.startl, 2)
        case false, _ => 3
    };
	
    return pt
}

unit mmu_isw_setup_std () = 
{
    -- CortexA53 parameters
    support4K = true;
    support16K = false;
    support64K = true;
    supportBE0 = false;
    supportBE = false;
    supportSM = false;
    ASID16 = true;
    PAR = 0`40; --`
    set_memory_features(support4K, support16K, support64K, supportBE0,  supportBE, supportSM, ASID16, PAR);

    -- implementation switches, ST1:

    -- TxSZ value is bigger than 39: 1 -> translation fault; 0 -> behave as 39
    MMU.ISW.IMPL_TxSZ_BIGGER_THAN_39_FAULT <- false;

    -- TxSZ value is smaller than 16: 1 -> translation fault; 0 -> behave as 16
    MMU.ISW.IMPL_TxSZ_LESS_THAN_16_FAULT <- false;

    -- TTBR not aligned to first level PT size: 1 -> treat as zero; 0 -> corrupted translation
    MMU.ISW.IMPL_TTBR_MISALIGNED_TREAT_AS_ZERO <- true;

    -- TTBR lower bits: 1 -> read as zero; 0 -> read actual value
    MMU.ISW.IMPL_TTBR_LOW_BITS_READ_AS_ZERO <- true;

    -- default value for TG0 and TG1 fields if invalid choice or not implemented, assume 4KB granule for now
    MMU.ISW.IMPL_TCR_TG_DEFAULT <- '00';

    -- return actually used value for TG on read of TCR_ELx instead of programmed value
    MMU.ISW.IMPL_TCR_READ_RETURNS_USED_TG <- true;

    -- raise a translation fault if contiguous bit is set for a too big set of blocks
    MMU.ISW.IMPL_CONTIGUOUS_BIT_FAULT <- true;
    
    -- ST2:
    
    -- VTCR.T0SZ value is smaller than 16: 1 -> translation fault; 0 -> behave as 16
    MMU.ISW.IMPL_VT0SZ_LESS_THAN_16_FAULT <- false;
    
    -- VTCR.T0SZ value is less than 16: treat as 16 for checking SL0
    MMU.ISW.IMPL_VT0SZ_LESS_THAN_IS_16_FOR_SL0 <- true;
    
    -- default value for TG0 if invalid choice or not implemented
    MMU.ISW.IMPL_VTCR_TG_DEFAULT <- '00'
}

unit mode_setup(ns::bool, el::nat) =
{
    C.SCR_EL3.NS <- ns;
    C.PSTATE.EL <- [el]
}

unit mmu_setup_std_both() = 
{
    mmu_isw_setup_std();

    -- setup ST2 page tables, 512MB blocks at level 2, 64K, input address size 36, identity mapping
    var pbpar = sample_pbpar_ST2();
    pbpar.outer <- WriteBack;

    coarse = true;
    st2 = true;
    var pt = config_pt(64, 36, 40, coarse, ST2Param(pbpar), sample_tpar(), st2);
   
    pt.vbase <- 0x0;
    pt.pbase <- 0x0;
    pt.pto0 <- PTO0_ST2;
    pt.pto1 <- PTO1_ST2;
    pt.pto2 <- PTO2_ST2;
    pt.pto3 <- PTO3_ST2;

    make_tables(pt);
    
    -- TODO: add finer granularity entries to protect ST2 tables
    {- make ST2 tables not writable for EL1/0
    executable = false;
    readable = false;
    writable = false;
    set_rights_ST2(0x07000018, executable, readable, writable);
    -} 

    -- setup ST1 page tables, 2MB blocks at level 2, 4K, input address size 32, identity mapping
    pbpar1 = sample_pbpar_ST1();

    st1 = false;
    pt <- config_pt(4, 32, 36, coarse, ST1Param(pbpar1), sample_tpar(), st1);

    pt.pbase <- 0x100000000;
    pt.pto0 <- PTO0_ST1;
    pt.pto1 <- PTO1_ST1;
    pt.pto2 <- PTO2_ST1;
    pt.pto3 <- PTO3_ST1;
    pt.lastl <- 2;

    make_tables(pt);

    transience = false;
    outer = true;
    inner = false;
    set_MAIR_normal(1, 0, inner, MemAttr_WT, MemHint_RWA, transience);
    set_MAIR_normal(1, 0, outer, MemAttr_WB, MemHint_WA, transience);

    lower = false;
    set_input_address_size_ST1(1, lower, 32);
    set_input_address_size_ST2(36);

    skip_levels_ST2(1);

    set_output_address_size_ST1(1, 0`36); --`
    set_output_address_size_ST2(0`40); --`
    set_granule_size_ST1(1,lower,0`4); --`
    set_granule_size_ST2(0`64); --`
    set_PT_cache_type_ST1(1, lower, outer, WriteBack, false);
    set_PT_cache_type_ST1(1, lower, inner, WriteBack, false);
    set_PT_cache_type_ST2(outer, WriteBack, false);
    set_PT_cache_type_ST2(inner, WriteBack, false);
    set_PT_shareability_ST1(1, lower, OuterShareable); 
    set_PT_shareability_ST2(OuterShareable);
    set_PTO_ST1 (1, lower, PTO1_ST1, 0`16); --`
	set_PTO_ST2 (PTO2_ST2, 0`8); --`
	
    enable_caches_ST1(1, true);
    enable_caches_ST1(2, true);
    enable_caches_ST1(3, true);
    enable_caches_ST2(true);

    -- enable alignment checks
    C.SCTLR_EL1.A <- true;
    C.SCTLR_EL2.A <- true;
    C.SCTLR_EL3.A <- true;

    epd0 = false;
    epd1 = true;
    enable_MMU_ST1(1, epd0, epd1);
    DC = false;
    enable_MMU_ST2(DC)
}

unit mmu_setup_std_ST2_only () = 
{
    mmu_isw_setup_std();

    -- setup ST2 page tables, 512MB blocks at level 2, 64K, input address size 36, identity mapping
    var pbpar = sample_pbpar_ST2();
    pbpar.outer <- WriteBack;

    coarse = true;
    st2 = true;
    var pt = config_pt(64, 36, 40, coarse, ST2Param(pbpar), sample_tpar(), st2);
   
    pt.vbase <- 0x0;
    pt.pbase <- 0x0;
    pt.pto0 <- PTO0_ST2;
    pt.pto1 <- PTO1_ST2;
    pt.pto2 <- PTO2_ST2;
    pt.pto3 <- PTO3_ST2;

    make_tables(pt);

    -- TODO:
    {- make ST2 not writable for EL1/0
    executable = false;
    readable = false;
    writable = false;
    set_rights_ST2(0x07000018, executable, readable, writable);
    -}

    set_input_address_size_ST2(36);

    skip_levels_ST2(1);

    outer = true;
    inner = false;

    set_output_address_size_ST2(0`40); --`
    set_granule_size_ST2(0`64); --`
    set_PT_cache_type_ST2(outer, WriteBack, false);
    set_PT_cache_type_ST2(inner, WriteBack, false);
    set_PT_shareability_ST2(OuterShareable);
    set_PTO_ST2 (PTO2_ST2, 0`8); --`

    enable_caches_ST1(1, true);
    enable_caches_ST1(2, true);
    enable_caches_ST1(3, true);
    enable_caches_ST2(true);

    -- enable alignment checks
    C.SCTLR_EL1.A <- true;
    C.SCTLR_EL2.A <- true;
    C.SCTLR_EL3.A <- true;

    DC = true;
    enable_MMU_ST2(DC)    
}
 

unit mmu_setup_std_ST1_only () = 
{
    
    mmu_isw_setup_std();

    -- setup ST1 page tables, 2MB blocks at level 2, 4K, input address size 32, identity mapping
    pbpar1 = sample_pbpar_ST1();

    coarse = true;
    st1 = false;
    var pt = config_pt(4, 32, 40, coarse, ST1Param(pbpar1), sample_tpar(), st1);

    pt.vbase <- 0x0;
    pt.pbase <- 0x100000000;
    pt.pto0 <- PTO0_ST1;
    pt.pto1 <- PTO1_ST1;
    pt.pto2 <- PTO2_ST1;
    pt.pto3 <- PTO3_ST1;
    pt.lastl <- 2;

    make_tables(pt);

    transience = false;
    outer = true;
    inner = false;
    set_MAIR_normal(1, 0, inner, MemAttr_WT, MemHint_RWA, transience);
    set_MAIR_normal(1, 0, outer, MemAttr_WB, MemHint_WA, transience);

    lower = false;
    set_input_address_size_ST1(1, lower, 32);

    set_output_address_size_ST1(1, 0`40); --`
    set_granule_size_ST1(1,lower,0`4); --`
    set_PT_cache_type_ST1(1, lower, outer, WriteBack, false);
    set_PT_cache_type_ST1(1, lower, inner, WriteBack, false);
    set_PT_shareability_ST1(1, lower, OuterShareable); 
    set_PTO_ST1 (1, lower, PTO1_ST1, 0`16); --`

    enable_caches_ST1(1, true);
    enable_caches_ST1(2, true);
    enable_caches_ST1(3, true);

    -- enable alignment checks
    C.SCTLR_EL1.A <- true;
    C.SCTLR_EL2.A <- true;
    C.SCTLR_EL3.A <- true;

    epd0 = false;
    epd1 = true;
    enable_MMU_ST1(1, epd0, epd1);
    disable_MMU_ST2()
}

unit mmu_setup_std_flat () =
{

    mmu_isw_setup_std();

    enable_caches_ST1(1, true);
    enable_caches_ST1(2, true);
    enable_caches_ST1(3, true);
    
    -- enable alignment checks
    C.SCTLR_EL1.A <- true;
    C.SCTLR_EL2.A <- true;
    C.SCTLR_EL3.A <- true;

    disable_MMU_ST1(1);
    disable_MMU_ST2()
}
   
unit init_automaton () = 
{
    MMU.ST1 <- wait;
    MMU.ST2 <- wait;
    MMU.S <- mmu_wait;
    MMU.F1.typ <- Fault_None;
    MMU.F2.typ <- Fault_None;
    MMU.ST2RPL <- None;
    
    C.S <- core_issue;
    C.CURR_REPLY <- None;
    C.CURR_FAULT.typ <- Fault_None;

    MC.S <- mem_wait
}

-----------------------
-- helper functions 
-----------------------

type BYTESTRING = bits(8) list 

unit store_bytes(a :: ADR, c :: BYTESTRING) = 
{
    n = Length(c);
    var code = c;

    for i in 0 .. n-1 do {
	PM(a+[i]) <- Head(code);
	code <- Tail(code)
    }
}

string padstring (s :: string) = 
{
    n = Length(s) mod 8;
    var str = s;
    
    when n>0 do {
	for i in 1 .. 8-n do str <- str : "0"
    };

    return str
}

BYTESTRING code2bs(code :: string) = 
{
    var c = padstring(code);

    var bs = Nil :: BYTESTRING;

    for i in 1 .. (Length(c) div 2) do {
	bs <- bs : ([Take(2,c)] @ Nil);
	c <- Drop(2,c)
    };
    return bs
}

unit code_setup(code :: string, codebase :: string) = store_bytes([codebase], code2bs(code))

unit set_pc(virtbase :: string) =  C.PC <- [virtbase]

unit set_reg(r :: nat, s :: string) = C.REG([r]) <- [s]


----------------------------
-- ELF support
----------------------------

string * string Chop (n::nat, s :: string) = (Take(n,s),Drop(n,s))

string rev_hex (s :: string) = if Length(s) <= 2 then return s else { b, s = Chop(2,s); return rev_hex(s) : b }

string * string ChopR (n::nat, s :: string) = (rev_hex(Take(n,s)),Drop(n,s))


exception ELF_ERROR :: string

unit install_elf(f :: string, codebase :: string, vbase :: string) = 
{
    when Length(f) < 116 do #ELF_ERROR ("unexpected end of file");

    magic, elf = Chop(8,f);
    class, elf = Chop(2,elf);
    endian, elf = Chop(2,elf);
    version, elf = ChopR(2,elf);
        elf = Drop(22,elf);
    machine, elf = ChopR(4,elf);
    version2, elf = ChopR(8,elf);
    entry, elf = ChopR(16,elf);
    phoff, elf = ChopR(16,elf);
        elf = Drop(24,elf);
    ehsize, elf = ChopR(4,elf);
    phentsize, elf = ChopR(4,elf);
    phnum, elf = ChopR(4,elf);

    when magic != "7F454C46" do #ELF_ERROR ("magic number: not an ELF file");
    when class != "02" do #ELF_ERROR ("64-bit format expected");
    when version != "01" or version2 != "00000001" do #ELF_ERROR ("original ELF version expected");
    when machine != "00B7" do #ELF_ERROR ("machine: AArch64 expected");
    
    -- set endianness
    match endian {
	case "01" => C.SCTLR_EL1.E0E <- false
	case "02" => C.SCTLR_EL1.E0E <- true
	case _    => #ELF_ERROR ("invalid endianness parameter")
    };

    -- set up code in memory and set pc
    code_setup(f,codebase);

    nph = [phnum]`64; --`
    phs = [phentsize]`64; --`
    ehs = [ehsize]`64; --`
    base = [vbase]`64; --`
    pho = [phoff]`64; --`
    eo = [entry]`64; --`

    C.PC <- base + ehs + pho + nph * phs + eo

--    return "magic":magic:"class":class:"endian":endian:"version":version:"machine":machine:"version2":version2:"entry":entry:"phoff":phoff:"ehsize":ehsize:"phentsize":phentsize:"phnum":phnum
}


-----------------------------
-- test evaluation functions
-----------------------------

string xtob (x :: string) = 
{
    b = [x]`64; --`
    bl = [b] :: bool list;
    return [bl]
}


-- read d bytes from hex address a
string inspect_mem( a :: string, d :: nat ) = 
{
    adr = [a]`48; --`
    var data = Nil :: bool list;
    for i in 0 .. d-1 do data <- [PM(adr+[i])]:data;
    return [data]
}

string inspect_hex( a :: string ) = 
{
    adr = [a]`48; --`
    var data :: word;
    for i in 0 .. 3 do data<8*i+7:8*i> <- PM(adr+[i]);
    return [data]
}


string inspect_map( pgm :: PAGEMAP, a :: string ) =
{
    bl = [pgm([a])]:: bool list;
    return [bl]
}

----------------------------
-- test cases
----------------------------



bits(36) TEST_conv(a :: string) = 
{
    b = [a] :: bits(48);

    return b<47:12>
}

PAGEMAP TEST_pagemap(g :: nat, b :: string) = 
{
    bs = [b] :: bits(48);
    base = bs<47:12>;
    match g {
	case 4  => linear_mapping (base, '0000', 3, false)
	case 16 => linear_mapping (base, 0`16  , 3, false) --`
	case _  => linear_mapping (base, 0`64  , 3, false) --`
    }
}

unit TEST_pte_loop_4K(pto :: string, target :: string, mapping :: PAGEMAP) = 
{
    bs = [pto]::bits(48);
    pbase = [target] :: bits(48);

    b = '0000':bs<11:3>;

    base = (bs<47:3>):'000';
    var d = PB_descriptor(pbase, ST1Param(sample_pbpar_ST1()), false);

    for i in 0 .. 2**9-1 do {
	d.adr <- mapping(b + [i]);
	for j in 0 .. 7 do PM(bs + [i*8] + [j]) <- &d<8*j+7:8*j>
    }	

}

unit TEST_pt_setup_4K(pto :: string, p :: string) = 
{
    base = [pto] :: bits(48);
    pbase = [p] :: bits(48);
    
    desc = PB_descriptor(pbase, ST1Param(sample_pbpar_ST1()), false);

    setup_PT(base, 2**9, linear_mapping(pbase<47:12>, '0000', 3, false), desc)
}


unit TEST_ptarray_4K(pto :: string, p :: string, npt :: nat) = 
{
    var base = [pto] :: bits(48);
    var pbase = [p] :: bits(48);
    
    desc = PB_descriptor(pbase, ST1Param(sample_pbpar_ST1()), false);
    br = 9;
    inc = 12;

    for i in 0 .. npt-1 do {
	setup_PT(base, 2**br, linear_mapping(pbase<47:12>, '0000', 3, false), desc);
	base <- base + [2**(br+3)];
	pbase <- pbase + [(2**(inc+br))]
    }
}


unit TEST_id_table_fine_4K_ST1() = 
{
    var pt;

    var pbpar = sample_pbpar_ST1();

    pt.vbase <- 0;
    pt.pbase <- 0;
    pt.m <- 22;
    pt.n <- 48;
    pt.pto0 <- PTO0_ST1;
    pt.pto1 <- PTO1_ST1;
    pt.pto2 <- PTO2_ST1;
    pt.pto3 <- PTO3_ST1;
    pt.granule <- 4;
    pt.startl <- 0;
    pt.lastl <- 3;
    pt.pbpar <- ST1Param(pbpar); 
    pt.tpar <-  sample_tpar(); 
    pt.st1 <- true;

    make_tables(pt)
}


unit TEST_id_table_fine_16K_ST1() = 
{
    var pt;

    var pbpar = sample_pbpar_ST1();
    pbpar.attr_index <- '110';
    pbpar.shareability <- InnerShareable;
    pbpar.global <- false;

    pt.vbase <- 0;
    pt.pbase <- 0;
    pt.m <- 26;
    pt.n <- 48;
    pt.pto0 <- PTO0_ST1;
    pt.pto1 <- PTO1_ST1;
    pt.pto2 <- PTO2_ST1;
    pt.pto3 <- PTO3_ST1;
    pt.granule <- 16;
    pt.startl <- 0;
    pt.lastl <- 3;
    pt.pbpar <- ST1Param(pbpar); 
    pt.tpar <-  sample_tpar(); 
    pt.st1 <- true;

    make_tables(pt)
}


unit TEST_id_table_fine_64K_ST1() = 
{
    var pt;

    var pbpar = sample_pbpar_ST1();
    pbpar.attr_index <- '101';
    pbpar.shareability <- NonShareable;
    pbpar.global <- false;
    pbpar.valid <- false;

    pt.vbase <- 0x40000000;
    pt.pbase <- 0;
    pt.m <- 30;
    pt.n <- 48;
    pt.pto0 <- PTO0_ST1;
    pt.pto1 <- PTO1_ST1;
    pt.pto2 <- PTO2_ST1;
    pt.pto3 <- PTO3_ST1;
    pt.granule <- 64;
    pt.startl <- 1;
    pt.lastl <- 3;
    pt.pbpar <- ST1Param(pbpar); 
    pt.tpar <-  sample_tpar(); 
    pt.st1 <- true;

    make_tables(pt)
}

unit TEST_id_table_half_4K_ST1() = 
{
    var pt;

    var pbpar = sample_pbpar_ST1();
    pbpar.attr_index <- '101';
    pbpar.shareability <- NonShareable;
    pbpar.global <- false;
    pbpar.valid <- false;

    pt.vbase <- 0x00100000;
    pt.pbase <- 0x00200000;
    pt.m <- 20;
    pt.n <- 48;
    pt.pto0 <- PTO0_ST1;
    pt.pto1 <- PTO1_ST1;
    pt.pto2 <- PTO2_ST1;
    pt.pto3 <- PTO3_ST1;
    pt.granule <- 4;
    pt.startl <- 0;
    pt.lastl <- 3;
    pt.pbpar <- ST1Param(pbpar); 
    pt.tpar <-  sample_tpar(); 
    pt.st1 <- true;

    make_tables(pt)
}

unit TEST_id_table_half_16K_ST1() = 
{
    var pt;

    var pbpar = sample_pbpar_ST1();
    pbpar.attr_index <- '101';
    pbpar.shareability <- NonShareable;
    pbpar.global <- false;
    pbpar.valid <- false;

    pt.vbase <- 0x00100000;
    pt.pbase <- 0x00200000;
    pt.m <- 20;
    pt.n <- 48;
    pt.pto0 <- PTO0_ST1;
    pt.pto1 <- PTO1_ST1;
    pt.pto2 <- PTO2_ST1;
    pt.pto3 <- PTO3_ST1;
    pt.granule <- 16;
    pt.startl <- 0;
    pt.lastl <- 3;
    pt.pbpar <- ST1Param(pbpar); 
    pt.tpar <-  sample_tpar(); 
    pt.st1 <- true;

    make_tables(pt)
}

unit TEST_id_table_coarse_4K_ST1() = 
{
    var pt;

    var pbpar = sample_pbpar_ST1();
    pbpar.global <- false;

    pt.vbase <- 0x10000000000;
    pt.pbase <- 0;
    pt.m <- 40;
    pt.n <- 48;
    pt.pto0 <- PTO0_ST1;
    pt.pto1 <- PTO1_ST1;
    pt.pto2 <- PTO2_ST1;
    pt.pto3 <- PTO3_ST1;
    pt.granule <- 4;
    pt.startl <- 0;
    pt.lastl <- 2;
    pt.pbpar <- ST1Param(pbpar); 
    pt.tpar <-  sample_tpar(); 
    pt.st1 <- true;

    make_tables(pt)
}

unit TEST_id_table_coarse_16K_ST1() = 
{
    var pt;

    var pbpar = sample_pbpar_ST1();
    pbpar.global <- false;

    pt.vbase <- 0xFFF800000000;
    pt.pbase <- 0x000800000000;
    pt.m <- 35;
    pt.n <- 48;
    pt.pto0 <- PTO0_ST1;
    pt.pto1 <- PTO1_ST1;
    pt.pto2 <- PTO2_ST1;
    pt.pto3 <- PTO3_ST1;
    pt.granule <- 16;
    pt.startl <- 0;
    pt.lastl <- 2;
    pt.pbpar <- ST1Param(pbpar); 
    pt.tpar <-  sample_tpar(); 
    pt.st1 <- true;

    make_tables(pt)
}

unit TEST_id_table_coarse_64K_ST1() = 
{
    var pt;

    var pbpar = sample_pbpar_ST1();
    pbpar.global <- false;

    pt.vbase <- 0xC0DE00000000;
    pt.pbase <- 0x000100000000;
    pt.m <- 32;
    pt.n <- 48;
    pt.pto0 <- PTO0_ST1;
    pt.pto1 <- PTO1_ST1;
    pt.pto2 <- PTO2_ST1;
    pt.pto3 <- PTO3_ST1;
    pt.granule <- 64;
    pt.startl <- 1;
    pt.lastl <- 2;
    pt.pbpar <- ST1Param(pbpar); 
    pt.tpar <-  sample_tpar(); 
    pt.st1 <- true;

    make_tables(pt)
}

nat * nat TEST_config_pt_ST1 (g :: nat, n :: nat, coarse :: bool) = 
{
    ptp = config_pt(g, n, n, coarse, ST1Param(sample_pbpar_ST1()), sample_tpar(), true);
    return (ptp.startl, ptp.lastl)
}


nat * nat TEST_config_pt_ST2 (g :: nat, n :: nat, coarse :: bool) = 
{
    ptp = config_pt(g, n, n, coarse, ST1Param(sample_pbpar_ST1()), sample_tpar(), false);
    return (ptp.startl, ptp.lastl)
}



unit TEST_id_table_coarse_4K_ST2(n :: nat) = 
{
    var pbpar = sample_pbpar_ST2();
 
    var pt = config_pt(4, 40, n, true, ST2Param(pbpar), sample_tpar(), false);
   
    pt.vbase <- 0x10000000000;
    pt.pbase <- 0;
    pt.pto0 <- PTO0_ST2;
    pt.pto1 <- PTO1_ST2;
    pt.pto2 <- PTO2_ST2;
    pt.pto3 <- PTO3_ST2;

    make_tables(pt)
}


string TEST_px_4K (n :: nat, l :: nat, conc :: bool) = 
{
    va = '000000000111111111000000000111111111000000000000';

    return [px(va, l, 9, n, conc)]
}

string TEST_px_16K (n :: nat, l :: nat, conc :: bool) = 
{
    va = '011111111111000000000001111111111100000000000000';

    return [px(va, l, 11, n, conc)]
}


string TEST_px_64K (n :: nat, l :: nat, conc :: bool) = 
{
    va = '111111000000000000011111111111110000000000000000';

    return [px(va, l, 13, n, conc)]
}

string TEST_px_64Kb (n :: nat, l :: nat, conc :: bool) = 
{
    va = 0x0001C0000000;

    return [px(va, l, 13, n, conc)]
}



unit TEST_id_table_coarse_16K_ST2(n :: nat) = 
{
    var pbpar = sample_pbpar_ST2();
 
    var pt = config_pt(16, 35, n, true, ST2Param(pbpar), sample_tpar(), false);
   
    pt.vbase <- 0xFFF800000000;
    pt.pbase <- 0x000800000000;
    pt.pto0 <- PTO0_ST2;
    pt.pto1 <- PTO1_ST2;
    pt.pto2 <- PTO2_ST2;
    pt.pto3 <- PTO3_ST2;

    make_tables(pt)
}

unit TEST_id_table_std_ST2 = 
{
    var pbpar = sample_pbpar_ST2();
    pbpar.outer <- WriteBack;

    var pt = config_pt(16, 36, 36, true, ST2Param(pbpar), sample_tpar(), false);
   
    pt.vbase <- 0x0;
    pt.pbase <- 0x0;
    pt.pto0 <- PTO0_ST2;
    pt.pto1 <- PTO1_ST2;
    pt.pto2 <- PTO2_ST2;
    pt.pto3 <- PTO3_ST2;

    make_tables(pt)
}

unit TEST_id_table_std_ST1 = 
{
    var pbpar = sample_pbpar_ST1();

    var pt = config_pt(4, 32, 36, true, ST1Param(pbpar), sample_tpar(), true);

    pt.vbase <- 0;
    pt.pbase <- 0;
    pt.pto0 <- PTO0_ST1;
    pt.pto1 <- PTO1_ST1;
    pt.pto2 <- PTO2_ST1;
    pt.pto3 <- PTO3_ST1;
    pt.lastl <- 2;

    make_tables(pt)
}
