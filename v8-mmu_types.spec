-----------------------------------------------------------------------
-- This is a formal specification of the			     --
-- ARMv8 address translation mechnism and MMU			     --
-- 								     --
-- Author: Christoph Baumann, KTH CSC Stockholm			     --
-----------------------------------------------------------------------


-- Notes:
-- 2015-01-28   moved type definitions here

{-

val () = Runtime.LoadF "v8-base_types.spec, v8-mem_types.spec, v8-mmu_types.spec";

-}



-------------------
-- Model exceptions
-------------------

exception TRANSLATION_FAULT
exception ADDRESS_SIZE_FAULT
exception ACCESS_FLAG_FAULT
exception PERMISSION_FAULT
exception MMU_SCHED_FAULT


------------------------------------------------
-- Switches for implementation defined behaviour
------------------------------------------------

record MMU_IMPL_SWITCHES {
-- TxSZ value is bigger than 39: 1 -> translation fault; 0 -> behave as 39
IMPL_TxSZ_BIGGER_THAN_39_FAULT :: bool

-- TxSZ value is smaller than 16: 1 -> translation fault; 0 -> behave as 16
IMPL_TxSZ_LESS_THAN_16_FAULT :: bool

-- TTBR not aligned to first level PT size: 1 -> treat as zero; 0 -> corrupted translation
IMPL_TTBR_MISALIGNED_TREAT_AS_ZERO :: bool

-- TTBR lower bits: 1 -> read as zero; 0 -> read actual value
IMPL_TTBR_LOW_BITS_READ_AS_ZERO :: bool

-- default value for TG0 and TG1 fields if invalid choice or not implemented, assume 4KB granule for now
IMPL_TCR_TG_DEFAULT :: bits(2)

-- return actually used value for TG on read of TCR_ELx instead of programmed value
IMPL_TCR_READ_RETURNS_USED_TG :: bool

-- raise a translation fault if contiguous bit is set for a too big set of blocks
IMPL_CONTIGUOUS_BIT_FAULT :: bool

-- ST2:

-- VTCR.T0SZ value is smaller than 16: 1 -> translation fault; 0 -> behave as 16
IMPL_VT0SZ_LESS_THAN_16_FAULT :: bool

-- VTCR.T0SZ value is less than 16: treat as 16 for checking SL0
IMPL_VT0SZ_LESS_THAN_IS_16_FOR_SL0 :: bool

-- default value for TG0 if invalid choice or not implemented
IMPL_VTCR_TG_DEFAULT :: bits(2)
}


------------------------------
-- helper functions
------------------------------

bits(8) MAIRbyte (mair :: dword, i :: nat) = 
{
   j = i mod 8;
   return mair<8*(j+1)-1:8*j>
}

MemoryAttributes MAIRtoAttr (b::bits(8)) = 
{
    var attr::MemoryAttributes;
    t, outc, outhint, outtrans = match b<7:4>
    {
	case '00 00'   => MemType_Device, UNKNOWN,    UNKNOWN, UNKNOWN
        case '00 RW'   => MemType_Normal, MemAttr_WT, RW,      true
        case '01 00'   => MemType_Normal, MemAttr_NC, UNKNOWN, UNKNOWN
        case '01 RW'   => MemType_Normal, MemAttr_WB, RW,      true
        case '10 RW'   => MemType_Normal, MemAttr_WT, RW,      false
        case '11 RW'   => MemType_Normal, MemAttr_WB, RW,      false
    };
    attr.typ <- t;
    attr.outer.attrs <- outc;
    attr.outer.hints <- outhint;
    attr.outer.transient <- outtrans;

    if t == MemType_Device then
    {
	attr.device <- match b<3:0>
	{
	    case '0000' => DeviceType_nGnRnE
	    case '0100' => DeviceType_nGnRE
	    case '1000' => DeviceType_nGRE
	    case '1100' => DeviceType_GRE
	    case _      => UNKNOWN
	}
    }
    else
    {
	inc, inhint, intrans = match b<3:0>
	{
	    case '00 00'   => UNKNOWN,    UNKNOWN, UNKNOWN
	    case '00 RW'   => MemAttr_WT, RW,      true
	    case '01 00'   => MemAttr_NC, UNKNOWN, UNKNOWN
	    case '01 RW'   => MemAttr_WB, RW,      true
	    case '10 RW'   => MemAttr_WT, RW,      false
	    case '11 RW'   => MemAttr_WB, RW,      false
	};
	attr.inner.attrs <- inc;
	attr.inner.hints <- inhint;
	attr.inner.transient <- intrans
    };

    return attr
}

-- stage 1 translation attributes short format, copied from ARM documentation, depending on data cache enable bit
MemAttrHints ShortConvertAttrsHints (RGN :: bits(2), DCen :: bool) = 
{
    var result :: MemAttrHints;
    if !DCen then -- force non-cacheable
    {
	result.attrs <- MemAttr_NC;
	result.hints <- MemHint_No
    }
    else 
    {
	(att, hint) = match RGN 
	{
	    case '00' => MemAttr_NC, MemHint_No
	    case '01' => MemAttr_WB, MemHint_RWA
	    case '10' => MemAttr_WT, MemHint_RA
	    case '11' => MemAttr_WB, MemHint_RA
	};
	result.attrs <- att;
	result.hints <- hint
    };
    result.transient <- false;
    return result
}


bool list nullvec(n::nat) =
{
    if n==0 then Nil 
    else false @ nullvec(n-1)
}

bool list onevec(n::nat) =
{
    if n==0 then Nil 
    else true @ nullvec(n-1)
}

-- set lowest n bits to zero
ADR align(addr::ADR,n::nat) = 
{
    return [Take([48-n], [addr]::bool list) : nullvec(n)]
}


-- base address of PT needs to be aligned according to number of PT entries, corresponding to number of bits for first level px + 3 (desc size)
ADR page_table_origin (nflb::nat, ttbr :: TTBRType, maltaz :: bool) = 
{
    var baddr = ttbr.BADDR;
    when maltaz do baddr <- align(baddr,nflb+3);
    return baddr
}

-- number of additional low zero bits in stage 1 descriptor addresses depending on granule size
nat gran_bits (TG::bits(2)) = 
{
    match TG
    {
	case '00' => 0   -- 4K
	case '10' => 2   -- 16K
	case '01' => 4   -- 64K
	case _    => UNKNOWN
    }
}

-- PS / IPS field coding for (i)pa length, limit to implemented physical address length m
nat ps2len(ps::bits(3), m::nat) = 
{
   len = match ps
    {
	case '000'     => 32
	case '001'     => 36
	case '010'     => 40
	case '011'     => 42
	case '100'     => 44
        case _         => 48 -- incl case '101'
    };
   if (len > m) then m else len
}

-- length of virtual address
nat VAlen (TxSZ :: nat) = 64-TxSZ

-- number of bits in px used by contiguous pages/blocks
nat cont_bits(br::nat, l::nat) = match br
{
    case 9  => 4
    case 11 => if (l == 2) then 5 else 7 -- different for level 2 and 3 lookup in 16 KB granule
    case 13 => 5
    case _  => UNKNOWN	
}


-- number of lookups depending on VAlen n and b resolved bits per lookup 
------ granule sizes are 4KB, 16KB, 64KB
------ page descriptors are 8Byte long
------ one lookup resolves b = 9, 11, or 13 bits
------ number of lookups = max{l | l*b + 3 < n}
------ this is at most 4
nat nlu (n::nat,b::nat,conc::bool) = 
{
    var resolved = b+3;
    var lookups = 0n0;
    for i in 1 .. 4 do
    {
	when resolved < n do 
	{
	    resolved <- resolved + b;
	    lookups <- lookups + 1
	}
    };
    return lookups - [conc] 
}


-- get page table index from va for level l with parameters b and n
------ one lookup resolves b = 9, 11, or 13 bits, unless concatenated tables, then up to 4 more in first px
------ n = VAlen
------ lowest b+3 bits of va are offset into granule
------ levels l are counted from MSB, highest bits -> level 0
bool list px (va::ADR, l::nat, b::nat, n::nat, conc::bool) =
{
    nlow = (3-l)*b + (b+3);
    nup = if conc 
        then n
	else Min( b + nlow, n );
    return Take(nup-nlow,Drop(48-nup,[va]))
}

-- get page table byte index from va 
------ one lookup resolves br = 9, 11, or 13 bits of va
------ multiple levels resolved if block descriptor
------ add 3 bits for byte granularity
bool list bx (va::ADR,br::nat,l::nat,blck::bool) =  
{ 
    b = if blck then (4-l) * br else br;
    return Drop(48-(b+3), [va])
}


-- get number of bits used to index first level PT
------ max b+3 bits + max 4 if concatenated, less if cut off by n
------ number of entries in first level PT = 2^(n-3) (dword size)
nat nflb (n::nat,b::nat,conc::bool) = n - (nlu(n,b,conc)*b + 3)



-------------------
-- Tables and walks
-------------------

-- translation table walk abstraction
record Walk
{
-- fixed parameters after init
    va   :: VA                -- virtual address to be translated
    acctype :: AccType        -- type of access for which translation is needed
    wr   :: bool              -- translation is for write access
    n    :: nat               -- virtual address length
    m    :: nat               -- physical address length
    br   :: nat               -- bits resolved in each step (depending on granule size, 9, 11, or 13) ... gran_bits(TG)
    st   :: nat               -- stage of translation
    el   :: nat               -- exception level (needed for attribute lookup in stage 1, and permission check)
    up   :: bool              -- uses upper translation table (only for EL1)
    conc :: bool              -- save first stage by concatenating up to 16 page tables
-- parameters updated during walking
    adr  :: ADR               -- current lookup result
    l    :: nat               -- level of next lookup (0 .. 3)
    c    :: bool              -- complete walk (independent of level because of blocks)
    attr :: MemoryAttributes  -- accumulated attributes
    R    :: bool              -- 'readable' bit (writeonly memory for stage 2)
    W    :: bool              -- 'writable' bit
    U    :: bool              -- 'EL0 access' bit      
    XN   :: bool              -- accumulated unprivileged execute never bit 
    PXN  :: bool              -- accumulated privileged execute never bit (always 1 in EL1 & EL0 if AP = 01)
    A    :: bool              -- accessed flag
    NS   :: bool              -- targeting non-secure memory
}


-- page or block desciptor
register PBDescriptor :: dword
{
-- next four only for table descriptors
   63 : NST          -- Security state for next lookup
62-61 : APT          -- Access permissions for next lookups (X0 at EL2 and EL3, 1:read-only, 0:no EL0 access)
   60 : XNT          -- Execute Never limit for next nookups (only for stage 1)
   59 : PXNT         -- PXN Execute Never limit for next nookups (only for stage 1)
-- next three only for page/block descriptors
   54 : XN           -- Execute never bit for block/page
   53 : PXN          -- Privileged Execute never bit for block/page at EL1 (0 at EL2 and EL3)
   52 : Contiguous   -- hint that entry is part of contiguous set of entries
-- address present in all descriptors
47-12 : adr          -- block/page address or address of next pte (lower align_page bits are RAZ)
-- lower bits only for page/block descriptors
   11 : nG           -- not Global bit (shared memory or only for current ASID)
   10 : AF           -- Accessed flag
  9-8 : SH           -- Shareability field (00 Non, 10 Outer, 11 Inner)
  7-6 : AP           -- Access Permissions
--    5 : NS           -- non-Secure bit -- we do not model scure state here
  5-2 : Attr         -- Memory attributes field (stage 1: 4-2 pointer to MAIR, stage 2: direct encoding)
    1 : pte          -- type of descriptor (0 block, 1 pte/page)
    0 : v            -- valid bit
}


-- extract address from descriptor and align correctly according to TCR granule size, level, and type
ADR DESC_ADR (desc::PBDescriptor, br::nat, l::nat) =
{
    b = if desc.pte then br else (4-l) * br;
    return align(desc.adr : 0b0`12, b+3) --`
}

-- translation of stage2 attributes descriptor to memory attributes record
MemoryAttributes Stage2toAttr (b::bits(4)) = 
{
    var attr::MemoryAttributes;
    t, outc = match b<3:2>
    {
	case '00' => MemType_Device, UNKNOWN
        case '01' => MemType_Normal, MemAttr_NC
        case '10' => MemType_Normal, MemAttr_WT
        case '11' => MemType_Normal, MemAttr_WB
    };
    attr.typ <- t;
    attr.outer.attrs <- outc;

    if t == MemType_Device then
    {
	attr.device <- match b<1:0>
	{
	    case '00' => DeviceType_nGnRnE
	    case '01' => DeviceType_nGnRE
	    case '10' => DeviceType_nGRE
	    case '11' => DeviceType_GRE
	}
    }
    else
    {
	attr.inner.attrs <- match b<1:0>
	{
	    case '00'   => UNKNOWN
	    case '01'   => MemAttr_NC
	    case '10'   => MemAttr_WT
	    case '11'   => MemAttr_WB
	}
    };

    return attr
}


PBDescriptor cast_desc(w::dword) = 
{
    var desc::PBDescriptor;
    desc.NST  <- w<63>;
    desc.APT  <- w<62:61>;
    desc.XNT  <- w<60>;
    desc.PXNT <- w<59>;
    desc.XN   <- w<54>;
    desc.PXN  <- w<53>;
    desc.Contiguous <- w<52>;
    desc.adr  <- w<47:12>;
    desc.nG   <- w<11>;
    desc.AF   <- w<10>;
    desc.SH   <- w<9:8>;
    desc.AP   <- w<7:6>;
    desc.Attr <- w<5:2>;
    desc.pte  <- w<1>;
    desc.v    <- w<0>;
    return desc
}


-- walk init
-- wr: write
-- tr: translation regime
-- st: stage
Walk init_walk (va::VA, acctype::AccType, wr::bool, el::nat, st::nat, TG :: bits(2), PS :: bits(3), TSZ :: nat, att :: MemoryAttributes, pl :: nat, conc :: bool, ns :: bool, upper :: bool) = 
{
    var w :: Walk;
    w.va <- va;
    w.br <- 9+gran_bits(TG);
    w.m  <- ps2len(PS,pl);
    w.n  <- VAlen(TSZ);
    w.l  <- 4-nlu(w.n, w.br, conc);
    w.wr <- wr;
    w.acctype <- acctype;
    w.st <- st;
    w.el <- el;
    w.conc <- conc;
    w.up <- upper;

    w.c  <- false;
    -- attributes for table walking according to TCR for given Translation Regime tr, overridden by HCR_EL2.DC
    w.attr <- att;

    -- w.adr updated first after TTBR lookup
    
    -- initialize rights
    w.R <- true;
    w.W <- true;
    w.U <- true;    
    w.PXN <- false;
    w.XN <- false;

    w.NS <- ns;
   
    return w
}


FaultRecord NoFaultfromWalk (w::Walk) = 
{
    var fault = NoFault;
        fault.acctype <- w.acctype;
	fault.ipaddress <- if w.st == 1 then w.adr else [w.va];
	fault.s2fs1walk <- (w.st == 2 and w.acctype == AccType_PTW);
	fault.write <- w.wr;
	fault.level <- w.l;
	fault.secondstage <- (w.st == 2);
    
    return fault
}

-- get PBdescriptor address descriptor from walk
AddressDescriptor ADfromWalk(w::Walk) = 
{
    var fault = NoFaultfromWalk(w);
    fault.acctype <- AccType_PTW;
    fault.ipaddress <- if w.st == 1 then w.adr else [w.va];
    fault.write <- false;
    fault.extflag <- UNKNOWN;
    
    -- construct descriptor
    var ad :: AddressDescriptor;
    ad.fault <- fault;
    ad.memattrs <- w.attr;
    ad.paddress <- w.adr;

    return ad
}


FaultRecord check_address_size(fault :: FaultRecord, adr :: VA, m :: nat) = 
{
    var f = fault;
    
    when (Take(64-m, [adr]::bool list) != nullvec(64-m)) do { f.typ <- Fault_AddressSize; f.ipaddress <- adr<47:0> };

    return f
}



---------------------------
-- MMU configuration
---------------------------

-- states of ST1
construct ST_state { wait init mem walk final }
construct MMU_state { mmu_wait mmu_trans}


record MMU_config {
    -- implementation switches
    ISW :: MMU_IMPL_SWITCHES

    -- cached registers
    SCTLr :: SCTLRType
    TCR1  :: TCR_EL1
    TCR23 :: TCR_EL2_EL3
    TTBR  :: TTBRType
    MAIr  :: dword
    IDMM  :: mm_feat
    HCR :: HCR_EL2
    VTTBR :: TTBRType
    VTCR :: VTCR_EL2

    -- for ST1:
    ST1 :: ST_state
    -- current walk and fault for ST1, current address descriptor (for lookup or result)
    W1 :: Walk
    F1 :: FaultRecord
    SZ1 :: nat -- size of last request
    DESC1 :: AddressDescriptor

    -- for ST2:
    ST2 :: ST_state
    W2 :: Walk
    F2 :: FaultRecord
    DESC2 :: AddressDescriptor
    MEMREQ :: MEM_REQUEST
    ST2RPL :: MEM_REQUEST option
    DBG1 :: nat
    DBG2 :: MEM_REQUEST option
    
    S :: MMU_state
    CURR_REQ :: MEM_REQUEST
}

