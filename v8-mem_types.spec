-----------------------------------------------------------------------
-- This is an extension of:					     --	
-- 								     --
-- Formal Specification of the ARMv8-A instruction set architecture  --
-- (c) Anthony Fox, University of Cambridge                          --
-- 								     --
-- for introducing system level functionality 			     --
-- and a memory system interface				     --
-- Author: Christoph Baumann, KTH CSC Stockholm			     --
-----------------------------------------------------------------------

{-

val () = Runtime.LoadF "v8-mem_types.spec";

-}

-- maximal physical address size
type ADR = bits(48)
-- effective virtual address
type VA = bits(64)

bits(N) Align (w::bits(N), n::nat) = [n * ([w] div n)]

bool Aligned (w::bits(N), n::nat) = w == Align (w, n)

---------------------------
-- Memory type definitions
---------------------------

-- memory access types
construct AccType
{
   AccType_NORMAL AccType_VEC        -- Normal loads and stores
   AccType_STREAM AccType_VECSTREAM  -- Streaming loads and stores
   AccType_ATOMIC                    -- Atomic loads and stores
   AccType_ORDERED                   -- Load-Acquire and Store-Release
   AccType_UNPRIV                    -- Load and store unprivileged
   AccType_IFETCH                    -- Instruction fetch
   AccType_PTW                       -- Page table walk
   -- Other operations
   AccType_DC                        -- Data cache maintenance
   AccType_IC                        -- Instruction cache maintenance
   AccType_AT                        -- Address translation
   AccType_TLB                       -- TLB maintenance
   AccType_MB                        -- Memory Barrier
}

-- different kinds of memory faults
construct Fault {
  Fault_None
  Fault_AccessFlag
  Fault_Alignment 
  Fault_Background
  Fault_Domain
  Fault_Permission
  Fault_Translation 
  Fault_AddressSize
  Fault_SyncExternal
  Fault_SyncExternalOnWalk
  Fault_SyncParity
  Fault_SyncParityOnWalk
  Fault_AsyncParity
  Fault_AsyncExternal
  Fault_Debug
  Fault_TLBConflict
  Fault_Lockdown
  Fault_Coproc
  Fault_ICacheMaint
}

record FaultRecord {
  typ :: Fault			-- Fault Status
  acctype :: AccType		-- Type of access that faulted
  ipaddress :: bits(48)		-- Intermediate physical address
  s2fs1walk :: bool		-- Is on a Stage 1 page table walk
  write :: bool			-- "TRUE for a read, FALSE for a write" // WHAT??? I suppose this is a typo
  level :: nat			-- For translation, access flag and permission faults 
  extflag :: bits(1)		-- IMPLEMENTATION DEFINED syndrome for external aborts
  secondstage :: bool		-- Is a Stage 2 abort
--  domain :: bits(4)		-- Arch32 only
--  debugmoe :: bits(4)		-- Arch32 only
}

FaultRecord CreateFaultRecord(typ::Fault, ipaddress::bits(48), level::nat, acctype::AccType, iswrite::bool, extflag::bits(1), secondstage::bool, s2fs1walk::bool) =
{
    var rec :: FaultRecord;
    rec.typ <- typ;
    rec.ipaddress <- ipaddress;
    rec.level <- level;
    rec.acctype <- acctype;
    rec.write <- iswrite;
    rec.extflag <- extflag;
    rec.secondstage <- secondstage;
    rec.s2fs1walk <- s2fs1walk;
    return rec
}

FaultRecord NoFault = 
{
    ipaddress = UNKNOWN;
    level = 0;
    acctype = AccType_NORMAL;
    iswrite = UNKNOWN;
    extflag = '0';
    secondstage = false;
    s2fs1walk = false;
    return CreateFaultRecord(Fault_None, ipaddress, level, acctype, iswrite, extflag, secondstage, s2fs1walk)
}

-- memory and device types
construct MemType { MemType_Normal MemType_Device }
construct DeviceType { DeviceType_GRE DeviceType_nGRE DeviceType_nGnRE DeviceType_nGnRnE }

record MemAttrHints {
  attrs :: bits(2)		-- The possible encodings for each attributes field are as below
  hints :: bits(2) 	 	-- The possible encodings for the hints are below
  transient :: bool
}

-- The cacheability attributes are defined as follows:
bits(2) MemAttr_NC = 0b00		-- Non-cacheable
bits(2) MemAttr_WT = 0b10		-- Write-through
bits(2) MemAttr_WB = 0b11		-- Write-back

-- The allocation hints are defined as follows:
bits(2) MemHint_No = 0b00		-- No allocate
bits(2) MemHint_WA = 0b01		-- Write-allocate, Read-no-allocate
bits(2) MemHint_RA = 0b10		-- Read-allocate, Write-no-allocate
bits(2) MemHint_RWA = 0b11		-- Read-allocate and Write-allocate


record MemoryAttributes {
  typ :: MemType 		-- Memory type
  device :: DeviceType		-- For Device memory types
  inner :: MemAttrHints		-- Inner hints and attributes
  outer :: MemAttrHints		-- Outer hints and attributes
  shareable :: bool
  outershareable :: bool
}

-- set uncacheable memory attributes
MemoryAttributes set_NC (memattr :: MemoryAttributes) = 
{
    var attr = memattr;
    
    attr.inner.attrs <- MemAttr_NC;
    attr.inner.hints <- MemHint_No;
    attr.inner.transient <- false;
    attr.outer.attrs <- MemAttr_NC;
    attr.outer.hints <- MemHint_No;
    attr.outer.transient <- false;
    attr.shareable <- true;
    attr.outershareable <- true;

    return attr
}


-- no security bit here, added to mmu/memory request
type FullAddress = bits(48)


record AddressDescriptor {
  fault :: FaultRecord
  memattrs :: MemoryAttributes
  paddress :: FullAddress
}

bool IsFault(addrdesc::AddressDescriptor) = (addrdesc.fault.typ != Fault_None)


construct MBReqDomain
{
   MBReqDomain_OuterShareable MBReqDomain_Nonshareable
   MBReqDomain_InnerShareable MBReqDomain_FullSystem
}

construct MBReqTypes {MBReqTypes_Reads MBReqTypes_Writes MBReqTypes_All}

construct MemBarrierOp {MemBarrierOp_DSB MemBarrierOp_DMB MemBarrierOp_ISB}

construct TLBItype {TLBI_ALL, TLBI_VMID, TLBI_VMIDS12, TLBI_IPA, TLBI_ASID, TLBI_VA} 

construct SystemOp {
    SysOpNone,
    MemBar :: MemBarrierOp * MBReqDomain * MBReqTypes,
    IFlush :: bool * bool                          -- (all,IS)
    DFlush :: bool * bool * bool * bool            -- (inv, clean, setway, U)
    DZero,
    ATrans :: bool * bool * nat * bool             -- (S1, S2, EL, W)
    TLBI   :: TLBItype * nat * bool * bool * bool  -- (type, EL, last, allASID, IS)
}



--------------------------------------------------------------
-- memory requests
--------------------------------------------------------------

-- read and write signatures
type READ  = AddressDescriptor * nat * AccType 
type WRITE = AddressDescriptor * nat * AccType * bool list

declare SND_read :: READ -> nat
declare RCV_read :: nat -> (bool list * FaultRecord)

-- mmu request contains cached versions of all relevant system registers
record MMU_REGS {
  -- for ST1:
  sctlr :: SCTLRType
  ttbr :: TTBRType
  tcr1 :: TCR_EL1
  tcr23 :: TCR_EL2_EL3
  mair :: dword
  idmm :: mm_feat

  -- for ST2:
  hcr :: HCR_EL2
  vttbr :: TTBRType
  vtcr :: VTCR_EL2
}

-- reply contains address descriptor of translation including faults and data, original request for identification of answer
record MEM_REQUEST {
  va :: VA
  write :: bool
  acctype :: AccType 
  EL :: nat
  bytesize :: nat 
  desc :: AddressDescriptor
  data :: bool list
  ns :: bool
  sopar :: SystemOp
}

type MMU_REQUEST = MEM_REQUEST * MMU_REGS 

MEM_REQUEST get_memreq(req :: MEM_REQUEST option) = 
{
    var r;
    when IsSome(req) do {
	rq = ValOf(req);
	r <- rq
    };
    return r
}

MMU_REQUEST get_mmureq(req :: MMU_REQUEST option) = 
{
    var r;
    when IsSome(req) do {
	rq = ValOf(req);
	r <- rq
    };
    return r
}

-------------------
-- Trustzone ------
-------------------

record TrustZonePermission {
    SR :: bool
    SW :: bool
    NSR :: bool
    NSW :: bool
}


