-----------------------------------------------------------------------
-- This is an extension of:					     --	
-- 								     --
-- Formal Specification of the ARMv8-A instruction set architecture  --
-- (c) Anthony Fox, University of Cambridge                          --
-- 								     --
-- for introducing system level functionality 			     --
-- and a memory system interface				     --
-- Author: Christoph Baumann, KTH CSC Stockholm			     --
-----------------------------------------------------------------------


-- Notes:
-- 2014-11-05	moved to separate file
-- 2014-12-11   new memory interface functions

{-

val () = Runtime.LoadF "v8-base.spec, v8-id.spec, v8-mem_types.spec, v8-mem.spec";

-}


--------------------------------------------------------------
-- Core -> Memory interface for reads, writes, and address translation
--------------------------------------------------------------


-- assumed to be aligned
unit MemSingle (address::dword, size::nat, acctype::AccType, data :: (bool list) option) = 
{
      -- when address != Align(address, size) do #ALIGNMENT_FAULT
      -- TODO: ClearExclusive, p. 5145

      write = IsSome(data);

      var req;
      req.va <- address;
      req.write <- write;
      req.acctype <- acctype;
      req.EL <- [C.PSTATE.EL];
      req.bytesize <- size;
      when write do req.data <- BER(ValOf(data));
      req.ns <- !(req.EL == 3 or req.EL == 1 and !C.SCR_EL3.NS);
      req.sopar <- SysOpNone;
      
      C.CURR_REQ <- req
}

bool match_reply(reply :: MEM_REQUEST option, address :: VA, size :: nat, acctype :: AccType, data :: (bool list) option, EL :: nat, ns :: bool) = 
{
    if IsSome(reply) then {
	    rpl = ValOf(reply);
	    addr_match = (rpl.va == address);
	    size_match = (rpl.bytesize == size);
	    type_match = (rpl.acctype == acctype);
	    el_match = (rpl.EL == EL);
	    ns_match = (rpl.ns == ns);
	    -- if a write was requested the same data should have been written
	    data_match = match data {
		case None => !rpl.write
		case Some(d) => rpl.write and BER(rpl.data) == d};
	    return addr_match and size_match and type_match and el_match and ns_match and data_match}
    else return false
}

-- this is the heart of the Mem function that allows sequential programming of the instruction set
-- it can be used to issue requests in the ISSUE and MEM transitions or return the received values in FETCHDEC and WB
-- TODO: distinguish atomic and non-atomic accesses
-- TODO: extended later here to support multiple requests (prefetching not modelled here but via caches)
(bool list) handle_mem_access (address :: VA, size :: nat, acctype :: AccType, data :: (bool list) option) = 
{
    write = IsSome(data);
    C.CURR_FAULT <- NoFault;
    aligned = CheckAlignment (address, size, acctype, write);
    
    var value :: bool list;
       
    when C.CURR_FAULT.typ == Fault_None do {
	    -- if reply for earlier request was received then use this data
	    ns = !(C.PSTATE.EL == 3 or C.PSTATE.EL == 1 and !C.SCR_EL3.NS);
	    if match_reply(C.CURR_REPLY, address, size, acctype, data, [C.PSTATE.EL], ns) then {
		    value <- BER(ValOf(C.CURR_REPLY).data);
		    C.CURR_FAULT <- ValOf(C.CURR_REPLY).desc.fault;
		    C.CURR_REPLY <- None}
	    -- otherwise issue request 
	    else MemSingle(address, size, acctype, data)
    };

    return value
-- 
-- for i in 0 .. size - 1 do MemSingle (address + [i], 1, acctype) <- [value<8*i+7:8*i>]  
}

unit drop_result(data :: bool list) = nothing

component Mem (address::dword, size::nat, acctype::AccType) :: bool list  
{
    value = handle_mem_access(address, size, acctype, None)
    assign value = drop_result(handle_mem_access(address, size, acctype, Some(value)))
}
	

bool match_reply_sys(reply :: MEM_REQUEST option, req :: MEM_REQUEST) = 
{
    if IsSome(reply) then {
	    rpl = ValOf(reply);
	    addr_match = (rpl.va == req.va);
	    size_match = (rpl.bytesize == req.bytesize);
	    type_match = (rpl.acctype == req.acctype);
	    when rpl.acctype notin set { AccType_DC, AccType_IC, AccType_AT, AccType_TLB, AccType_MB } do #SYSOP_SCHEDULE_FAULT;
	    el_match = (rpl.EL == req.EL);
	    ns_match = (rpl.ns == req.ns);
	    sop_match = (rpl.sopar == req.sopar);
	    return addr_match and size_match and type_match and el_match and ns_match and sop_match}
    else return false
}

-- call this function for issuing and receiving AT requests, answers are decoded and PAR is updated
unit handle_at(req :: MEM_REQUEST) =
{
    when req.acctype != AccType_AT do #SYSOP_SCHEDULE_FAULT;
    C.CURR_FAULT <- NoFault;
    
    if match_reply_sys(C.CURR_REPLY, req) then {
	    rpl = ValOf(C.CURR_REPLY);
	    set_PAR(rpl.desc, rpl.ns);
	    C.CURR_REPLY <- None}
    -- otherwise issue request 
    else C.CURR_REQ <- req
}

-- call this function for issuing and receiving DC, IC and TLBI requests, returned faults are recorded
unit handle_cachetlb(req :: MEM_REQUEST) =
{
    when req.acctype notin set { AccType_DC, AccType_IC, AccType_TLB } do #SYSOP_SCHEDULE_FAULT;
    C.CURR_FAULT <- NoFault;
    
    if match_reply_sys(C.CURR_REPLY, req) then {
	    C.CURR_FAULT <- ValOf(C.CURR_REPLY).desc.fault;
	    C.CURR_REPLY <- None}
    -- otherwise issue request 
    else C.CURR_REQ <- req
}

unit MemBarrier (op :: MemBarrierOp, domain :: MBReqDomain, t :: MBReqTypes) =
{
    C.CURR_FAULT <- NoFault;
    
    var req;

    if C.S == core_mem then req <- C.CURR_REQ
    else {
	req.write <- false;
	req.acctype <- AccType_MB;
	req.EL <- [C.PSTATE.EL];
	req.bytesize <- 1;
	req.data <- Nil;
	req.ns <- !(req.EL == 3 or req.EL == 1 and !C.SCR_EL3.NS);
	req.sopar <- MemBar(op,domain,t)
    };

    when req.acctype != AccType_MB do #SYSOP_SCHEDULE_FAULT;
	 
    if match_reply_sys(C.CURR_REPLY, req) then {
	    C.CURR_REPLY <- None}
    -- otherwise issue request 
    else C.CURR_REQ <- req    

}

unit handle_sysop(sys_op1::bits(3), sys_op2::bits(3), sys_crn::bits(4), sys_crm::bits(4), has_result::bool, t::reg) =
{
    var req;
    if C.S == core_mem then req <- C.CURR_REQ
    else {
	req <- decode_sysop(sys_op1,sys_op2,sys_crn,sys_crm,has_result,t);
	trp = trap_sysop(req.sopar);
	when trp > 0 do {if trp == 4 then #TRAP_TO_EL2 else #UNDEFINED_FAULT ("InsufficientPrivileges")}
    };
    
    match req.acctype {
	case AccType_DC or AccType_IC or AccType_TLB => handle_cachetlb(req)
	case AccType_AT => handle_at(req)
	case _ => #SYSOP_SCHEDULE_FAULT
    }	  
}	 



-- TODO: PC wrap around issue 
-- TODO: semantics for DC, TLBI, barriers, same signatures as sequential models

