-----------------------------------------------------------------------
-- This is an extension of:					     --	
-- 								     --
-- Formal Specification of the ARMv8-A instruction set architecture  --
-- (c) Anthony Fox, University of Cambridge                          --
-- 								     --
-- for introducing system level functionality 			     --
-- and a memory system interface				     --
-- Author: Christoph Baumann, KTH CSC Stockholm			     --
-----------------------------------------------------------------------


-- Notes:
-- 2014-11-05	moved to separate file
-- 2014-12-11   new memory interface functions

{-

val () = Runtime.LoadF "v8-base.spec, v8-id.spec, v8-mem_types.spec, v8-memseq_types.spec, v8-mem.spec";
val () = Runtime.LoadF "v8-base.spec, v8-id.spec, v8-mem_types.spec, v8-memmsg_types.spec, v8-mem.spec";

-}

exception SYSOP_SCHEDULE_FAULT
exception RESERVED_CACHE_TYPE

---------------------
-- helper functions
---------------------

unit CheckSPAlignment =
{
   sp = SP`64;                --` 
   stack_align_check = if C.PSTATE.EL == 0 then C.SCTLR_EL1.SA0 else SCTLR([C.PSTATE.EL]).SA;
   when stack_align_check and not Aligned (sp, 16) do C.CURR_FAULT.typ <- Fault_Alignment
}


bool CheckAlignment
     (address::dword, size::nat, acctype::AccType, iswrite::bool) =
{
   aligned = Aligned (address, size);
   when not aligned and (acctype == AccType_ATOMIC or acctype == AccType_ORDERED or SCTLR(TranslationRegime).A) do {
      C.CURR_FAULT.write <- iswrite;
      C.CURR_FAULT.acctype <- acctype;
      C.CURR_FAULT.typ <- Fault_Alignment
   };
   return aligned
}

bool BigEndian = [if C.PSTATE.EL == 0 then C.SCTLR_EL1.E0E else SCTLR([C.PSTATE.EL]).EE]

(bool list) list ByteList (l::bool list) =
    match l
    {
       case Nil => Nil
       case b0 @ b1 @ b2 @ b3 @ b4 @ b5 @ b6 @ v7 @ rest =>
          list {b0, b1, b2, b3, b4, b5, b6, v7} @ ByteList (rest)
       case rest => list {rest}
    }

bool list BigEndianReverse (l::bool list) = Concat (Reverse (ByteList (l)))

bool list BER (l :: bool list) = if BigEndian then BigEndianReverse (l) else l

unit set_PAR(desc :: AddressDescriptor, ns :: bool) =
{
    if desc.fault.typ == Fault_None then {
	    C.PAR_EL1.F <- false;
	    o = desc.memattrs.outer;
	    i = desc.memattrs.inner;
	    var x = 0`8; --`
	    if desc.memattrs.typ == MemType_Device then {
		x<3:2> <- match desc.memattrs.device {
		    case DeviceType_nGnRnE => '00'
		    case DeviceType_nGnRE => '01'
		    case DeviceType_nGRE => '10'
		    case DeviceType_GRE => '11'
		};
		C.PAR_EL1.SH1PTW <- true;
		C.PAR_EL1.SH0 <- false
	    }
	    else {
		x<7:4> <- match o.attrs {
		    case '00' => '0100' -- NC
		    case '10' => if o.transient then '00':o.hints else '10':o.hints -- WT
		    case '11' => if o.transient then '01':o.hints else '11':o.hints -- WB
		    case '01' => #RESERVED_CACHE_TYPE
		};
		x<3:0> <- match i.attrs {
		    case '00' => '0100' -- NC
		    case '10' => if i.transient then '00':i.hints else '10':i.hints -- WT
		    case '11' => if i.transient then '01':i.hints else '11':i.hints -- WB
		    case '01' => #RESERVED_CACHE_TYPE
		};
		if x == '01000100' then {
			C.PAR_EL1.SH1PTW <- true;
			C.PAR_EL1.SH0 <- false
	        }
		else {
		        C.PAR_EL1.SH1PTW <- desc.memattrs.shareable;
			when desc.memattrs.shareable do C.PAR_EL1.SH0 <- !desc.memattrs.outershareable
		}		    
	    };
	    C.PAR_EL1.ATTR <- x;
	    C.PAR_EL1.PA <- desc.paddress<47:12>;
	    C.PAR_EL1.NSST <- ns
    }
    else {
	C.PAR_EL1.F <- true;
	C.PAR_EL1.SH1PTW <- (desc.fault.acctype == AccType_PTW);
	C.PAR_EL1.NSST <- desc.fault.secondstage;
	#NOT_MODELED("PAR_EL1.FST")
    }
}



MEM_REQUEST decode_sysop(sys_op1::bits(3), sys_op2::bits(3), sys_crn::bits(4), sys_crm::bits(4), has_result::bool, t::reg) =
{
    accty, sop = match has_result, sys_op1, sys_crn, sys_crm, sys_op2 {
        case true , _    , '1011', _     , _     => #NOT_MODELED ("ImplementationDefinedInstr")
        case true , _    , '1111', _     , _     => #NOT_MODELED ("ImplementationDefinedInstr")
        case true , _    , _     , _     , _     => #UNDEFINED_FAULT ("Unallocated")
	case false, '000', '0111', '0001', '000' => AccType_IC, IFlush(true,true)   -- IC IALLUIS
	case false, '000', '0111', '0101', '000' => AccType_IC, IFlush(true,false)  -- IC IALLU
	case false, '011', '0111', '0101', '001' => AccType_IC, IFlush(true,false)  -- IC IVAU
	case false, '000', '0111', '0110', '001' => if C.PSTATE.EL == 1 and C.HCR_EL2.VM and C.SCR_EL3.NS then
		  AccType_DC, DFlush(true,true,false,false) else AccType_DC, DFlush(true,false,false,false) -- DC IVAC
	case false, '000', '0111', '0110', '010' => if C.PSTATE.EL == 1 and (C.HCR_EL2.VM or C.HCR_EL2.SWIO) and C.SCR_EL3.NS then
		  AccType_DC, DFlush(true,true,true,false) else AccType_DC, DFlush(true,false,true,false)   -- DC ISW
	case false, '000', '0111', '1010', '010' => AccType_DC, DFlush(false,true,true,false)   -- DC CSW
	case false, '000', '0111', '1110', '010' => AccType_DC, DFlush(true,true,true,false)    -- DC CISW
	case false, '011', '0111', '1010', '001' => AccType_DC, DFlush(false,true,false,false)  -- DC CVAC
	case false, '011', '0111', '1011', '001' => AccType_DC, DFlush(false,true,false,true)   -- DC CVAU
	case false, '011', '0111', '1110', '001' => AccType_DC, DFlush(true,true,false,false)   -- DC CIVAC
	case false, '011', '0111', '0100', '001' => AccType_DC, DZero                           -- DC ZVA
	case false, '000', '0111', '1000', '000' => AccType_AT, ATrans(true,false,1,false)      -- AT S1E1R
	case false, '000', '0111', '1000', '001' => AccType_AT, ATrans(true,false,1,true)       -- AT S1E1W
	case false, '000', '0111', '1000', '010' => AccType_AT, ATrans(true,false,0,false)      -- AT S1E0R
	case false, '000', '0111', '1000', '011' => AccType_AT, ATrans(true,false,0,true)       -- AT S1E0W
	case false, '100', '0111', '1000', '000' => AccType_AT, ATrans(true,false,2,false)      -- AT S1E2R
	case false, '100', '0111', '1000', '001' => AccType_AT, ATrans(true,false,2,true)       -- AT S1E2W
-- TODO: model as S1 if EL2 disabled
	case false, '100', '0111', '1000', '100' => AccType_AT, ATrans(true,true,1,false)       -- AT S12E1R
	case false, '100', '0111', '1000', '101' => AccType_AT, ATrans(true,true,1,true)        -- AT S12E1W
	case false, '100', '0111', '1000', '110' => AccType_AT, ATrans(true,true,1,false)       -- AT S12E0R
	case false, '100', '0111', '1000', '111' => AccType_AT, ATrans(true,true,1,true)        -- AT S12E0W
	case false, '110', '0111', '1000', '000' => AccType_AT, ATrans(true,false,3,false)      -- AT S1E3R
	case false, '110', '0111', '1000', '001' => AccType_AT, ATrans(true,false,3,true)       -- AT S1E3W
	case false, '000', '1000', '0011', '000' => AccType_TLB, TLBI(TLBI_VMID,1,false,false,true)     -- TLBI VMALLE1IS
	case false, '000', '1000', '0011', '001' => AccType_TLB, TLBI(TLBI_VA,1,false,false,true)       -- TLBI VAE1IS
	case false, '000', '1000', '0011', '010' => AccType_TLB, TLBI(TLBI_ASID,1,false,false,true)     -- TLBI ASIDE1IS
	case false, '000', '1000', '0011', '011' => AccType_TLB, TLBI(TLBI_VA,1,false,true,true)        -- TLBI VAAE1IS
	case false, '000', '1000', '0011', '101' => AccType_TLB, TLBI(TLBI_VA,1,true,false,true)        -- TLBI VALE1IS
	case false, '000', '1000', '0011', '111' => AccType_TLB, TLBI(TLBI_VA,1,true,true,true)         -- TLBI VAALE1IS
	case false, '000', '1000', '0111', '000' => AccType_TLB, TLBI(TLBI_VMID,1,false,false,false)    -- TLBI VMALLE1
	case false, '000', '1000', '0111', '001' => AccType_TLB, TLBI(TLBI_VA,1,false,false,false)      -- TLBI VAE1
	case false, '000', '1000', '0111', '010' => AccType_TLB, TLBI(TLBI_ASID,1,false,false,false)    -- TLBI ASIDE1
	case false, '000', '1000', '0111', '011' => AccType_TLB, TLBI(TLBI_VA,1,false,true,false)       -- TLBI VAAE1
	case false, '000', '1000', '0111', '101' => AccType_TLB, TLBI(TLBI_VA,1,true,false,false)       -- TLBI VALE1
	case false, '000', '1000', '0111', '111' => AccType_TLB, TLBI(TLBI_VA,1,true,true,false)        -- TLBI VAALE1
	case false, '100', '1000', '0000', '001' => if C.SCR_EL3.NS then AccType_TLB, TLBI(TLBI_IPA,1,false,false,true) else
	     AccType_TLB, SysOpNone                                                                     -- TLBI IPAS2E1IS
	case false, '100', '1000', '0000', '101' => if C.SCR_EL3.NS then AccType_TLB, TLBI(TLBI_IPA,1,true,false,true) else
	     AccType_TLB, SysOpNone                                                                     -- TLBI IPAS2LE1IS
	case false, '100', '1000', '0011', '000' => AccType_TLB, TLBI(TLBI_ALL,2,false,false,true)      -- TLBI ALLE2IS
	case false, '100', '1000', '0011', '001' => AccType_TLB, TLBI(TLBI_VA,2,false,false,true)       -- TLBI VAE2IS
	case false, '100', '1000', '0011', '100' => AccType_TLB, TLBI(TLBI_ALL,1,false,false,true)      -- TLBI ALLE1IS
	case false, '100', '1000', '0011', '101' => AccType_TLB, TLBI(TLBI_VA,2,true,false,true)        -- TLBI VALE2IS
	case false, '100', '1000', '0011', '110' => AccType_TLB, TLBI(TLBI_VMIDS12,1,false,false,true)  -- TLBI VMALLS12E1IS
	case false, '100', '1000', '0100', '001' => if C.SCR_EL3.NS then AccType_TLB, TLBI(TLBI_IPA,1,false,false,false) else
	     AccType_TLB, SysOpNone                                                                     -- TLBI IPAS2E1
	case false, '100', '1000', '0100', '101' => if C.SCR_EL3.NS then AccType_TLB, TLBI(TLBI_IPA,1,true,false,false) else
	     AccType_TLB, SysOpNone                                                                     -- TLBI IPAS2LE1
	case false, '100', '1000', '0111', '000' => AccType_TLB, TLBI(TLBI_ALL,2,false,false,false)     -- TLBI ALLE2
	case false, '100', '1000', '0111', '001' => AccType_TLB, TLBI(TLBI_VA,2,false,false,false)      -- TLBI VAE2
	case false, '100', '1000', '0111', '100' => AccType_TLB, TLBI(TLBI_ALL,1,false,false,false)     -- TLBI ALLE1
	case false, '100', '1000', '0111', '101' => AccType_TLB, TLBI(TLBI_VA,2,true,false,false)       -- TLBI VALE2
	case false, '100', '1000', '0111', '110' => AccType_TLB, TLBI(TLBI_VMIDS12,1,false,false,false) -- TLBI VMALLS12E1
	case false, '110', '1000', '0011', '000' => AccType_TLB, TLBI(TLBI_ALL,3,false,false,true)      -- TLBI ALLE3IS
	case false, '110', '1000', '0011', '001' => AccType_TLB, TLBI(TLBI_VA,3,false,false,true)       -- TLBI VAE3IS
	case false, '110', '1000', '0011', '101' => AccType_TLB, TLBI(TLBI_VA,3,true,false,true)        -- TLBI VALE3IS
	case false, '110', '1000', '0111', '000' => AccType_TLB, TLBI(TLBI_ALL,3,false,false,false)     -- TLBI ALLE3
	case false, '110', '1000', '0111', '001' => AccType_TLB, TLBI(TLBI_VA,3,false,false,false)      -- TLBI VAE3
	case false, '110', '1000', '0111', '101' => AccType_TLB, TLBI(TLBI_VA,3,true,false,false)       -- TLBI VALE3		  
	case false, _    , _     , _     , _     => #UNDEFINED_FAULT ("Unallocated")
    };
    
    var req :: MEM_REQUEST;

    req.va <- X(t);
    req.write <- true;
    req.acctype <- accty;
    req.EL <- [C.PSTATE.EL];
    req.bytesize <- 1;
    req.data <- Nil;
    req.ns <- !(req.EL == 3 or req.EL == 1 and !C.SCR_EL3.NS);
    req.sopar <- sop;

    return req
}

nat trap_sysop(sop :: SystemOp) =
{
    EL0 = (C.PSTATE.EL == 0);
    EL1 = (C.PSTATE.EL == 1);
    EL2 = (C.PSTATE.EL == 2);
    EL10 = (C.PSTATE.EL < 2);
    EL1NS = (C.PSTATE.EL == 1 and C.SCR_EL3.NS);
    EL10NS = (EL10 and C.SCR_EL3.NS);
        
    match sop {
	case SysOpNone => 0
        case MemBar(_, _, _) => 0
        case IFlush(true,_) => if EL0 then 1 else {if EL1NS and C.HCR_EL2.TPU then 4 else 0}
	case IFlush(false,_) => if EL10NS and C.HCR_EL2.TPU then 4 else {if EL0 and !C.SCTLR_EL1.UCI then 1 else 0}
	case DFlush(_, _, true, _) =>  if EL0 then 1 else 0
	case DFlush(_, false, false, _) =>  if EL0 then 1 else 0
	case DFlush(_, true, false, _) =>  if EL0 and !C.SCTLR_EL1.UCI then 1 else 0
	case DZero => if EL10NS and C.HCR_EL2.TDZ then 4 else {if EL0 and !C.SCTLR_EL1.DZE then 1 else 0}
        case ATrans(s1,false,el,w) => if [C.PSTATE.EL] < el then {if !C.SCR_EL3.NS and EL1 then 3 else el+1} else 0 
        case ATrans(s1,true,el,w) => if EL10 then {if C.SCR_EL3.NS then 2 else 3} else 0 
	case TLBI(TLBI_IPA,_, _, _, _) => if EL10 then {if C.SCR_EL3.NS then 2 else 3} else 0
	case TLBI(TLBI_VMIDS12,_, _, _, _) => if EL10 then {if C.SCR_EL3.NS then 2 else 3} else 0
	case TLBI(TLBI_ALL,el, _, _, _) => if EL10 then {if C.SCR_EL3.NS then 2 else 3} else {if EL2 and el == 3 then 3 else 0}
	case TLBI(_,el, _, _, _) => if [C.PSTATE.EL] < el then {if !C.SCR_EL3.NS and EL1 then 3 else el+1} else
	     {if EL1NS and C.HCR_EL2.TTLB then 4 else 0}
    }    
}
