val integrity_mmu_fst_thm = prove(

``! req lookup mmu_state out_mmu out_mem consumed mmu_state'.
(mmu_state.MMU.CURR_REQ.EL < 2) ==>
(mmu_state.MMU.CURR_REQ.ns) ==>
(((out_mmu, out_mem, consumed),mmu_state') = MMU_ST1_sched (req,lookup) mmu_state) ==>
(
 (mmu_state'.MMU.CURR_REQ.EL < 2) /\
 (mmu_state'.MMU.CURR_REQ.ns) /\
 (mmu_state'.MMU.ST2 = mmu_state.MMU.ST2) /\
 (mmu_state'.MMU.S = mmu_state.MMU.S)
 (* /\ *)
 (* ((mmu_state'.MMU.ST1 ≠ mem ∧ *)
 (*  (mmu_state'.MMU.ST1 ≠ final ∨ *)
 (*   mmu_state'.MMU.F1.typ ≠ Fault_None)) ==> *)
 (*  (out_mem = NONE)) *)
)``
,

REPEAT (GEN_TAC)
THEN (REPEAT DISCH_TAC)
THEN (MY_LET_AND_EXEC2 [MMU_ST1_sched_def])
THEN (TERM_PAT_ASSUM_W_THM ``Abbrev (x = if cnd then a else b)`` (fn
      x::cnd::y => (Q.ABBREV_TAC `cnd=^cnd`)))

THEN (Cases_on `~cnd`)
THENL [
   (FULL_SIMP_TAC (srw_ss()) [])
   THEN (DEABBREV_AND_FIX_TAC)
   THEN (RW_TAC (srw_ss()) [])
, ALL_TAC]

THEN FST
THEN (Cases_on `mmu_state.MMU.ST1`)
THENL [
  FST
  THEN (Q.ABBREV_TAC `ns = (MMU_Init_ST1 (FST (get_mmureq req mmu_state)) mmu_state)`)
  THEN (MY_LET_AND_EXEC2 [MMU_Init_ST1_def])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) []),

  FST
  THEN (MY_LET_AND_EXEC2 [])
  THEN (Cases_on `(Disabled_ST1 () mmu_state)`)
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (Cases_on `v`)
  THENL [
      (Q.ABBREV_TAC `ns' = (MMU_FlatMap_ST1 () (SND (SND (SND s'))))`)
      THEN (MY_LET_AND_EXEC2 [MMU_FlatMap_ST1_def])
      THEN (DEABBREV_AND_FIX_TAC)
      THEN (RW_TAC (srw_ss()) [])
  ,ALL_TAC]
  THEN (Q.ABBREV_TAC `ns' = MMU_StartWalk_ST1 () (SND (SND (SND s')))`)
  THEN (MY_LET_AND_EXEC2 [MMU_StartWalk_ST1_def])
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) []),

  FST
  THEN (Q.ABBREV_TAC `ns' = MMU_FetchDescriptor_ST1 () mmu_state`)
  THEN (MY_LET_AND_EXEC2 [MMU_FetchDescriptor_ST1_def])
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) []),

  FST
  THEN (Q.ABBREV_TAC `ns' = MMU_ExtendWalk_ST1 (FST (get_memreq lookup mmu_state)) mmu_state`)
  THEN (MY_LET_AND_EXEC2 [MMU_ExtendWalk_ST1_def, get_memreq_def])
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (Q.ABBREV_TAC `lu = if IS_SOME lookup then THE lookup else ARB`)
  THEN (Cases_on `lu.desc.fault.typ <> Fault_None`)
  THENL [
    FST
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (RW_TAC (srw_ss()) [])
  , ALL_TAC]

  THEN FST
  THEN (Q.ABBREV_TAC `cast_v = (walk_extend (s'.MMU.W1, FST (cast_desc (v2w lu.data) s'')) s'')`)
  THEN (PairCases_on `cast_v`)
  THEN FST

  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) []),

  FST
  THEN (Q.ABBREV_TAC `ns' = MMU_Complete_ST1 () mmu_state`)
  THEN (MY_LET_AND_EXEC2 [MMU_Complete_ST1_def])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) [])
  THEN FST
]
);
