val mem_trace_def = Define `
  mem_trace m_state (n:num) o_mmu2mem o_mem2mmu o_mem2core =
  if n = 0 then (m_state, o_mmu2mem 0, o_mem2mmu 0, o_mem2core 0)
  else
  let (m_state, mmu2mem, mem2mmu, core_in) = mem_trace m_state (n-1) o_mmu2mem o_mem2mmu o_mem2core in
  let (m_state, mmu2mem, mem2mmu, core_in) = mem_step (m_state, o_mmu2mem n, o_mem2mmu n, o_mem2core n) in
  (m_state, mmu2mem, mem2mmu, core_in)
`;

