load "pairLib";
load "pairTools";
load "blastLib";
open pairLib;
open pairTools;

fun my_match pat term =
  let val (l,_) = match_term pat term in
      map (fn m => (#residue m)) l
  end;

val FIX_TUPLE_EQ_TAC = PAT_ASSUM ``(a,b) = c`` (fn thm =>
  ASSUME_TAC (SYM thm)
  THEN (FULL_SIMP_TAC (srw_ss()) [])
);



val SPLIT_ABBREVS = (PAT_ASSUM ``Abbrev(a /\ b)`` (fn thm=>
      let val thm1 = (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm)
	  val thm11 = CONJUNCT1 thm1
	  val thm12 = CONJUNCT2 thm1
          val thm21 = REWRITE_RULE [Once (ISPEC (concl thm11) (GSYM markerTheory.Abbrev_def))] thm11
	  val thm22 = REWRITE_RULE [Once (ISPEC (concl thm12) (GSYM markerTheory.Abbrev_def))] thm12
      in
      (ASSUME_TAC thm21)
      THEN (ASSUME_TAC thm22)
      end
       ));

val proof_free_vars = fn tac_fv => fn (asl,w)   =>
    let val fvs = free_vars w
        val fvs1 = List.concat (List.map free_vars asl)
        val tac:tactic = tac_fv (fvs@fvs1)
    in
	 tac(asl,w)
    end
;

val LET_ASS_TAC = PAT_ASSUM ``LET a b`` (fn thm =>
  let val (body,value) = dest_let (concl thm)  in
  proof_free_vars (fn vars =>
  let val var_name = variant vars (mk_var ("v", type_of value)) in
      if (List.length(pairLib.strip_prod (type_of value)) = 1) then
         ((Q.ABBREV_TAC `^var_name = ^value`)
	 THEN (ASSUME_TAC thm))
      else if (is_pair value) then
         (ASSUME_TAC (SIMP_RULE (srw_ss()) [Once LET_DEF] thm))
      else 
	  ((ASSUME_TAC thm)
          THEN (Q.ABBREV_TAC `^var_name = ^value`)
          THEN (pairLib.PairCases_on `^var_name`))
  end
  )
  end
);


val LET_EQ_ASS_TAC = PAT_ASSUM ``LET a b = v`` (fn thm =>
  let val (a,b) = dest_eq (concl thm)
      val (body,value) = dest_let a
  in
  proof_free_vars (fn vars =>
  let val var_name = variant vars (mk_var ("v", type_of value)) in
      if (List.length(pairLib.strip_prod (type_of value)) = 1) then
         ((Q.ABBREV_TAC `^var_name = ^value`)
	 THEN (ASSUME_TAC thm))
      else if (is_pair value) then
         (ASSUME_TAC (SIMP_RULE (srw_ss()) [Once LET_DEF] thm))
      else 
	  ((ASSUME_TAC thm)
          THEN (Q.ABBREV_TAC `^var_name = ^value`)
          THEN (pairLib.PairCases_on `^var_name`))
  end
  )
  end
);

val LET_EQ2_ASS_TAC = 

PAT_ASSUM ``Abbrev(x = (LET a b))`` (fn thm =>
  let val (a,b) = dest_eq (concl 
         (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm))
      val (body,value) = dest_let b
  in
  proof_free_vars (fn vars =>
  let val var_name = variant vars (mk_var ("v", type_of value)) in
      if (List.length(pairLib.strip_prod (type_of value)) = 1) then
	  ((ASSUME_TAC (SIMP_RULE (srw_ss()) [Once LET_DEF] thm))
	  THEN (Q.ABBREV_TAC `^var_name = ^value`))
      else if (is_pair value) then
         (ASSUME_TAC (SIMP_RULE (srw_ss()) [Once LET_DEF] thm))
      else 
	  (ASSUME_TAC thm)
          THEN (Q.ABBREV_TAC `^var_name = ^value`)
          THEN (pairLib.PairCases_on `^var_name`)
  end
  )
  end
)

;

val FIX_ABBREV_TAC =
  (PAT_ASSUM ``Abbrev(~a)`` (fn thm=>
      ASSUME_TAC (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm)))
  ORELSE SPLIT_ABBREVS
;

val FIX_ABBREV_TAC2 =
  EVERY_ASSUM (fn thm =>
     (let val l = my_match ``Abbrev(a)`` (concl thm)
	  val thm1 = (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm)
	  val t = concl thm1 in
	  if (is_neg t) then ASSUME_TAC thm1
	  else if (is_var t) then ASSUME_TAC thm1
	  else if (is_conj t) then
	      let val thm11 = CONJUNCT1 thm1
		  val thm12 = CONJUNCT2 thm1
		  val thm21 = REWRITE_RULE [Once (ISPEC (concl thm11) (GSYM markerTheory.Abbrev_def))] thm11
		  val thm22 = REWRITE_RULE [Once (ISPEC (concl thm12) (GSYM markerTheory.Abbrev_def))] thm12
	      in
		  (ASSUME_TAC thm21)
		      THEN (ASSUME_TAC thm22)
	      end
	  else ASSUME_TAC thm
      end) THEN (WEAKEN_TAC (equal (concl thm)))
      handle _ => ASSUME_TAC thm
);


val DEABBREV_AND_FIX_TAC = 
    (REPEAT (PAT_ASSUM ``Abbrev(a)`` (fn thm=>
      ASSUME_TAC (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm))))
;

val DEABBREV_AND_FIX_TUPLE_TAC = 
    (PAT_ASSUM ``Abbrev((a,b)=c)`` (fn thm=>
      ASSUME_TAC (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm)))
;




val LET_X_TO_ONE_TAC = fn let_term => fn thm =>
  let val (body,value) = dest_let let_term in
  if (not (is_abs body)) then FAIL_TAC "is not a single term"
    else
    let val (var, f) = dest_abs body in
         if (is_pair  var) then
    	  FAIL_TAC "not a single variable"
         else
    	  proof_free_vars (fn vars =>
               let val var = variant vars var in
    		ASSUME_TAC (SIMP_RULE (srw_ss()) [Once LET_DEF] thm)
    		THEN (Q.ABBREV_TAC `^var = ^value`)
    	    end)
     end
  end
;


val LET_TUPLE_TO_TUPLE_TAC = fn let_term => fn thm =>
  let val (body,value) = dest_let let_term
      val ([vars], body) = strip_pabs body;
      val var_list = strip_pair vars
      val val_list = strip_pair value
  in
      if ((List.length var_list) <> (List.length val_list)) then
	  FAIL_TAC "lists with different variables"
      else
	  ASSUME_TAC (SIMP_RULE (srw_ss()) [Once LET_DEF] thm)
  end
;

val LET_ONE_TO_TUPLE_TAC = fn let_term => fn thm =>
  let val (body,value) = dest_let let_term
      val ([vars], body) = strip_pabs body;
      val var_list = strip_pair vars
      val val_list = strip_pair value
  in
      if ((List.length val_list) <> 1) then
	  FAIL_TAC "right value is not a single value"
      else
	  proof_free_vars (fn all_vars =>
            let val var = variant all_vars (mk_var ("my_temp_var", type_of value)) 
	    in
	  ASSUME_TAC thm
          THEN (Q.ABBREV_TAC `^var=^value`)
	  THEN (pairLib.PairCases_on `^var`)
	  THEN (PAT_ASSUM ``Abbrev (a = ^var)`` (fn thm =>
             let val thm_no_abbrev = SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm
	  	 val (a,b) = dest_eq (concl thm_no_abbrev)
		 val (s,_) = match_term vars a
		 val val_list = strip_pair a
	     in
               ASSUME_TAC thm_no_abbrev
	       THEN (MAP_EVERY (fn v =>
                  Q.ABBREV_TAC `^(variant all_vars (#redex v))=^(#residue v)`
	       ) s)
	       THEN (MAP_EVERY (fn v =>
                       PAT_ASSUM ``Abbrev (x=y)`` (fn thm => ALL_TAC)
	       	     ) s)
	     end
          ))
	    end)
  end
;



val SIMPLE_LET_TAC = 
  PAT_ASSUM ``LET a b`` (fn thm =>
  let val a = concl thm in
  (LET_X_TO_ONE_TAC a thm)
  ORELSE (LET_TUPLE_TO_TUPLE_TAC a thm)
  ORELSE (LET_ONE_TO_TUPLE_TAC a thm)
  end
);


val SIMPLE_LET_EQ_ASS_TAC = PAT_ASSUM ``LET a b = v`` (fn thm =>
  let val (a,b) = dest_eq (concl thm) in
  (LET_X_TO_ONE_TAC a thm)
  ORELSE (LET_TUPLE_TO_TUPLE_TAC a thm)
  ORELSE (LET_ONE_TO_TUPLE_TAC a thm)
  end
);



val PAT_UNDISCH = fn pat => 
    (PAT_ASSUM pat
      (fn thm => (ASSUME_TAC thm) THEN (UNDISCH_TAC (concl thm))));

  
val PROVE_BY_BLAST =    ((fn (asl,w) =>
     let val t = blastLib.BBLAST_PROVE w
     in
	 ASSUME_TAC t (asl,w)
     end 
	)
   THEN (FULL_SIMP_TAC (srw_ss()) []));

val ABBREV_GOAL = (fn (asl,w) => (Q.ABBREV_TAC `goal=^w`)(asl,w));


val INVERT_ABBR_LET = (PAT_ASSUM ``Abbrev(a = LET b c)`` (fn thm => 
  let val thm1 = (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm) in
    ASSUME_TAC (SYM thm1)
  end));

val INVERT_LET_EQ = (PAT_ASSUM ``a = LET b c`` (fn thm => 
    ASSUME_TAC (SYM thm)
  ));

val SIMP_BY_PAT = (fn pat => (fn thms =>
    (PAT_ASSUM pat (fn thm =>
      (ASSUME_TAC (SIMP_RULE (srw_ss()) thms thm))
    ))));

val NO_SPLIT_TAC = PAT_ASSUM ``a \/ b`` (fn thm => 
  (ASSUME_TAC thm) THEN (Q.ABBREV_TAC `no_split=^(concl thm)`));



fun TERM_PAT_ASSUM pat term_tac  =
  (PAT_ASSUM pat (fn thm =>
      (term_tac (my_match pat (concl thm)))
      THEN (ASSUME_TAC thm)));

fun TERM_PAT_ASSUM_W_THM pat term_tac  =
  (PAT_ASSUM pat (fn thm =>
      (ASSUME_TAC thm)
      THEN (term_tac (my_match pat (concl thm)))
  ));

fun SPECL_ASSUM pat lst =
  (PAT_ASSUM pat (fn thm => 
    (ASSUME_TAC (SPECL lst thm))
  ));

fun SYM_ASSUM pat = 
  (PAT_ASSUM pat (fn thm => ASSUME_TAC (SYM thm)));



val MY_LET_NORMAL_FORM = 

(
  EVERY_ASSUM (fn thm =>
     (let val body::value::[] = my_match ``LET a b`` (concl thm) in
        if (is_abs body) then
	    let val (var, f) = dest_abs body in
                (* let x = f a b c in g(c) ==> Ab(x=f a b c) /\ g(c) *)
		if (not (is_pair  var)) then
		    (PAT_ASSUM (concl thm) (fn thm1 =>
                      LET_X_TO_ONE_TAC (concl thm) thm1))
		else ALL_TAC
	    end
        (* let (x,y) = f a b c in g(c) ==> (x,y)=f a b c /\ g(c) *)
	else (PAT_ASSUM (concl thm) (fn thm1 =>
               LET_ONE_TO_TUPLE_TAC (concl thm) thm1))
	     ORELSE (PAT_ASSUM (concl thm) (fn thm1 =>
               LET_TUPLE_TO_TUPLE_TAC (concl thm) thm1))
     end)
     handle _ =>
     (let val body::value::f::[] = my_match ``(LET a b) = c`` (concl thm)
	  val (a,b) = dest_eq (concl thm) in
        if (is_abs body) then
	    let val (var, f) = dest_abs body in
                (* let x = f a b c in g(c) ==> Ab(x=f a b c) /\ g(c) *)
		if (not (is_pair  var)) then
		    (PAT_ASSUM (concl thm) (fn thm1 =>
                      LET_X_TO_ONE_TAC a thm1))
		else ALL_TAC
	    end
        (* let (x,y) = f a b c in g(c) ==> (x,y)=f a b c /\ g(c) *)
	else (PAT_ASSUM (concl thm) (fn thm1 =>
               LET_ONE_TO_TUPLE_TAC a thm1))
	     ORELSE (PAT_ASSUM (concl thm) (fn thm1 =>
               LET_TUPLE_TO_TUPLE_TAC a thm1))
     end)
     handle _ => 
     (let val var::value::[] = my_match ``Abbrev(a=b)`` (concl thm) in
	  (* Abb((x,y) = v) ==> (x,y) = v *)
	  if ((is_pair var) andalso ((is_var value) orelse (is_const value))) then
	      (PAT_ASSUM (concl thm) (fn thm=>
               ASSUME_TAC (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm)))
	  (* Abb((x,y) = f v) ==> f v = (x,y) *)
	  else if (is_pair var) then
	      (PAT_ASSUM (concl thm) (fn thm=>
               ASSUME_TAC (SYM (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm))))
	  else 
	  (* Abb(x = let ....) ==> let .... = x *)
              (let val l = my_match ``Abbrev(x = (LET a b))`` (concl thm) in
               	 (PAT_ASSUM (concl thm) (fn thm=>
                  ASSUME_TAC (SYM (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm))))
               end)
	      handle _ => ALL_TAC
     end)
     handle _ => 
     (let val var::value::[] = my_match ``a=b`` (concl thm) in
	  (* (x,y) = f v ==> f v = (x,y) *)
	  if (is_pair var) andalso ((not (is_var value)) andalso (not (is_const value)) andalso (not (is_pair value)) ) then
	      (PAT_ASSUM (concl thm) (fn thm=>
               ASSUME_TAC (SYM thm)))
	  else ALL_TAC
     end)
     handle _ => ALL_TAC
))

;











val MY_LET_NORMAL_FORM2 = 

(
  FIRST_ASSUM (fn thm =>
     (let val body::value::[] = my_match ``LET a b`` (concl thm) in
        if (is_abs body) then
	    let val (var, f) = dest_abs body in
                (* let x = f a b c in g(c) ==> Ab(x=f a b c) /\ g(c) *)
		if (not (is_pair  var)) then
		    (PAT_ASSUM (concl thm) (fn thm1 =>
                      LET_X_TO_ONE_TAC (concl thm) thm1))
		else FAIL_TAC "Assignment to a tuple"
	    end
        (* let (x,y) = f a b c in g(c) ==> (x,y)=f a b c /\ g(c) *)
	else (PAT_ASSUM (concl thm) (fn thm1 =>
               LET_ONE_TO_TUPLE_TAC (concl thm) thm1))
	     ORELSE (PAT_ASSUM (concl thm) (fn thm1 =>
               LET_TUPLE_TO_TUPLE_TAC (concl thm) thm1))
     end)
     handle _ =>
     (let val body::value::f::[] = my_match ``(LET a b) = c`` (concl thm)
	  val (a,b) = dest_eq (concl thm) in
        if (is_abs body) then
	    let val (var, f) = dest_abs body in
                (* let x = f a b c in g(c) ==> Ab(x=f a b c) /\ g(c) *)
		if (not (is_pair  var)) then
		    (PAT_ASSUM (concl thm) (fn thm1 =>
                      LET_X_TO_ONE_TAC a thm1))
		else FAIL_TAC "Assignment to a tuple"
	    end
        (* let (x,y) = f a b c in g(c) ==> (x,y)=f a b c /\ g(c) *)
	else (PAT_ASSUM (concl thm) (fn thm1 =>
               LET_ONE_TO_TUPLE_TAC a thm1))
	     ORELSE (PAT_ASSUM (concl thm) (fn thm1 =>
               LET_TUPLE_TO_TUPLE_TAC a thm1))
     end)
     handle _ => 
     (let val var::value::[] = my_match ``Abbrev(a=b)`` (concl thm) in
	  (* Abb((x,y) = v) ==> (x,y) = v *)
	  if ((is_pair var) andalso ((is_var value) orelse (is_const value))) then
	      (PAT_ASSUM (concl thm) (fn thm=>
               ASSUME_TAC (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm)))
	  (* Abb((x,y) = f v) ==> f v = (x,y) *)
	  else if (is_pair var) then
	      (PAT_ASSUM (concl thm) (fn thm=>
               ASSUME_TAC (SYM (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm))))
	  else 
	  (* Abb(x = let ....) ==> let .... = x *)
              (let val l = my_match ``Abbrev(x = (LET a b))`` (concl thm) in
               	 (PAT_ASSUM (concl thm) (fn thm=>
                  ASSUME_TAC (SYM (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm))))
               end)
	      handle _ => FAIL_TAC "Unreducible abbreviation"
     end)
     handle _ => 
     (let val var::value::[] = my_match ``a=b`` (concl thm) in
	  (* (x,y) = f v ==> f v = (x,y) *)
	  if (is_pair var) andalso ((not (is_var value)) andalso (not (is_const value)) andalso (not (is_pair value)) ) then
	      (PAT_ASSUM (concl thm) (fn thm=>
               ASSUME_TAC (SYM thm)))
	  else FAIL_TAC "Unreducible equality"
     end)
     handle _ => FAIL_TAC "Unreducible term"
))

;

             
fun MY_LET_AND_EXEC2 thms =
  (REPEAT ((CHANGED_TAC MY_LET_NORMAL_FORM2)
           ORELSE (CHANGED_TAC 
             (RULE_ASSUM_TAC (SIMP_RULE (srw_ss()) thms)))
));

val FST = (FULL_SIMP_TAC (srw_ss()) []);

val FIX_CONST_EQ = RULE_ASSUM_TAC (fn thm =>
  if not( is_eq (concl thm)) then thm
  else if not( is_const (fst (dest_eq (concl thm)))) then thm
  else (SYM thm)
);
