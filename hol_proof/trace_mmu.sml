val mmu_trace_def = Define `
mmu_trace mmu_state (n:num)  o_core2mmu o_mem2mmu o_mmu2core o_mmu2mem = 
  if n = 0 then (mmu_state, o_core2mmu 0, o_mem2mmu 0, o_mmu2core 0, o_mmu2mem 0)
  else
  let (mmu_state, core2mmu, mem2mmu, mmu2core, mmu2mem) = mmu_trace mmu_state (n-1) o_core2mmu o_mem2mmu o_mmu2core o_mmu2mem in
  let (mmu_state, core2mmu, mem2mmu, mmu2core, mmu2mem) = mmu_step (mmu_state, o_core2mmu n, o_mem2mmu n, o_mmu2core n, o_mmu2mem n) in
  (mmu_state, core2mmu, mem2mmu, mmu2core, mmu2mem)
`;
