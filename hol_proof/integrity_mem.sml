HOL_Interactive.toggle_quietdec();
open wordsTheory;
HOL_Interactive.toggle_quietdec();

val mmu_mem_policy_def = Define `
  mmu_mem_policy mmu2mem mem2mmu policy policy_tbl GI =
      ((mmu2mem <> NONE) ==>
        ((THE mmu2mem).write) ==>
        (~ (policy ((THE mmu2mem).desc.paddress)))) /\
      ((mem2mmu <> NONE) ==>
        (policy ((THE mem2mmu).desc.paddress)) ==>
        ((THE mem2mmu).data = w2v (GI ((THE mem2mmu).desc.paddress)))
        ) /\
      ((mmu2mem <> NONE) ==>
        ((THE mmu2mem).acctype = AccType_PTW) ==>
	((THE mmu2mem).desc.fault.secondstage) ==>
        ((policy_tbl ((THE mmu2mem).desc.paddress)) /\
         (~(THE mmu2mem).write))
        )
`;


val mem_integrity_def = ``
!m_state (n:num) o_mmu2mem o_mem2mmu o_mem2core policy policy_tbl.
(!adr. policy_tbl adr ==> policy adr) ==>
(!n'. 
      (n' <= n) ==> (
      mmu_mem_policy (o_mmu2mem n') (o_mem2mmu n') policy policy_tbl m_state.PM
      )
) ==>
let s' = mem_trace m_state n o_mmu2mem o_mem2mmu o_mem2core in
let (m_state', mmu2mem', mem2mmu', mem2core') = s' in
(!adr.
   (policy adr) ==>
   ((m_state.PM adr) = (m_state'.PM adr))
) /\
((m_state'.MC.curr.write) ==>
        (~ (policy (m_state'.MC.curr.desc.paddress)))) /\
((m_state'.MC.curr.acctype = AccType_PTW) ==>
        (policy_tbl (m_state'.MC.curr.desc.paddress))) /\
(mmu_mem_policy mmu2mem' mem2mmu' policy policy_tbl m_state.PM)
``;

    
val mem_integrity_thm = prove(``^mem_integrity_def``,

REPEAT (GEN_TAC)
THEN (Induct_on `n`)
THENL [
  (FULL_SIMP_TAC (srw_ss()) [Once mem_trace_def])
  THEN (RW_TAC (srw_ss()) [])
  THEN cheat,
  ALL_TAC]

THEN (REPEAT DISCH_TAC)
(* First we show that the hypotesys hold for the induct hypo *)
THEN (TERM_PAT_ASSUM ``a ==> b ==> c`` (fn a::b::c::[] => 
      (`^a /\ ^b` by ALL_TAC)))
THENL [
  (RW_TAC (arith_ss) []), 
  ALL_TAC
]
THEN (REV_FULL_SIMP_TAC (srw_ss()) [])

THEN (ABBREV_GOAL)
THEN (MY_LET_AND_EXEC2 [])

THEN (TERM_PAT_ASSUM_W_THM ``d = goal:bool`` (fn d::[] => 
  (Q.ABBREV_TAC `goal1 = ^d`)))
THEN (RW_TAC (srw_ss()) [])

(* I open the definition of mem trace for n+1 steps *)
THEN (SIMP_BY_PAT ``mem_trace m_state (SUC n) o_mmu2mem o_mem2mmu o_mem2core = a`` [Once mem_trace_def])

THEN (MY_LET_AND_EXEC2 [mem_step_def, mem_interface_def, mem_sched_def])

THEN (TERM_PAT_ASSUM_W_THM ``Abbrev(x1 = if x2 ≠ NONE ∨ x3 ≠ mem_wait then x4  else x5)``
     (fn x1::x2::x3::x4::x5::[] => 
  (Q.ABBREV_TAC `cnd = (^x2 ≠ NONE ∨ ^x3 ≠ mem_wait)`)
))

(* We are waiting a message, but there is nothing in the box *)
THEN (Cases_on `~cnd`)
THENL [
  (* I had to add this line *)
  (FULL_SIMP_TAC (srw_ss()) [])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) []),
  ALL_TAC
]

THEN (FULL_SIMP_TAC (srw_ss()) [])
THEN (Cases_on `m_state'''.MC.S = mem_wait`)
(* We first manage the reception of a message from the mmu *)
THENL [
  (FULL_SIMP_TAC (srw_ss()) [rcv_def, LET_DEF])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) []),
  ALL_TAC
]

THEN (FULL_SIMP_TAC (srw_ss()) [])
THEN (`m_state'''.MC.S = mem_serve` by (Cases_on `m_state'''.MC.S`) THEN
       FULL_SIMP_TAC (srw_ss()) [])

(* problem when we assign tuple to var *)
THEN (Q.ABBREV_TAC `v4 = reply nofault m_state'''`)
THEN (MY_LET_AND_EXEC2 [reply_def])
THEN (RW_TAC (srw_ss()) [])

THEN (Cases_on `~v'''''.write`)
(* We are not writing data *)
THENL [
  (FULL_SIMP_TAC (srw_ss()) [])
  THEN (Cases_on `v'''''.bytesize = 1`) THENL [
    (FULL_SIMP_TAC (srw_ss()) [read_mem_def, LET_DEF])

val w2w_id_thm = blastLib.BBLAST_PROVE (``!x:word48. (w2w x) = x``);
val read1_thm = prove (``!m_state adr:word48.
(FOR (0,0, (λi state.((), w2v ((SND state).PM (n2w i + w2w adr)) ++ FST state,SND state))) ([],m_state)) =
((), w2v (m_state.PM(adr)), m_state)
``, 
(RW_TAC (srw_ss()) [])
THEN (EVAL_TAC)
THEN (RW_TAC (srw_ss()) [w2w_id_thm])
);

    THEN (FULL_SIMP_TAC (srw_ss()) [read1_thm])
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (FULL_SIMP_TAC (srw_ss()) [])

    THEN (Q.ABBREV_TAC `fail_cnd = ¬FST
              (TZ_check
                 (m_state''.MC.curr with
                  data :=
                    w2v (m_state''.PM m_state''.MC.curr.desc.paddress))
                 m_state'')`)

    THEN (Cases_on `fail_cnd`)
    THENL [
       (FULL_SIMP_TAC (srw_ss()) [])
       THEN (RW_TAC (srw_ss()) []),
       ALL_TAC
    ]

    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (RW_TAC (srw_ss()) []),
    
    ALL_TAC
  ]

  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN (Cases_on `v'''''.bytesize = 2`) THENL [

val w2w_id_thm = blastLib.BBLAST_PROVE (``!x:word48. (w2w x) = x``);
val read2_thm = prove (``
!m_state adr:word48.
(read_mem (0w:word16,adr) m_state) =
(w2v (((w2w(m_state.PM(adr+1w)):word16)<<8) !! w2w(m_state.PM(adr))), m_state)
``, 
(RW_TAC (srw_ss()) [])
THEN (FULL_SIMP_TAC (srw_ss()) [read_mem_def])
THEN (EVAL_TAC)
THEN (RW_TAC (srw_ss()) [w2w_id_thm])
THEN (blastLib.BBLAST_TAC)
);
    
    (FULL_SIMP_TAC (srw_ss()) [read2_thm, LET_DEF])
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (FULL_SIMP_TAC (srw_ss()) [])

    THEN (TERM_PAT_ASSUM_W_THM ``(MEM_FAULT_CASE (if y1 then y2 else y3) x2 x3 x4 x5 x6) = x7`` (
	  fn y1::y2::y3::x2::x3::x4::x5::x6::x7::[] =>
	     (Q.ABBREV_TAC `fail_cnd = ^y1`)
	 ))

    THEN (Cases_on `fail_cnd`)
    THENL [
       (FULL_SIMP_TAC (srw_ss()) [])
       THEN (RW_TAC (srw_ss()) []),
       ALL_TAC
    ]
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (RW_TAC (srw_ss()) [])

    ,ALL_TAC
  ]


  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN (Cases_on `v'''''.bytesize = 4`) THENL [

val w2w_id_thm = blastLib.BBLAST_PROVE (``!x:word48. (w2w x) = x``);
val read3_thm = prove (``
!m_state adr:word48.
(read_mem (0w:word32,adr) m_state) =
(w2v (
  ((w2w(m_state.PM(adr+3w)):word32)<<24) !! ((w2w(m_state.PM(adr+2w)):word32)<<16) !!
  ((w2w(m_state.PM(adr+1w)):word32)<<8) !! w2w(m_state.PM(adr))
), m_state)
``, 
(RW_TAC (srw_ss()) [])
THEN (FULL_SIMP_TAC (srw_ss()) [read_mem_def])
THEN (EVAL_TAC)
THEN (RW_TAC (srw_ss()) [w2w_id_thm])
THEN (blastLib.BBLAST_TAC)
);
    
    (FULL_SIMP_TAC (srw_ss()) [read3_thm, LET_DEF])
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (FULL_SIMP_TAC (srw_ss()) [])

    THEN (TERM_PAT_ASSUM_W_THM ``(MEM_FAULT_CASE (if y1 then y2 else y3) x2 x3 x4 x5 x6) = x7`` (
	  fn y1::y2::y3::x2::x3::x4::x5::x6::x7::[] =>
	     (Q.ABBREV_TAC `fail_cnd = ^y1`)
	 ))

    THEN (Cases_on `fail_cnd`)
    THENL [
       (FULL_SIMP_TAC (srw_ss()) [])
       THEN (RW_TAC (srw_ss()) []),
       ALL_TAC
    ]
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (RW_TAC (srw_ss()) [])

    ,ALL_TAC
  ]

  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN (Cases_on `v'''''.bytesize = 8`) THENL [

val w2w_id_thm = blastLib.BBLAST_PROVE (``!x:word48. (w2w x) = x``);
val read4_thm = prove (``
!m_state adr:word48.
(read_mem (0w:word64,adr) m_state) =
(w2v (
  ((w2w(m_state.PM(adr+7w)):word64)<<56) !! ((w2w(m_state.PM(adr+6w)):word64)<<48) !!
  ((w2w(m_state.PM(adr+5w)):word64)<<40) !! ((w2w(m_state.PM(adr+4w)):word64)<<32) !!
  ((w2w(m_state.PM(adr+3w)):word64)<<24) !! ((w2w(m_state.PM(adr+2w)):word64)<<16) !!
  ((w2w(m_state.PM(adr+1w)):word64)<<8) !! w2w(m_state.PM(adr))
), m_state)
``, 
(RW_TAC (srw_ss()) [])
THEN (FULL_SIMP_TAC (srw_ss()) [read_mem_def])
THEN (EVAL_TAC)
THEN (RW_TAC (srw_ss()) [w2w_id_thm])
THEN (blastLib.BBLAST_TAC)
);
    
    (FULL_SIMP_TAC (srw_ss()) [read4_thm, LET_DEF])
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (FULL_SIMP_TAC (srw_ss()) [])

    THEN (TERM_PAT_ASSUM_W_THM ``(MEM_FAULT_CASE (if y1 then y2 else y3) x2 x3 x4 x5 x6) = x7`` (
	  fn y1::y2::y3::x2::x3::x4::x5::x6::x7::[] =>
	     (Q.ABBREV_TAC `fail_cnd = ^y1`)
	 ))

    THEN (Cases_on `fail_cnd`)
    THENL [
       (FULL_SIMP_TAC (srw_ss()) [])
       THEN (RW_TAC (srw_ss()) []),
       ALL_TAC
    ]
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (RW_TAC (srw_ss()) [])

    ,ALL_TAC
  ]

  (FULL_SIMP_TAC (srw_ss()) [])
  THEN (Cases_on `v'''''.bytesize = 8`) THENL [

val w2w_id_thm = blastLib.BBLAST_PROVE (``!x:word48. (w2w x) = x``);
val read5_thm = prove (``
!m_state adr:word48.
(read_mem (0w:word128,adr) m_state) =
(w2v (
  ((w2w(m_state.PM(adr+15w)):word128)<<120) !! ((w2w(m_state.PM(adr+14w)):word128)<<112) !!
  ((w2w(m_state.PM(adr+13w)):word128)<<104) !! ((w2w(m_state.PM(adr+12w)):word128)<<96) !!
  ((w2w(m_state.PM(adr+11w)):word128)<<88) !! ((w2w(m_state.PM(adr+10w)):word128)<<80) !!
  ((w2w(m_state.PM(adr+9w)):word128)<<72) !! ((w2w(m_state.PM(adr+8w)):word128)<<64) !!
  ((w2w(m_state.PM(adr+7w)):word128)<<56) !! ((w2w(m_state.PM(adr+6w)):word128)<<48) !!
  ((w2w(m_state.PM(adr+5w)):word128)<<40) !! ((w2w(m_state.PM(adr+4w)):word128)<<32) !!
  ((w2w(m_state.PM(adr+3w)):word128)<<24) !! ((w2w(m_state.PM(adr+2w)):word128)<<16) !!
  ((w2w(m_state.PM(adr+1w)):word128)<<8) !! w2w(m_state.PM(adr))
), m_state)
``, 
(RW_TAC (srw_ss()) [])
THEN (FULL_SIMP_TAC (srw_ss()) [read_mem_def])
THEN (EVAL_TAC)
THEN (RW_TAC (srw_ss()) [w2w_id_thm])
THEN (blastLib.BBLAST_TAC)
);
    
    (FULL_SIMP_TAC (srw_ss()) [read4_thm, LET_DEF])
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (FULL_SIMP_TAC (srw_ss()) [])

    THEN (TERM_PAT_ASSUM_W_THM ``(MEM_FAULT_CASE (if y1 then y2 else y3) x2 x3 x4 x5 x6) = x7`` (
	  fn y1::y2::y3::x2::x3::x4::x5::x6::x7::[] =>
	     (Q.ABBREV_TAC `fail_cnd = ^y1`)
	 ))

    THEN (Cases_on `fail_cnd`)
    THENL [
       (FULL_SIMP_TAC (srw_ss()) [])
       THEN (RW_TAC (srw_ss()) []),
       ALL_TAC
    ]
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (RW_TAC (srw_ss()) [])

    ,ALL_TAC
  ]

]


THEN (FULL_SIMP_TAC (srw_ss()) [])

THEN (Cases_on `v'''''.bytesize = 1`) THENL [
    (FULL_SIMP_TAC (srw_ss()) [write_mem_def, LET_DEF])
    cheat
]

open bitstringTheory;

val t1 = ``(write_mem (0w:word8,w2w v'''''',v'''''.data) m_state'')``;
val (_, t2) = dest_eq (concl (SIMP_CONV (srw_ss()) [write_mem_def, listTheory.TAKE_def] t1));
val (_, t3) = dest_eq (concl (EVAL t2));
SIMP_CONV (srw_ss()) [write_mem_def, listTheory.TAKE_def] t3;

type_of ``v'''''.data``;
