open optionTheory;

val stupid_is_some_thm = prove(``!x. (IS_SOME x) ==> (x <> NONE)``,(FULL_SIMP_TAC (srw_ss()) []));
val stupid_is_some1_thm = prove(``!x. (x <> NONE) ==> (IS_SOME x) ``,
RW_TAC (srw_ss()) []
THEN (Cases_on `x`)
THENL [
  (FULL_SIMP_TAC (srw_ss()) []),
  (FULL_SIMP_TAC (srw_ss()) [])
]);




use "integrity_ST2_sched.sml";

val mmu_correctness_def = ``
!mmu_state core2mmu mem2mmu mmu2core mmu2mem .
mmu_state.MMU.CURR_REQ.EL < 2 ==>
mmu_state.MMU.CURR_REQ.ns ==>
((mmu_state.MMU.S = mmu_wait)  ==>
 ((mmu_state.MMU.ST2 = wait) /\
  (mmu_state.MMU.ST1 = wait))
) ==>
let s' = mmu_step (mmu_state, core2mmu, mem2mmu, mmu2core, mmu2mem) in
let (mmu_state', core2mmu', mem2mmu', mmu2core', mmu2mem') = s' in
((mmu_state'.MMU.S = mmu_wait)  ==>
 ((mmu_state'.MMU.ST2 = wait) /\
  (mmu_state'.MMU.ST1 = wait))
)
``;

``^mmu_correctness_def``;


REPEAT (GEN_TAC)
THEN (REPEAT DISCH_TAC)
THEN (ABBREV_GOAL)

THEN (MY_LET_AND_EXEC2 [mmu_step_def, mmu_interface_def])

(* We are waiting a message from the core but the channel is empty *)
THEN (Q.ABBREV_TAC `cnd = (SND s).MMU.S ≠ mmu_wait ∨ core2mmu ≠ NONE`)
THEN (Cases_on `~cnd`)
THENL [
  FST
  THEN (Cases_on `¬FST s''`)
  THENL [
    (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) THEN (DEABBREV_AND_FIX_TAC) THEN (RW_TAC (srw_ss()) []),
    (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) THEN (DEABBREV_AND_FIX_TAC) THEN (RW_TAC (srw_ss()) [])
  ],
  ALL_TAC
]

THEN FST

(* Thi case should be easy, since it either do not perform *)
(* a step or go in a state that is not constrained by the *)
(* goal *)
THEN (Cases_on `(SND s).MMU.S = mmu_wait`)
THENL [
  (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (Q.ABBREV_TAC `ns1 = (MMU_sched (core2mmu,mem2mmu)
				      (SND s with MMU := (SND s).MMU with
                                 <|CURR_REQ := icorer;
                                   S := mmu_trans|>))`)
  THEN (PairCases_on `ns1`)
  THEN FST

  (* we show that the global state is not changed by the scheduler *)
  THEN (`(ns13.MMU.S = mmu_trans) /\
         (ns13.MMU.CURR_REQ.EL < 2) /\
         (ns13.MMU.CURR_REQ.ns)` by ALL_TAC)
  THENL [
    (MY_LET_AND_EXEC2 [MMU_sched_def])
    THEN (Q.ABBREV_TAC `ns2 = MMU_ST1_sched (core2mmu,mem2mmu)
                                          (SND s with
                                           MMU :=
                                             (SND s).MMU with
                                             <|CURR_REQ := icorer;
                                               S := mmu_trans|>)`)
    THEN (PairCases_on `ns2`)
    THEN (FST)
    (* we first prove the non changed state fot the first state *)
    THEN (`(ns23.MMU.S = mmu_trans) /\
           (ns23.MMU.CURR_REQ.EL < 2) /\
           (ns23.MMU.CURR_REQ.ns)` by ALL_TAC)
    THENL [
       (TERM_PAT_ASSUM ``Abbrev (((a,b,c),d) = MMU_ST1_sched (e,f) g)`` (
	  fn a::b::c::d::e::f::g::[] =>
	     ASSUME_TAC (SPECL [e,f,g,a,b,c,d] integrity_mmu_fst_thm)
       ))
       THEN (`(icorer.EL < 2) /\ (icorer.ns)` by cheat)
       THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
       THEN FST,
       ALL_TAC]
    
    THEN (PairCases_on `s'''''`)
    THEN (`(s'''''3.MMU.S = mmu_trans) /\
           (s'''''3.MMU.CURR_REQ.EL < 2) /\
           (s'''''3.MMU.CURR_REQ.ns)` by ALL_TAC)
    THENL [
      (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
      THEN (DEABBREV_AND_FIX_TAC)
      THEN (RW_TAC (srw_ss()) []) 
      THEN (REV_FULL_SIMP_TAC (srw_ss()) []),
      ALL_TAC
    ]
    THEN FST
    (* first stage done. We must handle the second stage *)

    THEN (Q.ABBREV_TAC `ns3 = MMU_ST2_sched (mem2mmu, s'''''2) s'''''3`)
    THEN (PairCases_on `ns3`)
    THEN (FST)
    THEN (`(ns32.MMU.S = mmu_trans) /\
           (ns32.MMU.CURR_REQ.EL < 2) /\
           (ns32.MMU.CURR_REQ.ns)` by ALL_TAC)
    THENL [
       (TERM_PAT_ASSUM ``Abbrev (((a,b),c) = MMU_ST2_sched (d,e) f)`` (
	  fn a::b::c::d::e::f::[] =>
	     ASSUME_TAC (SPECL [d,e,f,a,b,c] integrity_mmu_snd_thm)
       ))
       THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
       THEN FST,
       ALL_TAC
    ]


    
  ]
  



  (* Case of no_step       *)
  THEN (Cases_on `core2mmu = NONE`)
  THENL [
    FST
    THEN DEABBREV_AND_FIX_TAC
    THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
    THEN (RW_TAC (srw_ss()) [])
  , ALL_TAC]

  THEN FST
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (Q.ABBREV_TAC `ns = (MMU_sched (core2mmu,mem2mmu)
				      (SND s with MMU := (SND s).MMU with
                                 <|CURR_REQ := icorer;
                                   S := mmu_trans|>))`)
  THEN (PairCases_on `ns`)
  THEN FST
  THEN (MY_LET_AND_EXEC2 [MMU_sched_def])
  (* We first show that we started in mmu_wait state *)
  THEN (`mmu_state.MMU.S=mmu_wait` by ALL_TAC)
  THENL [
    DEABBREV_AND_FIX_TAC
    THEN (RW_TAC (srw_ss()) [])
    THEN FST
  , ALL_TAC]
  THEN FST

  THEN (`(SND s).MMU.ST2 = wait2` by ALL_TAC)
  THENL [
    DEABBREV_AND_FIX_TAC
    THEN (RW_TAC (srw_ss()) [])
    THEN FST
  , ALL_TAC]
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])

  THEN (PairCases_on `s`)
  THEN FST

  THEN (Q.ABBREV_TAC `ns_1 = (MMU_ST1_sched (core2mmu,mem2mmu)
                        (s1'''''''''' with
                         MMU :=
                           s1''''''''''.MMU with
                           <|CURR_REQ := icorer; S := mmu_trans|>))`)
  THEN (PairCases_on `ns_1`)
  THEN FST

  THEN (`(ns_13.MMU.ST2 = wait2) /\ (ns_13.MMU.ST1 = init) /\
         (ns_13.MMU.S = mmu_trans)` by ALL_TAC)
  THENL [
     (MY_LET_AND_EXEC2 [MMU_ST1_sched_def])
     (* We first show that the mmmu is in wait stage *)
     THEN (`s1''''''''''.MMU.ST1= wait` by ALL_TAC)
     THENL [
       DEABBREV_AND_FIX_TAC
       THEN (RW_TAC (srw_ss()) [])
       THEN FST
     , ALL_TAC]
     THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
     THEN (Q.ABBREV_TAC `ns_2 = (MMU_Init_ST1
                (FST
                   (get_mmureq s0'
                      (s1'''''''''' with
                       MMU :=
                         s1''''''''''.MMU with
                         <|CURR_REQ := icorer; S := mmu_trans|>)))
                (s1'''''''''' with
                 MMU :=
                   s1''''''''''.MMU with
                   <|CURR_REQ := icorer; S := mmu_trans|>))`)
     THEN (MY_LET_AND_EXEC2 [MMU_Init_ST1_def])
     THEN (DEABBREV_AND_FIX_TAC)
     THEN (ABBREV_GOAL)
     THEN (RW_TAC (srw_ss()) [])
     THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
     THEN (DEABBREV_AND_FIX_TAC)
     THEN (FST)
  , ALL_TAC]

  THEN FST
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) [])

, ALL_TAC]


THEN (Cases_on `(SND s).MMU.S`)
THENL [FST, FST]


(* First we show that we go in a state that is mmu_wait only if o_mmu != None *)
THEN (`SND s = mmu_state` by (RW_TAC (srw_ss()) []))
THEN FST
THEN (Q.ABBREV_TAC `ns = MMU_sched  (core2mmu,mem2mmu) mmu_state`)
THEN (PairCases_on `ns`)
THEN FST
THEN (Cases_on `ns0 = NONE`)
THENL [

  (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  (* we show that the global state is not changed by the scheduler *)
  THEN (`(ns3.MMU.S = mmu_state.MMU.S) /\
         (ns3.MMU.CURR_REQ.EL < 2) /\
         (ns3.MMU.CURR_REQ.ns)` by ALL_TAC)
  THENL [
    (MY_LET_AND_EXEC2 [MMU_sched_def])
    THEN (Q.ABBREV_TAC `ns_1 = MMU_ST1_sched (core2mmu,mem2mmu) mmu_state`)
    THEN (PairCases_on `ns_1`)
    THEN (FST)
    (* we first prove the non changed state fot the first state *)
    THEN (`(ns_13.MMU.S = mmu_state.MMU.S) /\
           (ns_13.MMU.CURR_REQ.EL < 2) /\
           (ns_13.MMU.CURR_REQ.ns)` by ALL_TAC)
    THENL [
       (TERM_PAT_ASSUM ``Abbrev (((a,b,c),d) = MMU_ST1_sched (e,f) g)`` (
	  fn a::b::c::d::e::f::g::[] =>
	     ASSUME_TAC (SPECL [e,f,g,a,b,c,d] integrity_mmu_fst_thm)
       ))
       THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
       THEN FST,
       ALL_TAC]
    THEN (PairCases_on `s'''''`)
    THEN (`(s'''''3.MMU.S = mmu_state.MMU.S) /\
           (s'''''3.MMU.CURR_REQ.EL < 2) /\
           (s'''''3.MMU.CURR_REQ.ns)` by ALL_TAC)
    THENL [
      (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
      THEN (DEABBREV_AND_FIX_TAC)
      THEN (RW_TAC (srw_ss()) []) 
      (* warning RW_TAC generates three sub goals, so the next is repeat for each goal *)
      THEN ((Cases_on `(SND s).MMU.ST2 = wait2`)
            THENL [FST, FST])
      ,
      ALL_TAC
    ]
    THEN FST
    
    (* first stage done. We must handle the second stage *)
    THEN (Q.ABBREV_TAC `ns_2 = MMU_ST2_sched mem2mmu s'''''3`)
    THEN (PairCases_on `ns_2`)
    THEN (FST)
    THEN (`(ns_22.MMU.S = mmu_state.MMU.S) /\
           (ns_22.MMU.CURR_REQ.EL < 2) /\
           (ns_22.MMU.CURR_REQ.ns)` by ALL_TAC)
    THENL [
       (TERM_PAT_ASSUM ``Abbrev (((a,b),c) = MMU_ST2_sched d e)`` (
	  fn a::b::c::d::e::[] =>
	     ASSUME_TAC (SPECL [d,e,a,b,c] integrity_mmu_snd_thm)
       ))
       THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
       THEN FST,
       ALL_TAC
    ]
    
    THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (RW_TAC (srw_ss()) []),
    ALL_TAC
  ]

  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) []),

  ALL_TAC
]

THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
THEN (`(ns3.MMU.ST2 = wait2) ∧ (ns3.MMU.ST1 = wait)` by ALL_TAC)
THENL [
   (* Remove unused assumptions *)
   (PAT_ASSUM ``Abbrev (s'' = (ns2,a1,a2,a3,a4,a5))`` (fn thm=>ALL_TAC))
   THEN  (MY_LET_AND_EXEC2 [MMU_sched_def])

   (* the second stage does no step *)
   THEN (Cases_on `~v'`)
   THENL [ 
     FST
     


   ]


]








THEN (MY_LET_AND_EXEC2 [MMU_sched_def])

THEN (Q.ABBREV_TAC `ns_1 = MMU_ST1_sched (core2mmu,mem2mmu) mmu_state`)
THEN (PairCases_on `ns_1`)
THEN FST

(* WThis is the output of the first stage *)
THEN (PairCases_on `s'''''''`)

(* If we output something from the mmu, then *)
(* the first stage returned in the initial state. *)
(* Moreover, we do not change the state of the global automaton *)
THEN (` (s'''''''3.MMU.S = mmu_trans) /\
        ((s'''''''1 <> NONE) ==> (s'''''''3.MMU.ST1 = wait)) /\
        (s'''''''3.MMU.ST2 = mmu_state.MMU.ST2)
      ` by ALL_TAC)
THENL [
  (Cases_on `mmu_state.MMU.ST2 <> wait2`)
  THENL [
    FST
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (RW_TAC (srw_ss()) [])
  , ALL_TAC]

  (* THIS must be strongly automated *)

  THEN (MY_LET_AND_EXEC2 [MMU_ST1_sched_def])
  THEN (Cases_on `mmu_state.MMU.ST1`)
  THENL [
     (FULL_SIMP_TAC (srw_ss()) [MMU_Init_ST1_def, LET_DEF])
     THEN (Cases_on `core2mmu = NONE`)
     THENL [
       (DEABBREV_AND_FIX_TAC)
       THEN (RW_TAC (srw_ss()) [])
       THEN FST
     , ALL_TAC]

     THEN (DEABBREV_AND_FIX_TAC)
     THEN (RW_TAC (srw_ss()) [])
     THEN FST
     THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
     THEN (RW_TAC (srw_ss()) []),

     (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
     THEN (Cases_on `FST (Disabled_ST1 () mmu_state)`)
     THENL [
       (FULL_SIMP_TAC (srw_ss()) [MMU_FlatMap_ST1_def, LET_DEF])
       THEN (DEABBREV_AND_FIX_TAC)
       THEN (RW_TAC (srw_ss()) [])
       THEN FST
       THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
       THEN (RW_TAC (srw_ss()) [])
     , ALL_TAC]

     THEN FST
     THEN (Q.ABBREV_TAC `ns_2 = (MMU_StartWalk_ST1 () mmu_state)`)
     THEN (MY_LET_AND_EXEC2 [MMU_StartWalk_ST1_def])
     THEN (Cases_on `f.typ <> Fault_None`)
     THENL [
       (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
       THEN (DEABBREV_AND_FIX_TAC)
       THEN (RW_TAC (srw_ss()) [])
       THEN FST
       THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
       THEN (RW_TAC (srw_ss()) [])
     , ALL_TAC]

     THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
     THEN (DEABBREV_AND_FIX_TAC)
     THEN (RW_TAC (srw_ss()) [])
     THEN FST
     THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
     THEN (RW_TAC (srw_ss()) []),

     (FULL_SIMP_TAC (srw_ss()) [LET_DEF, MMU_FetchDescriptor_ST1_def])
     THEN (DEABBREV_AND_FIX_TAC)
     THEN (RW_TAC (srw_ss()) [])
     THEN FST
     THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
     THEN (RW_TAC (srw_ss()) []),

     (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
     THEN (Cases_on `mem2mmu = NONE`)
     THENL [
          (DEABBREV_AND_FIX_TAC)
          THEN (RW_TAC (srw_ss()) [])
          THEN FST
     , ALL_TAC]
     
     THEN (Q.ABBREV_TAC `ns_2 = (MMU_ExtendWalk_ST1 (FST (get_memreq mem2mmu mmu_state)) mmu_state)`)
     THEN (MY_LET_AND_EXEC2 [MMU_ExtendWalk_ST1_def])
     THEN (Cases_on `(FST (get_memreq mem2mmu mmu_state)).desc.fault.typ <> Fault_None`)
     THENL [
        (DEABBREV_AND_FIX_TAC)
        THEN (RW_TAC (srw_ss()) [])
        THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
        THEN (RW_TAC (srw_ss()) [])
	THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
        THEN (RW_TAC (srw_ss()) []),
     ALL_TAC]

     THEN (FST)
     THEN (MY_LET_AND_EXEC2 [])
     THEN (Cases_on `~((f.typ = Fault_None) ∧ ¬w.c)`)
     THENL [
        (DEABBREV_AND_FIX_TAC)
        THEN (RW_TAC (srw_ss()) [])
        THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
        THEN (RW_TAC (srw_ss()) [])
	THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
        THEN (RW_TAC (srw_ss()) []),
     ALL_TAC]

     THEN (FST)
     THEN (DEABBREV_AND_FIX_TAC)
     THEN (RW_TAC (srw_ss()) [])
     THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
     THEN (RW_TAC (srw_ss()) [])
     THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
     THEN (RW_TAC (srw_ss()) []),

     (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
     THEN (Q.ABBREV_TAC `ns_2 = (MMU_Complete_ST1 () mmu_state)`)
     THEN (MY_LET_AND_EXEC2 [MMU_Complete_ST1_def])
     THEN (DEABBREV_AND_FIX_TAC)
     THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
     THEN (RW_TAC (srw_ss()) [])
  ]
, ALL_TAC]

THEN (FST)
THEN (Q.ABBREV_TAC `ns_2 = MMU_ST2_sched mem2mmu s'''''''3`)
THEN (Cases_on `~v''''''`)

THENL [
  (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) [])
  THEN FST
  THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
, ALL_TAC]

THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
THEN (MY_LET_AND_EXEC2 [MMU_ST2_sched_def])

THEN (`(SND s''''''').MMU.ST2 = mmu_state.MMU.ST2` by ALL_TAC)
THENL [
  (DEABBREV_AND_FIX_TAC)
  THEN FST,
ALL_TAC]
THEN FST

THEN (Cases_on `mmu_state.MMU.ST2`)
THENL [
  (* wait2 *)
  FST
  THEN (Q.ABBREV_TAC `ns_3 = (MMU_Init_ST2 (s'''''''3.MMU.ST1 = mem) (SND s'''''''))`)
  THEN (PairCases_on `ns_3`)
  THEN FST

  THEN (MY_LET_AND_EXEC2 [MMU_Init_ST2_def])

  (* To use ``(SND s''''''').MMU.ST2 = wait2`` *)
  THEN (REV_FULL_SIMP_TAC (srw_ss()) [])

  THEN (Q.ABBREV_TAC `res_iwst2 = (InitWalk_ST2
                   (w2w s'''''''''''.MMU.DESC1.paddress,
                    if s'''''''3.MMU.ST1 = mem then AccType_PTW
                    else s'''''''''''.MMU.W1.acctype,
                    s'''''''''''.MMU.W1.wr,s'''''''''''.MMU.W1.el)
                   s''''''''''')`)
  THEN (`ns_31.MMU.ST2 = init2` by ALL_TAC)
  THENL [
     (RW_TAC (srw_ss()) []),
     ALL_TAC
  ]
  THEN (Q.UNABBREV_TAC `s1''''''''''''`) THEN FST
  THEN (SYM_ASSUM ``(NONE,FST s''''''',ns_31) = s1'''''''''''``) THEN FST
  THEN (SYM_ASSUM ``(FST s''''''',ns_31) = s1''''''''''``) THEN FST
  THEN (SYM_ASSUM ``(r'''''''''',ns_31) = ns_2``) THEN FST
  THEN (PairCases_on `r''''''''''`) THEN FST
  THEN (Q.UNABBREV_TAC `s1'''''''''`) THEN FST
  THEN (SYM_ASSUM ``(T,s'''''''0,s'''''''1,r''''''''''0,ns_31) = s1''''''''``) THEN FST
  THEN (SYM_ASSUM ``(s'''''''0,s'''''''1,r''''''''''0,ns_31) = s1'''''''``) THEN FST
  THEN (SYM_ASSUM ``(s'''''''1,r''''''''''0,ns_31) = s1''''''``) THEN FST
  THEN (SYM_ASSUM ``(r''''''''''0,ns_31) = s1'''''``) THEN FST
  THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
  THEN (SYM_ASSUM ``(FST s,ns3) = s3'''``) THEN FST
  THEN (SYM_ASSUM ``(mem2mmu,FST s,ns3) = s3''``) THEN FST
  THEN (SYM_ASSUM ``(ns1,mem2mmu,FST s,ns3) = s3'``) THEN FST
  THEN (SYM_ASSUM ``(ns1,ns1,mem2mmu,FST s,ns3) = s3``) THEN FST
  THEN (SYM_ASSUM ``(F,ns1,ns1,mem2mmu,FST s,ns3) = s''''``) THEN FST

  THEN (`ns1 = NONE` by ALL_TAC)
  THENL [
    (RW_TAC (srw_ss()) []),
    ALL_TAC]
  
  THEN FST
  THEN (Cases_on `o_mmu = NONE`)
  THENL [
    FST
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (RW_TAC (srw_ss()) []),
    ALL_TAC
  ]
  FST
  THEN (PairCases_on `s''`)
  THEN (`s''5.MMU.S = mmu_wait` by ALL_TAC)
  THENL [
    (Cases_on `IsFault (THE ns0).desc`)
    THENL [
      FST THEN (RW_TAC (srw_ss()) []),
      FST THEN (RW_TAC (srw_ss()) [])
    ], ALL_TAC
  ]

  THEN FST
  


  THEN (Q.UNABBREV_TAC `s'''''''''''''`)

  THEN (DEABBREV_AND_FIX_TAC)

  THEN (RW_TAC (srw_ss()) [])

  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
]

















use "integrity_mmu_st1.sml";


val mmu_state_policy_def = Define `
mmu_state_policy mmu_state =
  (mmu_state.MMU.CURR_REQ.EL < 2) /\
  (mmu_state.MMU.CURR_REQ.ns) /\
  ((mmu_state.MMU.S = mmu_wait)  ==>
    ((mmu_state.MMU.ST2 = wait) /\
    (mmu_state.MMU.ST1 = wait))
  )
`;


val mmu_integrity_def = ``
!m_state (n:num) o_core2mmu o_mem2mmu o_mmu2core o_mmu2mem GI.
(!n'. 
      (n' <= n) ==> (
         (mmu_mem_policy (o_mmu2mem n') (o_mem2mmu n') policy policy_tbl GI) /\
         (policy_opt mmu_core_policy (o_core2mmu n'))
      )
) ==>
(mmu_state_policy m_state) ==>
let s' = mmu_trace m_state n o_core2mmu o_mem2mmu o_mmu2core o_mmu2mem in
let (mmu_state', core2mmu', mem2mmu', mmu2core', mmu2mem') = s' in
(mmu_mem_policy mmu2mem' mem2mmu' policy policy_tbl GI) /\
(policy_opt mmu_core_policy core2mmu') /\
(mmu_state_policy mmu_state')
``;


``^mmu_integrity_def``;

REPEAT (GEN_TAC)
THEN (Induct_on `n`)
THENL [
  (FULL_SIMP_TAC (srw_ss()) [Once mmu_trace_def])
  THEN (RW_TAC (srw_ss()) [])
  THEN (FULL_SIMP_TAC (srw_ss()) [])
, ALL_TAC]

THEN (REPEAT DISCH_TAC)
THEN (ABBREV_GOAL)
(* First we show that the hypotesys hold for the induct hypo *)
THEN (TERM_PAT_ASSUM ``a ==> b ==> c`` (fn a::b::c::[] => 
      (`^a/\^b` by ALL_TAC)))
THENL [
  (RW_TAC (arith_ss) []), 
  ALL_TAC
]
THEN (REV_FULL_SIMP_TAC (srw_ss()) [])

THEN (MY_LET_AND_EXEC2 [mmu_mem_policy_def])

THEN (SIMP_BY_PAT ``mmu_trace m_state (SUC n) o_core2mmu o_mem2mmu o_mmu2core o_mmu2mem = a`` [Once mmu_trace_def])
THEN (MY_LET_AND_EXEC2 [])

(* We show that the state of the automata are sync *)
THEN (`^mmu_correctness_def` by cheat)
THEN (TERM_PAT_ASSUM ``mmu_step (a,b,c,d,e) = x`` (fn a::b::c::d::e::x => 
    (PAT_ASSUM ``!mmu_state core2mmu mem2mmu mmu2core mmu2mem.p`` ( fn thm =>
       let val nt = (SIMP_RULE (srw_ss()) [LET_DEF] (SPECL [a,b,c,d,e] thm))
     val hyp = list_mk_conj(fst(strip_imp (concl(nt))))
 in
     (`^hyp` by ALL_TAC) THENL
           [ALL_TAC, ASSUME_TAC nt]
 end))
))
THENL [
  (FULL_SIMP_TAC (srw_ss()) [mmu_state_policy_def])
, ALL_TAC]
THEN FST
THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
(* Done We show that the state of the automata are sync *)

THEN (MY_LET_AND_EXEC2 [mmu_step_def, mmu_interface_def])

THEN (Q.ABBREV_TAC `c = (SND s).MMU.S ≠ mmu_wait ∨ o_core2mmu (SUC n) ≠ NONE`)

THEN (Cases_on `~c`)
THENL [
  (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (SPECL_ASSUM ``!n':num . (n'<= SUC n) ==> p`` [``SUC n``])
  THEN (RW_TAC (srw_ss()) [])
  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN (REV_FULL_SIMP_TAC (srw_ss()) []),
  ALL_TAC
]

THEN FST
THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])

THEN (Cases_on `(SND s).MMU.S`)

(* We are in the case of mmu_wait *)
THENL [
  FST
  THEN (MY_LET_AND_EXEC2 [])
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) 
  THEN (MY_LET_AND_EXEC2 [MMU_sched_def])

  (* We show that the automaton-1 is in wait state *)
  THEN (`(SND s).MMU.ST2 = wait2` by ALL_TAC)
  THENL [
     (RW_TAC (srw_ss()) [])
     THEN (FULL_SIMP_TAC (srw_ss()) [])
  , ALL_TAC]

  THEN FST

  THEN (MY_LET_AND_EXEC2 [])
  (* TO FIX *)
  THEN (PairCases_on `s'''''''''`)
  THEN FST
  THEN (MY_LET_AND_EXEC2 [])
  THEN FST

  (* This rely on the integrity of the first stage *)
  THEN (PairCases_on `v''''''''`)
  THEN (TERM_PAT_ASSUM ``MMU_ST1_sched (a,b) c = ((d1,d2,d3),e)`` (fn args =>
      let val nt = (SPECL args integrity_mmu_fst_thm) 
     val hyp = list_mk_conj(fst(strip_imp (concl(nt))))
 in
     (`^hyp` by ALL_TAC) THEN (ASSUME_TAC nt)
 end))
  THENL [
    (`IS_SOME (o_core2mmu (SUC n))` by cheat)
    THEN (FULL_SIMP_TAC (srw_ss()) [get_mmureq_def, LET_DEF, policy_opt_def])
    THEN (SPECL_ASSUM ``!n':num . (n'<= SUC n) ==> p`` [``SUC n``])
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (RW_TAC (srw_ss()) [])
    THEN (FULL_SIMP_TAC (srw_ss()) [mmu_core_policy_def])
    THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
    THEN (FULL_SIMP_TAC (srw_ss()) [mmu_core_policy_def])
  , ALL_TAC]
  THEN (REV_FULL_SIMP_TAC (srw_ss()) [])

  (* I'm sim[plygying the condition on the step for the second stage *)
  THEN (`(s'''''''''3.MMU.CURR_REQ.EL < 2 ∧
         s'''''''''3.MMU.CURR_REQ.ns)` by ALL_TAC)
  THENL [
      (DEABBREV_AND_FIX_TAC)
      THEN (RW_TAC (srw_ss()) [])
  , ALL_TAC]
  THEN FST

  (* We must show that the first stage did not affect the state of *)
  (* the second one, since we know that the second stage is in state wait2 *)
  THEN (`s'''''''''3.MMU.ST2 = wait2` by ALL_TAC)
  THENL [
     (DEABBREV_AND_FIX_TAC)
     THEN (RW_TAC (srw_ss()) [])
  , ALL_TAC]
  THEN FST

  (* The first stage did not produce a look up. *)
  (* This is becouse it raised an exception and the *)
  (* second stage does not need a look-up *)
  THEN (Cases_on `~v'''''''`)

  THENL [
    (FULL_SIMP_TAC (srw_ss()) [])
    (* we must guarantee that the fist stage did not return a memory request *)
    (* when there is an exception *)
    (* I think there is a bug in the model *)
    THEN (`s'''''''''2 = NONE` by cheat)
    THEN FST
    (* Case we used the message *)
    THEN (Cases_on `cons`)
    THENL [
       FST
       THEN (DEABBREV_AND_FIX_TAC)
       THEN (SPECL_ASSUM ``!n':num . (n'<= SUC n) ==> p`` [``SUC n``])
       THEN (RW_TAC (srw_ss()) [])
       THEN (FULL_SIMP_TAC (srw_ss()) [stupid_is_some_thm, policy_opt_def, mmu_state_policy_def])
       THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
    , ALL_TAC]

    THEN FST
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (SPECL_ASSUM ``!n':num . (n'<= SUC n) ==> p`` [``SUC n``])
    THEN (RW_TAC (srw_ss()) [])
    THEN (FULL_SIMP_TAC (srw_ss()) [stupid_is_some_thm, policy_opt_def, mmu_state_policy_def])
    THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
 , ALL_TAC]

  (* The first stage produced something. *)
  (* the second stage must perform a look-up *)
  THEN (Q.ABBREV_TAC `ns = (MMU_ST2_sched (o_mem2mmu (SUC n)) s'''''''''3)`)
  THEN (PairCases_on `ns`)
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  
  THEN (MY_LET_AND_EXEC2 [MMU_ST2_sched_def])

  (* We must show that the first stage did not affect the state of *)
  (* the second one, since we know that the second stage is in state wait2 *)
  THEN (`(SND s''''''''''''').MMU.ST2 = wait2` by ALL_TAC)
  THENL [
     (DEABBREV_AND_FIX_TAC)
     THEN (RW_TAC (srw_ss()) [])
  , ALL_TAC]
  THEN FST

  THEN (Q.ABBREV_TAC `ns_1 = (MMU_Init_ST2 (s'''''''''3.MMU.ST1 = mem)
                 (SND s'''''''''''''))`)
  
  THEN (MY_LET_AND_EXEC2 [MMU_Init_ST2_def])

  THEN (REPEAT SPLIT_ABBREVS)
  THEN (REV_FULL_SIMP_TAC (srw_ss()) [])

  (* Case we used the message *)
  THEN (Cases_on `cons`)
  THENL [
    FST
    THEN (SPECL_ASSUM ``!n':num . (n'<= SUC n) ==> p`` [``SUC n``])
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (RW_TAC (srw_ss()) [])
    THEN (FULL_SIMP_TAC (srw_ss()) [stupid_is_some_thm, policy_opt_def, mmu_state_policy_def])
    THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
     ,ALL_TAC
  ]
  THEN FST
  THEN (SPECL_ASSUM ``!n':num . (n'<= SUC n) ==> p`` [``SUC n``])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) [])
  THEN (FULL_SIMP_TAC (srw_ss()) [stupid_is_some_thm, policy_opt_def, mmu_state_policy_def])
  THEN (REV_FULL_SIMP_TAC (srw_ss()) [])

, ALL_TAC]


(* Here the wait case is completed *)
(* Here the wait case is completed *)
(* Here the wait case is completed *)
(* Here the wait case is completed *)
(* Here the wait case is completed *)
(* Here the wait case is completed *)
(* Here the wait case is completed *)
(* Here the wait case is completed *)
(* Here the wait case is completed *)
THEN (FULL_SIMP_TAC (srw_ss()) [])
THEN (MY_LET_AND_EXEC2 [])
THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) 
THEN (MY_LET_AND_EXEC2 [MMU_sched_def])

(* First we show how the state and channel can be affected by the first stage *)
(* (or not affected if it is not scheduled) *)

THEN (PairCases_on `s''''''''`)
THEN FST
THEN (SUBGOAL_THEN (``
 (s''''''''3.MMU.CURR_REQ.EL < 2) /\
 (s''''''''3.MMU.CURR_REQ.ns) /\
 (s''''''''3.MMU.ST2 = mmu_state.MMU.ST2) /\
 ((s''''''''2 = NONE) \/
  ((s''''''''3.MMU.ST1 ≠ walk) ==> (s''''''''2 = NONE)))
  `` 
) (fn thm => ASSUME_TAC thm))
THENL [
  (* TO FIX, mess with redundand constraints *)
  (Q.UNABBREV_TAC `s'''''''`)
  THEN (ABBREV_GOAL)
  THEN (`s = (o_core2mmu (SUC n),mmu_state')` by (RW_TAC (srw_ss()) []))
  THEN (`mmu_state' = mmu_state` by (RW_TAC (srw_ss()) []))

  THEN FST
  (* remove unusued assumtion *)
  THEN (PAT_ASSUM ``(if a then b else c) = d`` (fn thm => ALL_TAC))
  THEN (PAT_ASSUM ``Abbrev (v''''''':bool = a)`` (fn thm => ALL_TAC))
  THEN (PAT_ASSUM ``Abbrev (s'''''''''' = (if a then b else c))`` (fn thm => ALL_TAC))
  THEN (PAT_ASSUM ``Abbrev (s'''' = (if  o_mmu ≠ NONE then b else c))`` (fn thm => ALL_TAC))
  THEN (FST)


  THEN (RW_TAC (srw_ss()) [])

  THEN (Cases_on `~(mmu_state'.MMU.ST2 = wait2)`)
  THENL [
     FST
     THEN (DEABBREV_AND_FIX_TAC)
     THEN (RW_TAC (srw_ss()) [])

     THEN (SPECL_ASSUM ``!n':num . (n'<= SUC n) ==> p`` [``SUC n``])
     THEN (FULL_SIMP_TAC (srw_ss()) [policy_opt_def, mmu_core_policy_def, mmu_state_policy_def])
     THEN (DEABBREV_AND_FIX_TAC)
     THEN (FULL_SIMP_TAC (srw_ss()) [])
     THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
















    THEN (FULL_SIMP_TAC (srw_ss()) [get_mmureq_def, LET_DEF, policy_opt_def, mmu_core_policy_def])
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (RW_TAC (srw_ss()) [])
    THEN (FULL_SIMP_TAC (srw_ss()) [])
    THEN (REV_FULL_SIMP_TAC (srw_ss()) [])

   ]
]
















  (* make step of ST2 is not busy *)
  THEN (FULL_SIMP_TAC (srw_ss()) [])
  (* TO FIX *)
  THEN (PairCases_on `s'''''''''`)
  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN (`(s'''''''''3.MMU.CURR_REQ.EL < 2 ∧
          s'''''''''3.MMU.CURR_REQ.ns)` by cheat)
  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN (MY_LET_AND_EXEC2 [])
  THEN (REPEAT SPLIT_ABBREVS)

  THEN (`(s'''''''''3.MMU.ST2 = (SND s).MMU.ST2)` by ALL_TAC)
  THENL [
     (DEABBREV_AND_FIX_TAC)
     THEN (RW_TAC (srw_ss()) []),
     ALL_TAC
  ]
  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN FIX_ABBREV_TAC2
  THEN (FULL_SIMP_TAC (srw_ss()) [])
  THEN (MY_LET_AND_EXEC2 [MMU_ST2_sched_def])

  THEN (Cases_on `(SND s'''''''''''').MMU.ST2`)
  THENL [
     (* the second stage is waiting something from the memory *)
     (FULL_SIMP_TAC (srw_ss()) [])
     THEN (DEABBREV_AND_FIX_TAC)
     THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF]),

     (* the second stage initialized the lookup *)
     (FULL_SIMP_TAC (srw_ss()) [])
     THEN (MY_LET_AND_EXEC2 [Disabled_ST2_def])
     
     (* 0 : VM	-- Enable Stage 2 address translation *)
     THEN (`(SND s'''''''''''').MMU.HCR.VM` by cheat)
     THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
     THEN (REV_FULL_SIMP_TAC (srw_ss()) [])

     (* TO FIX *)
     THEN (PairCases_on `s0'`)
     THEN (FULL_SIMP_TAC (srw_ss()) [])
     THEN (Q.ABBREV_TAC `ns = MMU_StartWalk_ST2 () s0'3`)

     THEN (MY_LET_AND_EXEC2 [MMU_StartWalk_ST2_def])
     
     THEN (DEABBREV_AND_FIX_TAC)
     THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
     THEN (SPECL_ASSUM ``!n':num . (n'<= SUC n) ==> p`` [``SUC n``])
     THEN (RW_TAC (srw_ss()) [])
     THEN (FULL_SIMP_TAC (srw_ss()) [stupid_is_some_thm])
     THEN (REV_FULL_SIMP_TAC (srw_ss()) [])

     (* long but it manage *),
     (FULL_SIMP_TAC (srw_ss()) [])
     THEN (MY_LET_AND_EXEC2 [MMU_FetchDescriptor_ST2_def])
     THEN (DEABBREV_AND_FIX_TAC)
     THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
     THEN (SPECL_ASSUM ``!n':num . (n'<= SUC n) ==> p`` [``SUC n``])
     THEN (RW_TAC (srw_ss()) [])
     THEN (FULL_SIMP_TAC (srw_ss()) [stupid_is_some_thm])
     THEN (REV_FULL_SIMP_TAC (srw_ss()) [])

     THEN (`policy_tbl mmu_state'.MMU.DESC2.paddress` by cheat),

     (FULL_SIMP_TAC (srw_ss()) [])
     THEN (Cases_on `o_mem2mmu (SUC n) = NONE`)
     THENL [
         (FULL_SIMP_TAC (srw_ss()) [])
	 THEN (DEABBREV_AND_FIX_TAC)
	 THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
	 THEN (SPECL_ASSUM ``!n':num . (n'<= SUC n) ==> p`` [``SUC n``])
	 THEN (RW_TAC (srw_ss()) [])
	 THEN (FULL_SIMP_TAC (srw_ss()) [stupid_is_some_thm])
	 THEN (REV_FULL_SIMP_TAC (srw_ss()) []),
	 ALL_TAC
     ]
     THEN (FULL_SIMP_TAC (srw_ss()) [])
     THEN (Q.ABBREV_TAC `ns = (MMU_ExtendWalk_ST2 (FST s'''''''''''') (SND s''''''''''''))`)
     THEN (MY_LET_AND_EXEC2 [MMU_ExtendWalk_ST2_def])

     THEN (DEABBREV_AND_FIX_TAC)
     THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
     THEN (SPECL_ASSUM ``!n':num . (n'<= SUC n) ==> p`` [``SUC n``])
     THEN (RW_TAC (srw_ss()) [])
     THEN (FULL_SIMP_TAC (srw_ss()) [stupid_is_some_thm])
     THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
  ]
]












(* ****************************** *)






val mmu_integrity_def = ``
!mmu_state core2mmu mem2mmu mmu2core mmu2mem GI.
(mmu_mem_policy mmu2mem mem2mmu policy policy_tbl GI) /\
(policy_opt mmu_core_policy core2mmu)
 ==>
(mmu_state_policy mmu_state) ==>

let s' = (MMU_sched (core2mmu,mem2mmu) mmu_state) in
let ((mmu2core', mmu2mem', used), mmu_state') = s' in
(mmu_mem_policy mmu2mem' mem2mmu policy policy_tbl GI) /\
(policy_opt mmu_core_policy core2mmu) /\
(mmu_state_policy mmu_state')
``;

``^mmu_integrity_def``;

REPEAT (GEN_TAC)
THEN (REPEAT DISCH_TAC)
THEN (ABBREV_GOAL)
THEN (MY_LET_AND_EXEC2 [mmu_mem_policy_def])

THEN (MY_LET_AND_EXEC2 [MMU_sched_def])
THEN FST
THEN (Cases_on `~v`)
THENL [
  (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (Q.ABBREV_TAC `ns1 = (MMU_ST1_sched (core2mmu,mem2mmu) mmu_state)`)    
  THEN (PairCases_on `ns1`)
  THEN (FST)
  THEN (Cases_on `(mmu_state.MMU.ST2 <> wait)`)
  THENL [
    FST THEN DEABBREV_AND_FIX_TAC THEN (RW_TAC (srw_ss()) []),
    ALL_TAC
  ]
  THEN (`(ns13.MMU.CURR_REQ.EL < 2) /\ (ns13.MMU.CURR_REQ.ns)` by cheat)
  THEN (FULL_SIMP_TAC (srw_ss()) [mmu_state_policy_def])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) [])
  THEN cheat,
  ALL_TAC
]

THEN FST





