val integrity_mmu_snd_thm = prove(

``! req1 req2 mmu_state out_mem consumed mmu_state'.
(mmu_state.MMU.CURR_REQ.EL < 2) ==>
(mmu_state.MMU.CURR_REQ.ns) ==>
(((out_mem, consumed),mmu_state') = MMU_ST2_sched (req1,req2) mmu_state) ==>
(
 (mmu_state'.MMU.CURR_REQ.EL < 2) /\
 (mmu_state'.MMU.CURR_REQ.ns) /\
 (mmu_state'.MMU.S = mmu_state.MMU.S)
 (* /\ *)
 (* ((mmu_state'.MMU.ST1 ≠ mem ∧ *)
 (*  (mmu_state'.MMU.ST1 ≠ final ∨ *)
 (*   mmu_state'.MMU.F1.typ ≠ Fault_None)) ==> *)
 (*  (out_mem = NONE)) *)
)``
,

REPEAT (GEN_TAC)
THEN (REPEAT DISCH_TAC)
THEN (MY_LET_AND_EXEC2 [MMU_ST2_sched_def])
THEN (PairCases_on `s'`)
THEN FST
THEN (Cases_on `s'3.MMU.ST2`)
THENL [
  (* wait *)
  (FULL_SIMP_TAC (srw_ss()) [MMU_Init_ST2_def, LET_DEF])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) [])
  THEN ((Cases_on `IS_SOME req2`) THEN FST),

  (* init2 *)
  (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (Cases_on `FST (Disabled_ST2 s'3)`)
  THENL [
       (FULL_SIMP_TAC (srw_ss()) [MMU_FlatMap_ST2_def, LET_DEF])
       THEN (DEABBREV_AND_FIX_TAC)
       THEN (RW_TAC (srw_ss()) [])
       THEN ((Cases_on `IS_SOME req2`) THEN FST)
     ,ALL_TAC]
  (* this is slow *)
  THEN (FULL_SIMP_TAC (srw_ss()) [MMU_StartWalk_ST2_def, LET_DEF, prepare_initial_lookup_def])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) [])
  THEN ((Cases_on `IS_SOME req2`) THEN FST),

  (FULL_SIMP_TAC (srw_ss()) [LET_DEF, MMU_FetchDescriptor_ST2_def])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) [])
  THEN ((Cases_on `IS_SOME req2`) THEN FST),

  (* walk case is complex *)
  ALL_TAC,

  (FULL_SIMP_TAC (srw_ss()) [LET_DEF, MMU_Complete_ST2_def])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) [])
  THEN ((Cases_on `IS_SOME req2`) THEN FST)
]

THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
THEN (Cases_on `req1 = NONE`)
THENL [
   FST THEN (DEABBREV_AND_FIX_TAC)
   THEN (RW_TAC (srw_ss()) [])
   THEN ((Cases_on `IS_SOME req2`) THEN FST)
 ,ALL_TAC]
  
THEN FST
THEN (Q.ABBREV_TAC `ns2 = (MMU_ExtendWalk_ST2 s'2 s'3)`)
THEN (PairCases_on `ns2`)
THEN FST
THEN (MY_LET_AND_EXEC2 [MMU_ExtendWalk_ST2_def])




THEN (`(s''''.MMU.CURR_REQ.EL < 2) /\
       (s''''.MMU.CURR_REQ.ns) /\
       (s''''.MMU.S = mmu_state.MMU.S)` by ALL_TAC)

THENL [
  (* remove unused assumptions *)
  (PAT_ASSUM ``(x = ((),ns21))`` (fn thm => ALL_TAC))
  THEN (Cases_on `~(s'2.desc.fault.typ = Fault_None)`)
  THENL [
    FST THEN (DEABBREV_AND_FIX_TAC) THEN (RW_TAC (srw_ss()) [])
	THEN ((Cases_on `(IS_SOME req2)`)
	      THENL [
	        (Cases_on `(IS_SOME req1)`) THEN FST THEN (RW_TAC (srw_ss()) []),
		(Cases_on `(IS_SOME req1)`) THEN FST THEN (RW_TAC (srw_ss()) [])
	      ]),
    ALL_TAC]

  THEN FST
  THEN (Q.ABBREV_TAC `ns1 = (walk_extend (s'.MMU.W2,FST (cast_desc (v2w s'2.data) s''')) s''')`)
  THEN (PairCases_on `ns1`)
  THEN FST
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])

  THEN (`(ns12.MMU.CURR_REQ.EL < 2) /\
         (ns12.MMU.CURR_REQ.ns) /\
         (ns12.MMU.S = mmu_state.MMU.S)` by ALL_TAC)
  THENL [
       (TERM_PAT_ASSUM ``Abbrev (((a,b),c) = walk_extend (d,e) f)`` (
	  fn a::b::c::d::e::f::[] =>
	     ASSUME_TAC (SPECL [d,e,f,a,b,c] integrity_walk_extend_thm)
       ))
       THEN (`s'''.MMU.CURR_REQ.EL < 2` by ALL_TAC)
       THENL [
         (DEABBREV_AND_FIX_TAC) THEN
         ((Cases_on `(IS_SOME req2)`)
	    THENL [
	    (Cases_on `(IS_SOME req1)`) THEN FST THEN (RW_TAC (srw_ss()) []),
	    (Cases_on `(IS_SOME req1)`) THEN FST THEN (RW_TAC (srw_ss()) [])
         ]),
	 ALL_TAC]
       THEN (`s'''.MMU.CURR_REQ.ns` by ALL_TAC)
       THENL [
         (DEABBREV_AND_FIX_TAC) THEN
         ((Cases_on `(IS_SOME req2)`)
	    THENL [
	    (Cases_on `(IS_SOME req1)`) THEN FST THEN (RW_TAC (srw_ss()) []),
	    (Cases_on `(IS_SOME req1)`) THEN FST THEN (RW_TAC (srw_ss()) [])
         ]),
	 ALL_TAC]
       THEN (`s'''.MMU.S= mmu_state.MMU.S` by ALL_TAC)
       THENL [
         (DEABBREV_AND_FIX_TAC) THEN
         ((Cases_on `(IS_SOME req2)`)
	    THENL [
	    (Cases_on `(IS_SOME req1)`) THEN FST THEN (RW_TAC (srw_ss()) []),
	    (Cases_on `(IS_SOME req1)`) THEN FST THEN (RW_TAC (srw_ss()) [])
         ]),
	 ALL_TAC]

       THEN FST
     ,ALL_TAC
  ]

  THEN FST
  THEN (Cases_on `(ns11.typ = Fault_None) ∧ ns10.c`)
  THENL [
    (DEABBREV_AND_FIX_TAC) THEN
         ((Cases_on `(IS_SOME req2)`)
	    THENL [
	    (Cases_on `(IS_SOME req1)`) THEN FST THEN (RW_TAC (srw_ss()) []),
	    (Cases_on `(IS_SOME req1)`) THEN FST THEN (RW_TAC (srw_ss()) [])
         ]),
    ALL_TAC
  ]
  THEN FST
  THENL [
    (* THIS REPETITION MUST BE FIXED *)
    (DEABBREV_AND_FIX_TAC) THEN
         ((Cases_on `(IS_SOME req2)`)
	    THENL [
	    (Cases_on `(IS_SOME req1)`) THEN FST THEN (RW_TAC (srw_ss()) []),
	    (Cases_on `(IS_SOME req1)`) THEN FST THEN (RW_TAC (srw_ss()) [])
         ]),
    (DEABBREV_AND_FIX_TAC) THEN
         ((Cases_on `(IS_SOME req2)`)
	    THENL [
	    (Cases_on `(IS_SOME req1)`) THEN FST THEN (RW_TAC (srw_ss()) []),
	    (Cases_on `(IS_SOME req1)`) THEN FST THEN (RW_TAC (srw_ss()) [])
         ])
  ]
 ,ALL_TAC
]

THEN FST
(* remove unused assumptions *)
THEN (PAT_ASSUM ``Abbrev (s'''' = x)`` (fn thm => ALL_TAC))
THEN (Q.ABBREV_TAC `cnd = s''''.MMU.F2.typ ≠ Fault_None ∨ s''''.MMU.W2.c`)
THEN (Cases_on `cnd`)

THENL [
  (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (DEABBREV_AND_FIX_TAC) THEN (RW_TAC (srw_ss()) []),
  FST THEN (DEABBREV_AND_FIX_TAC) THEN (RW_TAC (srw_ss()) [])
]

);
