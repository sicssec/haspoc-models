val integrity_walk_extend_thm = prove(

``! w desc mmu_state w' fault mmu_state'.
(mmu_state.MMU.CURR_REQ.EL < 2) ==>
(mmu_state.MMU.CURR_REQ.ns) ==>
(((w', fault),mmu_state') = walk_extend (w,desc) mmu_state) ==>
(
 (mmu_state'.MMU.CURR_REQ.EL < 2) /\
 (mmu_state'.MMU.CURR_REQ.ns) /\
 (mmu_state'.MMU.S = mmu_state.MMU.S)
 (* /\ *)
 (* ((mmu_state'.MMU.ST1 ≠ mem ∧ *)
 (*  (mmu_state'.MMU.ST1 ≠ final ∨ *)
 (*   mmu_state'.MMU.F1.typ ≠ Fault_None)) ==> *)
 (*  (out_mem = NONE)) *)
)``
,

REPEAT (GEN_TAC)
THEN (REPEAT DISCH_TAC)
THEN (MY_LET_AND_EXEC2 [walk_extend_def])

(* We must prevent combinatorial explosion we we prove intermediate states *)
THEN (PairCases_on `s'`)
THEN (FST)
(* strange that I've to split the pair twice *)
THEN (PairCases_on `s''`)
THEN (FST)
THEN (` (s''2.MMU.CURR_REQ.EL < 2) /\
        (s''2.MMU.CURR_REQ.ns) /\
        (s''2.MMU.S = mmu_state.MMU.S)` by ALL_TAC)

THENL [
  (* remove unused assumptions *)
  (PAT_ASSUM ``Abbrev (s''' = x)`` (fn thm => ALL_TAC))
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (Q.ABBREV_TAC `cnd1 = ((w.l = 4 − v) ∧ (SND (SND s)).MMU.ISW.IMPL_CONTIGUOUS_BIT_FAULT) ∧ desc.Contiguous`)
  THEN (Cases_on `cnd1`)
  THENL [
     FST THEN (DEABBREV_AND_FIX_TAC)
     THEN (RW_TAC (srw_ss()) []),
     (FST) THEN (DEABBREV_AND_FIX_TAC)
     THEN (RW_TAC (srw_ss()) [])
  ],
  ALL_TAC
]

THEN (FST)
(* remove unused assumptions *)
THEN (PAT_ASSUM ``x = (v'',s''0,s''1,s''2)`` (fn thm => ALL_TAC))

THEN (Q.ABBREV_TAC `cnd = (v0 ∨ v'') ∨ w.l > 3`)
THEN (Cases_on `cnd`)
THENL [
  FST THEN DEABBREV_AND_FIX_TAC THEN (RW_TAC (srw_ss()) []),
  ALL_TAC
]

THEN FST
THEN (MY_LET_AND_EXEC2 [])

THEN (PairCases_on `s'`)
THEN (FST)
THEN (` (s'2'.MMU.CURR_REQ.EL < 2) /\
        (s'2'.MMU.CURR_REQ.ns) /\
        (s'2'.MMU.S = mmu_state.MMU.S)` by ALL_TAC)

THENL [
  (* remove unused assumptions *)
  (PAT_ASSUM ``Abbrev (s''''' = x)`` (fn thm => ALL_TAC))
  THEN (PAT_ASSUM ``Abbrev (s'''' = x)`` (fn thm => ALL_TAC))
  THEN (PAT_ASSUM ``Abbrev (s'' = x)`` (fn thm => ALL_TAC))
  THEN (Q.ABBREV_TAC `cnd1 = desc.pte ∧ w.l ≠ 3`)
  THEN (Cases_on `cnd1`)
  THENL [
     FST THEN (DEABBREV_AND_FIX_TAC)  THEN (RW_TAC (srw_ss()) []),
     (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) THEN (DEABBREV_AND_FIX_TAC)
     THEN (RW_TAC (srw_ss()) [])
  ],
  ALL_TAC
]

THEN (FST)
(* remove unused assumptions *)
THEN (PAT_ASSUM ``Abbrev((s'0',s'1',s'2')=x)`` (fn thm => ALL_TAC))

THEN (PairCases_on `s''`)
THEN (FST)
THEN (` (s''2'.MMU.CURR_REQ.EL < 2) /\
        (s''2'.MMU.CURR_REQ.ns) /\
        (s''2'.MMU.S = mmu_state.MMU.S)` by ALL_TAC)

THENL [
   
  (* remove unused assumptions *)
  (PAT_ASSUM ``Abbrev (s''''' = x)`` (fn thm => ALL_TAC))
  THEN (PAT_ASSUM ``Abbrev (s'''' = x)`` (fn thm => ALL_TAC))
  THEN (Q.ABBREV_TAC `cnd1 = (w.st = 1) ∨ lastl`)
  THEN (Cases_on `cnd1`)
  THENL [
    (Cases_on `w.st = 2`)
    THENL [
      (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) THEN (DEABBREV_AND_FIX_TAC)
      THEN (RW_TAC (srw_ss()) []),
      (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) THEN (DEABBREV_AND_FIX_TAC)
      THEN (RW_TAC (srw_ss()) [])
    ],
    ALL_TAC
  ]

  THEN FST
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) []),

  ALL_TAC]

THEN FST

(* remove unused assumptions *)
THEN (PAT_ASSUM ``Abbrev((s''0',s''1',s''2')=x)`` (fn thm => ALL_TAC))
THEN (PairCases_on `s''''`)
THEN (FST)
THEN (` (s''''2.MMU.CURR_REQ.EL < 2) /\
        (s''''2.MMU.CURR_REQ.ns) /\
        (s''''2.MMU.S = mmu_state.MMU.S)` by ALL_TAC)

THENL [
  (* remove unused assumptions *)
  (PAT_ASSUM ``Abbrev (s''''' = x)`` (fn thm => ALL_TAC))
  THEN (Cases_on `w.st = 1`)
  THENL [
    (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) THEN (DEABBREV_AND_FIX_TAC)
    THEN (RW_TAC (srw_ss()) []),
    FST
  ]
  THEN (Cases_on `lastl`)
  THENL [
    (Cases_on `w.st = 2`)
    THENL [
      (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) THEN (DEABBREV_AND_FIX_TAC)
      THEN (RW_TAC (srw_ss()) []),
      (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) THEN (DEABBREV_AND_FIX_TAC)
      THEN (RW_TAC (srw_ss()) [])
    ],
    FST THEN (DEABBREV_AND_FIX_TAC)
	THEN (RW_TAC (srw_ss()) [])
  ]
 , ALL_TAC
]

(* remove unused assumptions *)
THEN (PAT_ASSUM ``Abbrev((s''''0,s''''1,s''''2)=x)`` (fn thm => ALL_TAC))
THEN (Cases_on `lastl`)

THENL [
  FST
  THEN (Cases_on `w.st = 1`)
  THENL [
    (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) THEN (DEABBREV_AND_FIX_TAC)
    THEN (RW_TAC (srw_ss()) []),
    ALL_TAC]
  THEN (FST)
  THEN (Cases_on `w.st = 2`)
  THENL [
    (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) THEN (DEABBREV_AND_FIX_TAC)
      THEN (RW_TAC (srw_ss()) []),
    (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) THEN (DEABBREV_AND_FIX_TAC)
      THEN (RW_TAC (srw_ss()) [])
  ],
  ALL_TAC
]

THEN FST
THEN (Cases_on `w.st = 1`)
THENL [
  (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) []),
  (FULL_SIMP_TAC (srw_ss()) [LET_DEF]) THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) [])
]

);
