(* THIS IS WHERE I WAS WORKING *)

val mmu_core_policy_def = Define `
mmu_core_policy (req,reg:MMU_REGS) =
  (req.EL < 2) /\
  (req.ns)
`;

val policy_opt_def = Define `
policy_opt policy channel =
(channel <> NONE) ==>
(policy (THE channel))
`;


(* for now we just use a whole GB as area for the page table. *)
val mmu_policy_tbl_def = Define `
  mmu_policy_tbl mmu_state =
     \adr.
     ((0xffc0000000w && adr) = (0xffc0000000w && mmu_state.MMU.VTTBR.BADDR))
`;

val mmu_policy_data_def = Define `
  mmu_policy_data mmu_state =
     \adr.
     ((0xffc0000000w && adr) = (0xffc0000000w && mmu_state.MMU.VTTBR.BADDR))
`;

val mmu_invariant_def = Define `
  mmu_invariant mmu_state mmu2mem mem2mmu policy policy_tbl GI =
(mmu_mem_policy mmu2mem mem2mmu policy policy_tbl GI) /\
(mmu_state.MMU.CURR_REQ.EL < 2) /\
(mmu_state.MMU.CURR_REQ.ns) /\
(mmu_state.MMU.HCR.VM) /\
((mmu_state.MMU.F2.typ = Fault_None) ==>
 ((mmu_state.MMU.ST2 = mem) (* \/ (mmu_state.MMU.ST2 = final) *)) ==>
 ((policy_tbl mmu_state.MMU.DESC2.paddress))
) /\
((mmu_state.MMU.DESC2.fault.typ = Fault_None) ==>
 ((mmu_state.MMU.ST2 = final)) ==>
 (mmu_state.MMU.MEMREQ.write) ==>
 (~(policy mmu_state.MMU.DESC2.paddress))
) /\
(mmu_state.MMU.W2.n = 32) /\
(mmu_state.MMU.W2.br = 9) /\
(mmu_state.MMU.W2.l = 1) /\
(~mmu_state.MMU.W2.conc) /\
(mmu_state.MMU.VTCR.T0SZ = 32w) /\
(mmu_state.MMU.VTCR.TG0 = 0w) /\
(mmu_state.MMU.IDMM.TGRAN4 = 0w) /\
(mmu_state.MMU.VTCR.SL0 = 1w) /\
(~mmu_state.MMU.ISW.IMPL_TTBR_MISALIGNED_TREAT_AS_ZERO) /\
((mmu_state.MMU.ST2 = mem) ==> (mmu_state.MMU.F2.typ = Fault_None)) /\
(* It must be aligned, since we will use four descriptors from this address, each having 8 bytes? *)
(0xffffffffe0w && mmu_state.MMU.VTTBR.BADDR = mmu_state.MMU.VTTBR.BADDR)
`;



val integrity_ST2_sched_def = ``
!mmu_state core2mmu mem2mmu mmu2mem lookup omem GI.
(policy_opt mmu_core_policy core2mmu) ==>
(mmu_invariant mmu_state mmu2mem mem2mmu (mmu_policy_data mmu_state) (mmu_policy_tbl mmu_state) GI) ==>
((IS_SOME omem) ==> (mmu_state.MMU.ST2 = wait)) ==>
let s' = (MMU_ST2_sched (lookup,omem) mmu_state) in
let ((mmu2mem', used), mmu_state') = s' in
(policy_opt mmu_core_policy core2mmu) /\
(mmu_invariant mmu_state' mmu2mem' mem2mmu (mmu_policy_data mmu_state) (mmu_policy_tbl mmu_state) GI)``;



g `^integrity_ST2_sched_def`;

REPEAT (GEN_TAC)
THEN (REPEAT DISCH_TAC)
THEN (ABBREV_GOAL)
THEN (MY_LET_AND_EXEC2 [MMU_ST2_sched_def])

THEN (PairCases_on `s''`)
THEN (PairCases_on `s`)
THEN (FST)
THEN (`~s''0` by 
        ((Cases_on `IS_SOME omem`)
         THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
	 THEN (DEABBREV_AND_FIX_TAC) THEN (RW_TAC (srw_ss()) [])))
THEN (`s''1 = NONE` by 
        ((Cases_on `IS_SOME omem`)
         THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
	 THEN (DEABBREV_AND_FIX_TAC) THEN (RW_TAC (srw_ss()) [])))
THEN (`s''2 = s0` by 
        ((Cases_on `IS_SOME omem`)
         THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
	 THEN (DEABBREV_AND_FIX_TAC) THEN (RW_TAC (srw_ss()) [])))

THEN (`(mmu_invariant s''3 mmu2mem mem2mmu (mmu_policy_data s''3) (mmu_policy_tbl s''3) GI)` by ALL_TAC)
THENL [
  (FULL_SIMP_TAC (srw_ss()) [mmu_invariant_def, mmu_policy_tbl_def, mmu_policy_data_def])
  THEN (Cases_on `IS_SOME omem`)
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (DEABBREV_AND_FIX_TAC) THEN (RW_TAC (srw_ss()) []),
  ALL_TAC
]
THEN FST

(* Remove unused assumptions *)
(* No for now *)

THEN (Cases_on `s''3.MMU.ST2`)

THENL [
  (FULL_SIMP_TAC (srw_ss()) [MMU_Init_ST2_def, LET_DEF,
			     InitWalk_ST2_def, init_walk_def,
			     VAlen_def, TSZ_ST2_def,
			     TG_ST2_def, TGImplemented_def,
			     gran_bits_def, mmu_invariant_def,
			     mmu_mem_policy_def,
			     concatenated_def
  ])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) [])
  THEN (Cases_on `IS_SOME omem`)
  THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
  THEN FST

  THEN (FULL_SIMP_TAC (srw_ss()) [nlu_def])
  THEN EVAL_TAC,
  
  ALL_TAC,
  ALL_TAC,
  ALL_TAC,
  ALL_TAC]


(* case init *)
THENL [

  FST
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF, Disabled_ST2_def, mmu_invariant_def])
  THEN (Q.ABBREV_TAC `ns1 = (MMU_StartWalk_ST2 () s''3)`)
  THEN (PairCases_on `ns1`)
  THEN (FST)
  THEN (MY_LET_AND_EXEC2 [MMU_StartWalk_ST2_def])

  (* Let assume that there is a memory fault *)
  THEN (Cases_on `(f.typ <> Fault_None)`)

  THENL [
    (* We must first show that the prepare_initial_lookup do not break the invariant on W2 even *)
    (* if there is an exception *)
    FST
    THEN (Q.ABBREV_TAC `nl = (prepare_initial_lookup
            (s.MMU.W2,FST (PTO_ST2 s.MMU.W2 s),
             FST (SL0fault s.MMU.W2.br s),T,
             s.MMU.ISW.IMPL_TxSZ_LESS_THAN_16_FAULT,F) s)`)
    THEN (PairCases_on `nl`)
    THEN (MY_LET_AND_EXEC2 [prepare_initial_lookup_def])
  (* We know that there will be no error *)
    THEN (TERM_PAT_ASSUM_W_THM ``Abbrev
         (s''''''' = if a then b else c)`` (fn a::l =>
       (Q.ABBREV_TAC `cnd = ^a`)))
    THEN (`cnd` by cheat)

    THEN FST
        
    THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF, mmu_mem_policy_def])
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (RW_TAC (srw_ss()) [])
    THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
    THEN cheat,
    ALL_TAC
  ]
  
  (* In this other case there is not fault *)
  THEN FST
  THEN (MY_LET_AND_EXEC2 [])
  THEN (Q.ABBREV_TAC `nd1 = (ADfromWalk w s'''')`)
  THEN (PairCases_on `nd1`)
  THEN FST
  THEN (MY_LET_AND_EXEC2 [ADfromWalk_def])
  THEN FST
  
  THEN (Q.ABBREV_TAC `nl = (prepare_initial_lookup
            (s.MMU.W2,FST (PTO_ST2 s.MMU.W2 s),
             FST (SL0fault s.MMU.W2.br s),T,
             s.MMU.ISW.IMPL_TxSZ_LESS_THAN_16_FAULT,F) s)`)
  THEN (PairCases_on `nl`)
  THEN (MY_LET_AND_EXEC2 [prepare_initial_lookup_def])

  THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `at`])
  (* We know that there will be no error *)
  THEN (TERM_PAT_ASSUM_W_THM ``Abbrev
         (s''''''''''''' = if a then b else c)`` (fn a::l =>
       (Q.ABBREV_TAC `cnd = ^a`)))
  THEN (`~cnd` by ALL_TAC)
  THENL [
   (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
   THEN (Cases_on `cnd`) THEN FST
   THEN (RW_TAC (srw_ss()) [])
   THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `s''''''''''''''`,
	Abbr `s'''''''''''''`]),
   ALL_TAC
  ]

  THEN FST
  THEN (MY_LET_AND_EXEC2 [])

  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF, mmu_mem_policy_def])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) [])
  THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
  THEN (FST)
  
  (* I have two cases, but I should mamange only one, they are the same *)
  THENL [cheat, ALL_TAC]

  THEN (FULL_SIMP_TAC (srw_ss()) [PTO_ST2_def, page_table_origin_def,
				  LET_DEF, px_def, mmu_policy_tbl_def])
  THEN (`s''3.MMU.VTTBR.BADDR = mmu_state.MMU.VTTBR.BADDR` by ALL_TAC)
  THENL [
    (Cases_on `IS_SOME omem`) THEN
    FST THEN (RW_TAC (srw_ss()) []),
    ALL_TAC
  ]
  THEN (FST)
  THEN (Q.ABBREV_TAC `v = 
          (v2w
       (TAKE (2 :num)
          (DROP (16 :num)
             (w2v
                (((47 :num) >< (0 :num))
                   (s''3 :dharma8_state).MMU.W2.va :word48)))) :
       bool[17])`)
  THEN (Q.ABBREV_TAC `ba = mmu_state.MMU.VTTBR.BADDR`)
  THEN (PAT_UNDISCH ``(0xFFFFFFFFE0w) && (ba) = ba``)
  THEN (`0xfffffffffcw && v = 0w` by cheat)
  THEN (PAT_UNDISCH ``(0xfffffffffcw) && (v) = 0w``)
  THEN (PROVE_BY_BLAST),

  ALL_TAC,
  ALL_TAC,
  ALL_TAC]


THENL [
  (* case mem *)
  (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (Q.ABBREV_TAC `ns1 = (MMU_FetchDescriptor_ST2 () s''3)`)
  THEN (PairCases_on `ns1`)
  THEN (FST)
  THEN (MY_LET_AND_EXEC2 [MMU_FetchDescriptor_ST2_def])

  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF, mmu_mem_policy_def, mmu_invariant_def, mmu_policy_tbl_def])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) [])
  THEN (Cases_on `IS_SOME omem`)
  THEN FST
  THEN (RW_TAC (srw_ss()) []),
  
  ALL_TAC,
  ALL_TAC
]

THENL[
  (* case walk *)
  ALL_TAC,

  (* case final *)
  (FULL_SIMP_TAC (srw_ss()) [LET_DEF, MMU_Complete_ST2_def, mmu_invariant_def, mmu_mem_policy_def, combine_descriptors_def, IsFault_def])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) [])

  THEN (`mmu_state.MMU.ST2 = final` by cheat)
  THEN (FST)
  THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
  
  (* to complete since the final stage is wrong *)
  (* We need a way to identify that this output is not a second stage walk *)
  THEN cheat
]

THEN FST
THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF, mmu_invariant_def])
THEN (Q.ABBREV_TAC `nd1 = (MMU_ExtendWalk_ST2 s0 s''3)`)
THEN (PairCases_on `nd1`)
THEN FST
THEN (MY_LET_AND_EXEC2 [MMU_ExtendWalk_ST2_def])

(* We must prevent combinatorial explosion we we prove intermediate states *)
THEN (`(mmu_invariant s'''' mmu2mem mem2mmu policy (mmu_policy_tbl s'''') GI)` by ALL_TAC)
THENL [
  (* remove unused assumptions *)
  (PAT_ASSUM ``v = ((),nd11)`` (fn thm => ALL_TAC))
  THEN (Cases_on `s0.desc.fault.typ <> Fault_None`)
  THENL [
    (FULL_SIMP_TAC (srw_ss()) [mmu_invariant_def, mmu_mem_policy_def, mmu_policy_tbl_def])
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (RW_TAC (srw_ss()) []),
    ALL_TAC
  ]
  THEN (FST)
  THEN (MY_LET_AND_EXEC2 [])

  (* Now we check the result of walk_extend *)
  THEN (`(mmu_invariant s'''''' mmu2mem mem2mmu policy (mmu_policy_tbl s'''''') GI)` by ALL_TAC)
  THENL [
    ALL_TAC,
    
    (* We focus on the continuation *)
    (Cases_on `(f.typ <> Fault_None)`)
    THENL [
      (FULL_SIMP_TAC (srw_ss()) [mmu_invariant_def, mmu_mem_policy_def, mmu_policy_tbl_def])
      THEN (DEABBREV_AND_FIX_TAC)
      THEN (RW_TAC (srw_ss()) []),
      ALL_TAC
    ]
    THEN (FST)
    THEN (Cases_on `w.c`)
    THENL [
      (FULL_SIMP_TAC (srw_ss()) [mmu_invariant_def, mmu_mem_policy_def, mmu_policy_tbl_def])
      THEN (DEABBREV_AND_FIX_TAC)
      THEN (RW_TAC (srw_ss()) [])
      THEN (REV_FULL_SIMP_TAC (srw_ss()) []),
      ALL_TAC
    ]
    
    (* I whould prove that I can have only one level here, so c can not hold *)
    THEN cheat
  ]

  (* Lets' open walk extend *)
  THEN (Q.ABBREV_TAC `ns1 = (walk_extend (s.MMU.W2,FST (cast_desc (v2w s0.data) s'')) s'')`)
  THEN (PairCases_on `ns1`)
  THEN FST
  THEN (MY_LET_AND_EXEC2 [walk_extend_def])

  ,
  ALL_TAC
]
THEN FST

  

























(* OLD FAULT STATUS THAT WE MUST STILL CHECK *)




  (* First we manage the fault *)
  THEN (Cases_on `f.typ <> Fault_None`)

  THENL [
    FST
    THEN (MY_LET_AND_EXEC2 [])
    THEN (Q.ABBREV_TAC `nd1 = (ADfromResult (w,f) s'''''')`)
    THEN (MY_LET_AND_EXEC2 [ADfromResult_def])
    THEN (FST)
    THEN (PairCases_on `s'''''''''''`)
    THEN (`s'''''''''''0.paddress = w.adr` by ALL_TAC)
    THENL [
      (Q.ABBREV_TAC `cnd = (((w.st = 2) ∧ w.el < 2) ∧ (w.attr.typ = MemType_Normal)) ∧ ((w.acctype = AccType_IFETCH) ∧ s''''''.MMU.HCR.ID ∨ s''''''.MMU.HCR.CD ∧ w.acctype ≠ AccType_IFETCH)`)
      THEN (Cases_on `IS_SOME omem`)
      THEN (Cases_on `IS_SOME lookup`)
      THEN (Cases_on `cnd`)
      THEN (FST THEN (DEABBREV_AND_FIX_TAC)
             THEN (RW_TAC (srw_ss()) [])
             THEN (REV_FULL_SIMP_TAC (srw_ss()) [LET_DEF])
             THEN FST),
      ALL_TAC
    ]

    THEN (`policy_tbl mmu_state'.MMU.DESC2.paddress` by ALL_TAC)
    THENL [
       (RW_TAC (srw_ss()) [Abbr `s'''`]) 
       THEN (Q.ABBREV_TAC `lk =  (prepare_initial_lookup
            (s''''.MMU.W2,FST (PTO_ST2 s''''.MMU.W2 s''''),
             FST (SL0fault s''''.MMU.W2.br s''''),T,
             s''''.MMU.ISW.IMPL_TxSZ_LESS_THAN_16_FAULT,F) s'''')`)
       THEN (PairCases_on `lk`)
       THEN (MY_LET_AND_EXEC2 [prepare_initial_lookup_def])
       THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `at`])
       THEN (REV_FULL_SIMP_TAC (srw_ss()) [LET_DEF])
       THEN (`s''''.MMU.W2.n = 32` by
	      (DEABBREV_AND_FIX_TAC
	       THEN (RW_TAC (srw_ss()) [])))
       THEN (FST)
       THEN (Q.ABBREV_TAC `sl0_fault = (SL0fault s''''.MMU.W2.br s'''')`)
       THEN (PairCases_on `sl0_fault`)
       THEN (MY_LET_AND_EXEC2 [SL0fault_def])


       THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
       THEN (`s''''.MMU.W2.br = 9` by ALL_TAC)
       THENL [
         (REV_FULL_SIMP_TAC (srw_ss()) [])
         THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `sl0_fault1`, Abbr `s''`])
	 THEN (Cases_on `IS_SOME omem`)
	 THENL [
	   FST,
	   (FULL_SIMP_TAC (srw_ss()) [Abbr `s`])
	 ],
	 ALL_TAC
       ]

       THEN FST
       THEN (MY_LET_AND_EXEC2 [])
       
       THEN (`v' = 1` by ALL_TAC)
       THENL [
         (FULL_SIMP_TAC (srw_ss()) [Abbr `v'`, Abbr `sl0_fault1`, Abbr `s''`])
	 THEN (Cases_on `IS_SOME omem`)
	 THENL [
	   FST,
	   (FULL_SIMP_TAC (srw_ss()) [Abbr `s`])
	 ],
	 ALL_TAC
       ]

       THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `bits_resolved`])
       THEN (`v0 = 32` by ALL_TAC)
       THENL [
          (FULL_SIMP_TAC (srw_ss()) [Abbr `v0`, n_for_SL0_def, LET_DEF,
				     VAlen_def, Abbr `sl0_fault1`, Abbr `s''`,
				     Abbr `TxSZ`])
	  THEN (Cases_on `IS_SOME omem`)
	  THENL [
            FST,
	    (FULL_SIMP_TAC (srw_ss()) [Abbr `s`])
	  ],
	  ALL_TAC
       ]

       THEN (FST)
       THEN (`taprfx ≠ if sl0_fault1.MMU.W2.up then onevec TxSZ else nullvec TxSZ` by ALL_TAC)
       
       THENL [
         (FULL_SIMP_TAC (srw_ss()) [Abbr `sl0_fault1`, Abbr `s''`, Abbr `TxSZ`,
				    Abbr `taprfx`])
         THEN (Cases_on `IS_SOME omem`)
	 THENL [
	   FST
	 ]
       ]
      
    ]

,
    ALL_TAC
  ]
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) []),













  (* case mem *)
  (FULL_SIMP_TAC (srw_ss()) [])
  THEN (MY_LET_AND_EXEC2 [MMU_FetchDescriptor_ST2_def])
  THEN (REV_FULL_SIMP_TAC (srw_ss()) [])
  THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
  THEN (DEABBREV_AND_FIX_TAC)
  THEN (RW_TAC (srw_ss()) []),
  
]


