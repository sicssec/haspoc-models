load "dharma8Theory";
HOL_Interactive.toggle_quietdec();
open dharma8Theory;
open wordsLib;
HOL_Interactive.toggle_quietdec();


val mem_step_def = Define `
mem_step (m_state, mmu2mem, mem2mmu, mem2core) =
  let m_or = ((\obs_state . (mmu2mem, nofault)))  in
  let ((used_msg, rpl, rcvr), m_state) = mem_interface m_or m_state in
  let mem2core = if (rpl <> NONE) /\ (rcvr = rcvr_core) then rpl else mem2core in
  let mem2mmu = if (rpl <> NONE) /\ (rcvr = rcvr_mmu) then rpl else mem2mmu in
  let mmu2mem = if used_msg <> NONE then NONE else mmu2mem in
  (m_state, mmu2mem, mem2mmu, mem2core)
`;


val mmu_step_def = Define `
mmu_step (mmu_state, core2mmu, mem2mmu, mmu2core, mmu2mem) =
  let mmu_or = ((\obs_state . (core2mmu, mem2mmu))) in
  let ((used_c2mmu, used_m2mmu, rpl_c, rpl_m), mmu_state) = mmu_interface mmu_or mmu_state in
  let mmu2core = if rpl_c = NONE then mmu2core else rpl_c in
  let core2mmu = if used_c2mmu <> NONE then NONE else core2mmu in
  let mmu2mem = if rpl_m = NONE then mmu2mem else rpl_m in
  let mem2mmu = if used_m2mmu <> NONE then NONE else mem2mmu in
  (mmu_state, core2mmu, mem2mmu, mmu2core, mmu2mem)
`;

