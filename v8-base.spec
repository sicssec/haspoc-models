-----------------------------------------------------------------------
-- This is an extension of:					     --	
-- 								     --
-- Formal Specification of the ARMv8-A instruction set architecture  --
-- (c) Anthony Fox, University of Cambridge                          --
-- 								     --
-- with system level functionality.				     --
-- Author: Christoph Baumann, KTH CSC Stockholm			     --
-----------------------------------------------------------------------


-- Notes:
-- 2014-09-08	started modifications with Anthony, introduce memory interface 
-- 		functions for read, write, and address translation; add data types
-- 2014-10-07	extending model for address translation
-- 2014-11-05	refactoring, move definitions to separate files
-- 2015-01-16   moved types out

{-

val () = Runtime.LoadF "v8-base_types.spec, v8-base.spec";

-}

---------------------------------------
-- Processor state and system registers
---------------------------------------

record CORE_CONFIG
{
-- implementation switches
   CISW :: CORE_IMPL_SWITCHES 

-- system state and control
   SCTLR_EL1 :: SCTLRType
   SCTLR_EL2 :: SCTLRType
   SCTLR_EL3 :: SCTLRType
   SCR_EL3   :: SCRType
   
-- mmu register
   TCR_EL1   :: TCR_EL1
   TCR_EL2   :: TCR_EL2_EL3
   TCR_EL3   :: TCR_EL2_EL3
   TTBR0_EL1 :: TTBRType
   TTBR0_EL2 :: TTBRType
   TTBR0_EL3 :: TTBRType
   TTBR1_EL1 :: TTBRType
   MAIR_EL1  :: dword
   MAIR_EL2  :: dword
   MAIR_EL3  :: dword
   PAR_EL1   :: PARType
-- TODO: need AMAIR?

-- hypervisor control registers / virtualization support
   HCR_EL2    :: HCR_EL2
   VTCR_EL2   :: VTCR_EL2
   VTTBR_EL2  :: TTBRType     -- only 8 bit ASID, TODO: define shortcuts?
   VPIDR_EL2  :: MIDRType
   VMPIDR_EL2 :: MPIDRType
   HSTR_EL2   :: word

-- ID registers
   ID_AA64ISAR0_EL1 :: isa_feat  -- AArch64 Instruction Set Attribute Register 0, read-only, TODO: add real type
   ID_AA64MMFR0_EL1 :: mm_feat   -- AArch64 Memory Model Feature Register 0, read-only
   ID_AA64PFR0_EL1 :: proc_feat  -- AArch64 Processor Feature Register 0, TODO, read-only
   MIDR_EL1 :: MIDRType
   MPIDR_EL1 :: MPIDRType


-- TODO: need ACTLR?
   TPIDR_EL0 :: dword 
   TPIDRRO_EL0 :: dword 
   TPIDR_EL1 :: dword 
   TPIDR_EL2 :: dword 
   TPIDR_EL3 :: dword 
   PSTATE    :: ProcState

-- interrupts
   VBAR_EL1 :: VBARType
   VBAR_EL2 :: VBARType
   VBAR_EL3 :: VBARType
   
-- user registers
   REG    :: reg -> dword
   SP_EL0 :: dword
   SP_EL1 :: dword
   SP_EL2 :: dword
   SP_EL3 :: dword
   PC     :: dword

   branch_hint :: BranchType option
   
   -- state of the core automaton
   S :: CORE_STATE
   CURR_INSTR :: word
   CURR_REQ :: MEM_REQUEST
   CURR_REPLY :: MEM_REQUEST option
   CURR_FAULT :: FaultRecord
}


declare C :: CORE_CONFIG

nat TranslationRegime = TR(C.PSTATE.EL)

SCTLRType SCTLR (tr :: nat) = 
{
   match tr
   {
      case 1 => C.SCTLR_EL1
      case 2 => C.SCTLR_EL2
      case 3 => C.SCTLR_EL3
      case _ => UNKNOWN -- unreachable
   }
}


TTBRType TTBR0(tr::nat) =
{
   match tr
   {
      case 1 => C.TTBR0_EL1
      case 2 => C.TTBR0_EL2
      case 3 => C.TTBR0_EL3
      case _ => UNKNOWN -- unreachable
   }
}



dword MAIR(tr::nat) =  
{
   match tr
   {
      case 1 => C.MAIR_EL1
      case 2 => C.MAIR_EL2
      case 3 => C.MAIR_EL3
      case _ => UNKNOWN -- unreachable
   }
}


-- ASID only used if TranslationRegime < 2
bits(16) curr_ASID =
{
    var asid = C.TTBR0_EL1.ASID;
    when C.TCR_EL1.A1 do asid <- C.TTBR1_EL1.ASID;
    when !C.TCR_EL1.AS do asid<15:8> <- 0b0`8; --`
    return asid
}


---------------------------
-- Registers
---------------------------


component X (n::reg) :: bits(N) with N in 8, 16, 32, 64
{
   value = if n == 31 then 0 else [C.REG (n)]
   assign value = -- when N not in set {32, 64} do #ASSERT ("bad X write")
      when n <> 31 do C.REG(n) <- [value]
}

component SP :: bits(N) with N in 8, 16, 32, 64
{
   value =
   {
      sp = if not C.PSTATE.SPS then
              C.SP_EL0
           else match C.PSTATE.EL
                {
                   case 0 => C.SP_EL0
                   case 1 => C.SP_EL1
                   case 2 => C.SP_EL2
                   case 3 => C.SP_EL3
                };
     return [sp]
   }
   assign value = -- when N not in set {32, 64} do #ASSERT ("bad SP write")
   {
      v = [value];
      if not C.PSTATE.SPS then
         C.SP_EL0 <- v
      else match C.PSTATE.EL
           {
              case 0 => C.SP_EL0 <- v
              case 1 => C.SP_EL1 <- v
              case 2 => C.SP_EL2 <- v
              case 3 => C.SP_EL3 <- v
           }
   }
}

unit Hint_Branch (branch_type::BranchType) = C.branch_hint <- Some (branch_type)

unit BranchTo (target::dword, branch_type::BranchType) =
{
   var target = target;
   Hint_Branch (branch_type);
   match C.PSTATE.EL
   {
       case 0 or 1 =>
       {
          when target<55> and C.TCR_EL1.TBI1 do
             target<63:56> <- '11111111';
          when not target<55> and C.TCR_EL1.TBI0 do
             target<63:56> <- '00000000'
       }
       case 2 => when C.TCR_EL2.TBI do target<63:56> <- '00000000'
       case 3 => when C.TCR_EL3.TBI do target<63:56> <- '00000000'
   };
   C.PC <- target
}




-----------------------------
-- General Operations
-----------------------------

bool ConditionTest (cond::cond, N::bool, Z::bool, Cbit::bool, V::bool) =
{
   -- Evaluate base condition
   result =
     match cond<3:1>
     {
        case 0b000 => Z                -- EQ or NE
        case 0b001 => Cbit             -- CS or CC
        case 0b010 => N                -- MI or PL
        case 0b011 => V                -- VS or VC
        case 0b100 => Cbit and not Z   -- HI or LS
        case 0b101 => N == V           -- GE or LT
        case 0b110 => N == V and not Z -- GT or LE
        case 0b111 => true             -- AL
     };

   -- Condition flag values in the set '111x' indicate always true.
   -- Otherwise, invert condition if necessary.
   if cond<0> and cond != 0b1111 then
      not result
   else
      result
}

bool ConditionHolds (cond::cond) =
    ConditionTest (cond, C.PSTATE.N, C.PSTATE.Z, C.PSTATE.C, C.PSTATE.V)

bool list Ones  (n::nat) = PadLeft (true, n, Nil)
bool list Zeros (n::nat) = PadLeft (false, n, Nil)

bits(N) Replicate (l::bool list) = -- when N mod Length(l) == 0 do #ASSERT ("")
   [l ^ (N div Length (l))]

int HighestSetBit (w::bits(N)) = if w == 0 then -1 else [Log2 (w)]

nat CountLeadingZeroBits (w::bits(N)) = [[N] - 0i1 - HighestSetBit (w)]

nat CountLeadingSignBits (w::bits(N)) =
   CountLeadingZeroBits ((w >>+ 1) ?? (w && ~(1 #>> 1))) - 1

bool list Poly32Mod2_loop (i::nat, data::bool list, poly::bool list) =
   if i < 32 then
      data
   else
   {
      data = if data<i> then
                data<Length(data)-1:i> :
                data<i-1:0> ?? PadRight (false, i, poly)
             else
                data;
      Poly32Mod2_loop (i - 1, data, poly)
   }

word Poly32Mod2 (data::bool list, poly::word) =
   return [Poly32Mod2_loop (Length (data) - 1, data, [poly])<31:0>]

{-
word Poly32Mod2 (data::bool list, poly::word) =
{
    N = Length (data);
    var data = data;
    poly = [poly] :: bool list;
    for i in N - 1 .. 32 do
       when data<i> do
          data<i-1:0> <- data<i-1:0> ?? PadRight (false, i, poly);
    return [data<31:0>]
}
-}

-----------------------------
-- Addition and Subtraction
-----------------------------

bits(N) * bool * bool * bool * bool
   AddWithCarry (x::bits(N), y::bits(N), carry_in::bool) =
{
   unsigned_sum = [x] + [y] + [carry_in] :: nat;
   signed_sum = [x] + [y] + [carry_in] :: int;
   result = [unsigned_sum]::bits(N);
   n = Msb(result);
   z = result == 0;
   c = [result] != unsigned_sum;
   v = [result] != signed_sum;
   return result, n, z, c, v
}

unit SetTheFlags (setflags::bool, n::bool, z::bool, c::bool, v::bool) =
   when setflags do
   {
      C.PSTATE.N <- n;
      C.PSTATE.Z <- z;
      C.PSTATE.C <- c;
      C.PSTATE.V <- v
   }

-------------
-- Shifting
-------------

bits(N) LSL (x::bits(N), shift::nat) = x << shift 
bits(N) LSR (x::bits(N), shift::nat) = x >>+ shift
bits(N) ASR (x::bits(N), shift::nat) = x >> shift
bits(N) ROR (x::bits(N), shift::nat) = x #>> shift

construct ShiftType {ShiftType_LSL ShiftType_LSR ShiftType_ASR ShiftType_ROR}

ShiftType DecodeShift (sh::bits(2)) = [sh]

bits(N) ShiftValue (value::bits(N), ty::ShiftType, amount::nat) =
   match ty
   {
      case ShiftType_LSL => LSL (value, amount)
      case ShiftType_LSR => LSR (value, amount)
      case ShiftType_ASR => ASR (value, amount)
      case ShiftType_ROR => ROR (value, amount)
   }

bits(N) ShiftReg (reg::reg, ty::ShiftType, amount::nat) with N in 32, 64 =
   ShiftValue (X(reg)::bits(N), ty, amount)

-------------
-- Extension
-------------

bits(N) ExtendWord (w::bits(M), signed::bool) =
   if signed then SignExtend (w) else ZeroExtend (w)

bits(N) Extend (l::bool list, unsigned::bool) =
   if unsigned or not Head(l) then [l] else [PadLeft (true, N, l)]

construct ExtendType
{
   ExtendType_UXTB ExtendType_UXTH ExtendType_UXTW ExtendType_UXTX
   ExtendType_SXTB ExtendType_SXTH ExtendType_SXTW ExtendType_SXTX
}

ExtendType DecodeRegExtend (ext::bits(3)) = [ext]

bits(N) ExtendValue (value::bits(N), ty::ExtendType, sh::nat)
        with N in 8, 16, 32, 64 =
{
   value = [value] :: bool list;
   unsigned, len =
      match ty
      {
         case ExtendType_SXTB => false, 8
         case ExtendType_SXTH => false, 16
         case ExtendType_SXTW => false, 32
         case ExtendType_SXTX => false, 64
         case ExtendType_UXTB => true, 8
         case ExtendType_UXTH => true, 16
         case ExtendType_UXTW => true, 32
         case ExtendType_UXTX => true, 64
      };
   len = Min (len, N - sh);
   Extend (value<len-1:0> << sh, unsigned)
}

bits(N) ExtendReg (reg::reg, ty::ExtendType, sh::nat) with N in 8, 16, 32, 64 =
   ExtendValue (X(reg)::bits(N), ty, sh)

-------------
-- Masks
-------------

(bits(M) * bits(M)) option DecodeBitMasks
   (immN::bits(1), imms::bits(6), immr::bits(6), immediate::bool) =
{
   x = immN : ~imms;
   len = HighestSetBit (x);
   if len < 1 then
      None
   else
   {
      len = [len]::nat;
      levels = [Ones (len)]`6;
      S = imms && levels;
      R = immr && levels;
      if immediate and S == levels then
         None
      else
      {
         diff = [S - R] :: bool list;
         esize = 0n2 ** len;
         d = diff<len-1:0>;
         welem = PadLeft (false, esize, Ones ([S] + 0n1));
         telem = PadLeft (false, esize, Ones ([d] + 0n1));
         wmask = Replicate (welem #>> [R]);
         tmask = Replicate (telem);
         return Some (wmask, tmask)
      }
   }
}


