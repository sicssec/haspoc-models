use "/home/christoph/HOL/examples/l3-machine-code/lib/Ptree.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/Ptree.sml";
use "/home/christoph/HOL/examples/l3-machine-code/lib/MutableMap.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/MutableMap.sml";
use "/home/christoph/HOL/examples/l3-machine-code/lib/IntExtra.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/IntExtra.sml";
use "/home/christoph/HOL/examples/l3-machine-code/lib/Nat.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/Nat.sml";
use "/home/christoph/HOL/examples/l3-machine-code/lib/Set.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/Set.sml";
use "/home/christoph/HOL/examples/l3-machine-code/lib/L3.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/L3.sml";
use "/home/christoph/HOL/examples/l3-machine-code/lib/Bitstring.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/Bitstring.sml";
use "/home/christoph/HOL/examples/l3-machine-code/lib/BitsN.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/BitsN.sml";

use "dharma8.sig";
use "dharma8.sml";

fun core_step (m2c, mmu2c, c_out) = 
    let val c_or = (fn c_state:(dharma8.MEM_REQUEST * dharma8.CORE_STATE) => if m2c <> NONE then m2c else mmu2c)
	val (used_msg, rpl) = dharma8.core_interface c_or
	val c_out1 = if rpl = NONE then c_out else rpl
	val m2c1 = if used_msg = m2c then NONE else m2c
	val mmu2c1 = if used_msg = mmu2c then NONE else mmu2c
    in
	(m2c1, mmu2c1, c_out1)
    end;


fun mem_step (mmu2m, m2c, m2mmu) = 
    let val m_or = ((fn chan => fn m_state:(dharma8.MEM_REQUEST * dharma8.MEM_STATE) => (chan, dharma8.nofault)) mmu2m)
	val (used_msg, (rpl, rcvr)) = dharma8.mem_interface m_or
	val m2c1 = if rpl <> NONE andalso rcvr = dharma8.rcvr_core then rpl else m2c
	val m2mmu1 = if rpl <> NONE andalso rcvr = dharma8.rcvr_mmu then rpl else m2mmu
	val mmu2m1 = if used_msg <> NONE then NONE else mmu2m
    in
	(mmu2m1, m2c1, m2mmu1)
    end;

fun mmu_step (c2mmu, m2mmu, mmu2c, mmu2m) = 
    let val mmu_or = (fn mmu_conf:(dharma8.MMU_config)  => (c2mmu, m2mmu))
	val (used_c2mmu, (used_m2mmu, (rpl_c, rpl_m))) = dharma8.mmu_interface mmu_or
	val mmu2c1 = if rpl_c = NONE then mmu2c else rpl_c
	val c2mmu1 = if used_c2mmu <> NONE then NONE else c2mmu
	val mmu2m1 = if rpl_m = NONE then mmu2m else rpl_m
	val m2mmu1 = if used_m2mmu <> NONE then NONE else m2mmu
    in
	(c2mmu1, m2mmu1, mmu2c1, mmu2m1)
    end;

type MEM_COMPS = dharma8.MEM_CONFIG * (BitsN.nbit dharma8.Map.map) * (dharma8.TrustZonePermission dharma8.Map.map);
type STATE = dharma8.CORE_CONFIG * dharma8.MMU_config * (MEM_COMPS) * dharma8.MEM_REQUEST option * (dharma8.MEM_REQUEST * dharma8.MMU_REGS) option * dharma8.MEM_REQUEST option * dharma8.MEM_REQUEST option * dharma8.MEM_REQUEST option;

fun one_step((c, mmu, (mc,m,tzc), m2c, c2mmu, m2mmu, mmu2c, mmu2m), id) =
    let val _ = dharma8.C := c
	val _ = dharma8.MC := mc 
	val _ = dharma8.PM := m
	val _ = dharma8.TZC := tzc
	val _ = dharma8.MMU := mmu
        val (m2c1, c2mmu1, m2mmu1, mmu2c1, mmu2m1) = (
	    if id = 1 then 
		let val (m2c2, mmu2c2, c2mmu2) = core_step (m2c, mmu2c, c2mmu) in 
		    (m2c2, c2mmu2, m2mmu, mmu2c2, mmu2m)
		end
	    else if id = 2 then 
		let val (c2mmu2, m2mmu2, mmu2c2, mmu2m2) = mmu_step (c2mmu, m2mmu, mmu2c, mmu2m) in 
		    (m2c, c2mmu2, m2mmu2, mmu2c2, mmu2m2)
		end
	    else 
	    	let val (mmu2m2, m2c2, m2mmu2) = mem_step (mmu2m, m2c, m2mmu) in
	    	    (m2c2, c2mmu, m2mmu2, mmu2c, mmu2m2)
		end
	)
    in
	(!dharma8.C, !dharma8.MMU, (!dharma8.MC,!dharma8.PM,!dharma8.TZC), m2c1, c2mmu1, m2mmu1, mmu2c1, mmu2m1)
    end;

(* execute memory steps in one atomic step *)
fun mem_step_atomic c = one_step(one_step(c,3),3);

(* fast forward MMU until memory access or core reply (fault) *)
fun mmu_step_ff (c : STATE) = 
    let 
    	val c' = ref c 
    in
        while ((#7 (!c')) = NONE andalso (#8 (!c')) = NONE) do c' := one_step(!c',2);
	!c'
    end;


(* fast forward MMU and memory steps until next ST1 memory access or core reply (fault) *)
fun mmu_step_ff_ST2_atomic c : STATE = 
    let 
    	val c' = ref c 
	fun stop_loop_fault(s : STATE) = (#7 s) <> NONE
	fun ST2_lookup(s : STATE) = (#8 s) <> NONE andalso #secondstage(#fault(#desc(valOf(#8 s))))
	fun stop_loop_mem(s : STATE) = (#8 s) <> NONE andalso not (ST2_lookup(s))
    in
        while not (stop_loop_fault(!c') orelse stop_loop_mem(!c')) do (
	      c' := mmu_step_ff (!c');
	      if ST2_lookup(!c') then c' := mem_step_atomic (!c') else ()
	);
	!c'
    end;

(* complete atomic MMU and depending memory steps *) 
fun mmu_step_atomic c : STATE = 
    let 
    	val c' = ref c 
	fun stop_loop_fault(s : STATE) = (#7 s) <> NONE
	fun table_lookup(s : STATE) = (#8 s) <> NONE andalso #acctype(valOf(#8 s)) = dharma8.AccType_PTW
	fun stop_loop_mem(s : STATE) = (#8 s) <> NONE andalso not (table_lookup(s))
    in
        while not (stop_loop_fault(!c') orelse stop_loop_mem(!c')) do (
	      c' := mmu_step_ff (!c');
	      if table_lookup(!c') then c' := mem_step_atomic (!c') else ()
	);
	!c'
    end;

(* fast forward core to next MMU request, for now halt at faults *)
fun core_step_ff (c : STATE) = 
    let 
    	val c' = ref c 
	fun stop_loop(s : STATE) = (#5 s) <> NONE orelse (#S(#1 s) = dharma8.core_issue andalso #typ(#CURR_FAULT(#1 s)) <> dharma8.Fault_None)
    in
        while not(stop_loop (!c')) do c' := one_step(!c',1);
	!c'
    end;

(* atomic combination of core, mmu, and memory cycle *)
fun cmm_cycle (c : STATE) = 
    let 
    	val c' = core_step_ff c
	val c'' = if (#5 (c')) <> NONE then mmu_step_atomic c' else c'
    in
	if (#8 (c'')) <> NONE then mem_step_atomic c'' else c''
    end;

(* sequential execution of whole program until PC reaches certain address *)
fun execute_until_pc (c : STATE, pc : string) = 
    let 
    	val c' = ref c 
	val x = valOf(BitsN.fromHexString(pc,64))
	fun stop_loop(s : STATE) = #PC(#1 (!c')) = x orelse (#S(#1 s) = dharma8.core_issue andalso #typ(#CURR_FAULT(#1 s)) <> dharma8.Fault_None)
    in
	while not(stop_loop(!c')) do c' := cmm_cycle (!c');
	!c'
    end;


(* I/O *)

fun byteToHex b = 
    let 
    	val x = Word8.toString b
    in
	if (String.size x) = 1 then "0" ^ x else x
    end;

fun fileToHex f = 
    let 
    	val strm = TextIO.openIn f
	val s = Byte.stringToBytes(valOf(TextIO.inputLine strm))
	val i = ref 0
	val x = ref ""	    	
    in
	TextIO.closeIn strm;
	while (!i < (Word8Vector.length s - 1)) do (
	      x := (!x) ^ byteToHex(Word8Vector.sub(s,!i));
	      i := (!i) + 1
	);
	!x
    end;	      

fun b2s b = if b then "1" else "0";
val bl2s = foldl (fn (b, s) => s ^ b2s(b)) "";
fun b2B b l = valOf(BitsN.fromBinString(b,l));

fun bs2bs64 s = if size(s) < 64 then bs2bs64 ("0" ^ s) else s;

fun X2X48 s = if size(s) < 12 then X2X48 ("0" ^ s) else s;
 
fun show_cstate ( c : STATE ) = 
    case #S(#1 c) of 
    	   dharma8.core_issue => "core_issue"
	 | dharma8.core_fetch => "core_fetch"
	 | dharma8.core_dec => "core_dec"
	 | dharma8.core_mem => "core_mem";

fun show_req (r : dharma8.MEM_REQUEST) = 
    let
	val _ = print("va:0x" ^ (BitsN.toHexString(#va r)) ^ " pa:0x" ^ (BitsN.toHexString(#paddress(#desc r))) ^ " size:" ^ Int.toString(#bytesize r))
	val d = #data r
	val _ = if d = [] then print(" typ:") else print(" write_data:" ^ (bl2s d) ^ " typ:")
	val _ = PolyML.print (#acctype r)
    in
	()
    end;

fun show_nonempty_gpr (c : STATE, i) = 
    let 
    	val gpr = BitsN.toBinString(dharma8.Map.lookup(#REG(#1 c),i))
    in
	if gpr <> "0" then (
	   print ("X" ^ Int.toString(i));
	   if i > 10 then () else print " ";
	   print (" = " ^ bs2bs64(gpr) ^ "\n")
	)
	else
	   ()
    end;	

fun show_gpr (c : STATE ) = 
    let 
    	val gpr = #REG(#1 c)
	val i = ref 0
    in 
	while (!i < 32) do (
	      show_nonempty_gpr(c,!i);
	      i := (!i) + 1
	)
    end;

fun descu ((a1,a2), (b1,b2)) = BitsN.<+(a1,b1);

fun insert f a [] = [a]
 |  insert f a (b::l) = if f(a,b) then b::(insert f a l) else a::(b::l);

fun ins_sort f [] = []
 |  ins_sort f (a::l) = insert f a (ins_sort f l);

fun descusort l = ins_sort descu l;



fun show_mem (c : STATE) = 
    let
	val w = descusort (dharma8.write_list (#1(#3 c),#2(#3 c)))
	fun print_entry (a,b) = print("0x" ^ X2X48(BitsN.toHexString a) ^ ": " ^ bs2bs64(BitsN.toBinString b) ^ "\n")
	fun print_list [] = ()
	 |  print_list (a::l) = (print_entry a; print_list l)
    in
	if w = [] then print "No writes recorded\n" else print_list w
    end;

fun show (c : STATE) = 
    let
	val _ = print ("CORE_PC    = 0x" ^ (BitsN.toHexString(#PC(#1 c))) ^ "\n")
    	val _ = print ("CORE_STATE = ")
	val _ = PolyML.print (#S(#1 c))
	val _ = print ("CORE_INSTR = ")
	val _ = PolyML.print(dharma8.Decode (#CURR_INSTR (#1 c)))
	val _ = print ("CORE_FAULT = ")
	val _ = PolyML.print(#typ(#CURR_FAULT (#1 c)),BitsN.toHexString(#ipaddress(#CURR_FAULT(#1 c))))
	val _ = print ("CHAN_C2MMU = ")
	val _ = PolyML.print((#5 c))
	val _ = print ("CHAN_MMU2C = ")
	val _ = PolyML.print((#7 c))
	val _ = print ("MMU_STATE  = ")
	val _ = PolyML.print(#S (#2 c))
	val _ = print ("MMU_REQ    = ")
	val _ = show_req(#CURR_REQ (#2 c))
	val _ = print ("MMU_ST1    = ")
	val _ = PolyML.print(#ST1 (#2 c))
	val _ = print ("MMU_FAULT1 = ")
	val _ = PolyML.print(#typ(#F1 (#2 c)),BitsN.toHexString(#ipaddress(#F1(#2 c))))
	val _ = print ("MMU_ST2    = ")
	val _ = PolyML.print(#ST2 (#2 c))
	val _ = print ("MMU_FAULT2 = ")
	val _ = PolyML.print(#typ(#F2 (#2 c)),BitsN.toHexString(#ipaddress(#F2(#2 c))))
	val _ = print ("CHAN_M2MMU = ")
	val _ = PolyML.print((#6 c))
	val _ = print ("CHAN_MMU2M = ")
	val _ = PolyML.print((#8 c))
	val _ = print ("MEM_STATE  = ")
	val _ = PolyML.print(#S (#1(#3 c)))
	val _ = print ("MEM_REQ    = ")
	val _ = show_req(#curr (#1(#3 c)))
	val _ = print ("CHAN_M2C   = ")
	val _ = PolyML.print((#4 c))
	val _ = show_gpr c
	val _ = show_mem c
    in
	()
    end;

(* helper functions *)

val zero_32 = BitsN.fromInt(0,32);
fun GPR r = (dharma8.X(64)) (BitsN.fromNat(r,5));
fun decode_fetch (c:STATE) = dharma8.Decode(valOf(BitsN.fromBinString(bl2s(#data(valOf(#4 c))),32)));

fun set_state (c : STATE) = 
    let
        val _ = dharma8.C := (#1 c)
        val _ = dharma8.MMU := (#2 c)
	val _ = dharma8.MC := #1(#3 c)
	val _ = dharma8.PM := #2(#3 c)
	val _ = dharma8.TZC := #3(#3 c)
    in
	c
    end;

fun get_state (c : STATE) = (!dharma8.C, !dharma8.MMU, (!dharma8.MC,!dharma8.PM,!dharma8.TZC), #4 c, #5 c, #6 c, #7 c, #8 c);

(* configuring trustzone controler *)

fun make_TZC_perm(sr,sw,nsr,nsw) = {SR = sr, SW = sw, NSR = nsr, NSW = nsw} : dharma8.TrustZonePermission;

fun upd_TZC (tzc, start, pages, perm : dharma8.TrustZonePermission) =
    if pages = 0 then tzc else upd_TZC(dharma8.Map.update(tzc,start,perm),start+1,pages-1,perm);

fun add_TZC_region (m : MEM_COMPS, start : string, pages : int, perm : dharma8.TrustZonePermission) =
    let
	val tzc = (#3 m)
	val s = BitsN.toNat(valOf(BitsN.fromBinString(substring(dharma8.xtob(start),16,36),36)))
	val newtzc = upd_TZC(tzc,s,pages,perm)
    in
	((#1 m), (#2 m), newtzc) : MEM_COMPS
    end;

