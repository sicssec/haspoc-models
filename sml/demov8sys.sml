use "v8testbed.sml";

(* initialization *)
   
(* ST1: 2MB blocks, startl: 1, lastl: 2, 4KB granularity
   ST2: 512MB blocks, startl: 2, lastl: 2, 64KB granularity *)

(* setup both stages *) 

val _ = dharma8.mmu_setup_std_both();
(* set non-secure, EL0 *)
val _ = dharma8.mode_setup(true, 0);
val _ = dharma8.install_elf(fileToHex("asm/a.out"), ("100010000", "00010000"));

val _ = dharma8.init_automaton();

(* set link register to "0001029A", misaligned *)
val _ = dharma8.set_reg(30,"0001029A");

val m2c = NONE:dharma8.MEM_REQUEST option;
val c2mmu = NONE:(dharma8.MEM_REQUEST * dharma8.MMU_REGS) option;
val m2mmu = NONE:dharma8.MEM_REQUEST option;
val mmu2c = NONE:dharma8.MEM_REQUEST option;
val mmu2m = NONE:dharma8.MEM_REQUEST option;

(* configure secure memory, make 0x100000000 to 0x100400000 and page tables non-secure *)
val m1 = add_TZC_region ((!dharma8.MC,!dharma8.PM,!dharma8.TZC), "100000000", 1024, make_TZC_perm(true,true,true,true));
val m2 = add_TZC_region (m1, "004000000", 16348, make_TZC_perm(true,true,true,true));

val c_init = (!dharma8.C, !dharma8.MMU, m2, m2c, c2mmu, m2mmu, mmu2c, mmu2m);



(* execute whole program in one step *)

val _ = show(execute_until_pc(c_init,"0001029A"));


(* atomic transitions

val c_1 = core_step_ff c_init;		(* Core transitions until fetch request to MMU *)
val c_2 = mmu_step_ff c_1;		(* MMU transitions until first memory request (ST2) *)
val c_3 = mem_step_atomic c_2;		(* atomic memory transition *)
val c_4 = mmu_step_ff c_3;		(* MMU transitions until next memory request (ST1) *)
val c_5 = mem_step_atomic c_4;		(* atomic memory transition *)
val c_6 = mmu_step_ff_ST2_atomic c_5;	(* MMU and memory transitions until the next ST1 memory request *)
val c_7 = mem_step_atomic c_6;		(* atomic memory transition *)
val c_8 = mmu_step_ff_ST2_atomic c_7;	(* MMU and memory transitions until the final memory request for fetch *)
val c_9 = mem_step_atomic c_8;		(* atomic memory transition *)
val c_10 = core_step_ff c_9;		(* Core, finish instruction execution, start next one *)
val c_11 = mmu_step_atomic c_10;	(* MMU and memory transitions for complete translation of fetch address *)
val c_12 = mem_step_atomic c_11;	(* atomic memory transition *)
val c_13 = core_step_ff c_12;		(* Core, decode, start memory operation *)
val c_14 = mmu_step_atomic c_13;	(* MMU and memory transitions for complete translation of load address *)
val c_15 = mem_step_atomic c_14;	(* atomic memory transition *)
val c_16 = cmm_cycle c_15; 		(* atomic core, mmu, memory cycle, finish fetch "add	X2, X2, #1" *)
val c_17 = cmm_cycle c_16; 		(* atomic core, mmu, memory cycle, execute X2 := X2 + 1, next, finish fetch "mov  X0, #65536" *)
val c_18 = cmm_cycle c_17; 		(* atomic core, mmu, memory cycle, execute X0 := 0, next, finish fetch "str	X2, [X0, #40]!" *)
val c_19 = cmm_cycle c_18; 		(* atomic core, mmu, memory cycle, decode, request execute store operation *)
val c_20 = cmm_cycle c_19; 		(* atomic core, mmu, memory cycle, write back address to X0, fetch "mov     X30, X1" *)
val c_21 = cmm_cycle c_20; 		(* atomic core, mmu, memory cycle, execute LR := X1, next, finish fetch "ret" *)
val c_22 = cmm_cycle c_21; 		(* atomic core, mmu, memory cycle, return to "10001029A", alignment fault on fetch *)
val c_23 = cmm_cycle c_22;		(* atomic core, mmu, memory cycle, loop *)

*)

(* step by step simulation 

val c_1 = one_step(c_init, 1); 	 (* Core init, req fetch translation *) 
val c_2 = one_step(c_1, 2);	 (* MMU ST1 init *)
val c_3 = one_step(c_2, 2);	 (* MMU ST1, prepare first lookup, ST2 init *)
val c_4 = one_step(c_3, 2);	 (* MMU ST2, prepare first lookup *)
val c_5 = one_step(c_4, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_6 = one_step(c_5, 3);	 (* Memory init, receive request *)
val c_7 = one_step(c_6, 3);	 (* Memory serve, reply to MMU *)
val c_8 = one_step(c_7, 2);	 (* MMU ST2, receive descriptor, extend walk, finalize and return to ST1 *)
val c_9 = one_step(c_8, 2);	 (* MMU ST1, request next pte on level 1 *)
val c_10 = one_step(c_9, 3);	 (* Memory init, receive request *)
val c_11 = one_step(c_10, 3);	 (* Memory serve, reply to MMU *)
val c_12 = one_step(c_11, 2);	 (* MMU ST1, receive descriptor, extend walk, prepare next lookup, ST2 init *)
val c_13 = one_step(c_12, 2);	 (* MMU ST2, prepare lookup *)
val c_14 = one_step(c_13, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_15 = one_step(c_14, 3);	 (* Memory init, receive request *)
val c_16 = one_step(c_15, 3);	 (* Memory serve, reply to MMU *)
val c_17 = one_step(c_16, 2);	 (* MMU ST2, receive descriptor, extend walk, finalize and return to ST1 *)
val c_18 = one_step(c_17, 2);	 (* MMU ST1, request next pte on level 1 *)
val c_19 = one_step(c_18, 3);	 (* Memory init, receive request *)
val c_20 = one_step(c_19, 3);	 (* Memory serve, reply to MMU *)
val c_21 = one_step(c_20, 2);	 (* MMU ST1, receive descriptor, extend walk, prepare final memory access, ST2 init *)
val c_22 = one_step(c_21, 2);	 (* MMU ST2, prepare lookup *)
val c_23 = one_step(c_22, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_24 = one_step(c_23, 3);	 (* Memory init, receive request *)
val c_25 = one_step(c_24, 3);	 (* Memory serve, reply to MMU *)
val c_26 = one_step(c_25, 2);	 (* MMU ST2, finish walk, combine descriptors *)
val c_27 = one_step(c_26, 2);	 (* MMU ST1 finish, send fetch request to mem*)
val c_28 = one_step(c_27, 3);	 (* Memory init, receive request *)
val c_29 = one_step(c_28, 3);	 (* Memory serve, reply to core *)
val c_30 = one_step(c_29, 1);	 (* Core receive fetch result, go to decode phase *)
val c_31 = one_step(c_30, 1);	 (* Core decode and execute, MOV instruction, X1 := LR *)

val c_32 = one_step(c_31, 1);	 (* Core init, req fetch translation *) 
val c_33 = one_step(c_32, 2);	 (* MMU ST1 init *)
val c_34 = one_step(c_33, 2);	 (* MMU ST1, prepare first lookup, ST2 init *)
val c_35 = one_step(c_34, 2);	 (* MMU ST2, prepare first lookup *)
val c_36 = one_step(c_35, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_37 = one_step(c_36, 3);	 (* Memory init, receive request *)
val c_38 = one_step(c_37, 3);	 (* Memory serve, reply to MMU *)
val c_39 = one_step(c_38, 2);	 (* MMU ST2, receive descriptor, extend walk, finalize and return to ST1 *)
val c_40 = one_step(c_39, 2);	 (* MMU ST1, request next pte on level 1 *)
val c_41 = one_step(c_40, 3);	 (* Memory init, receive request *)
val c_42 = one_step(c_41, 3);	 (* Memory serve, reply to MMU *)
val c_43 = one_step(c_42, 2);	 (* MMU ST1, receive descriptor, extend walk, prepare next lookup, ST2 init *)
val c_44 = one_step(c_43, 2);	 (* MMU ST2, prepare lookup *)
val c_45 = one_step(c_44, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_46 = one_step(c_45, 3);	 (* Memory init, receive request *)
val c_47 = one_step(c_46, 3);	 (* Memory serve, reply to MMU *)
val c_48 = one_step(c_47, 2);	 (* MMU ST2, receive descriptor, extend walk, finalize and return to ST1 *)
val c_49 = one_step(c_48, 2);	 (* MMU ST1, request next pte on level 1 *)
val c_50 = one_step(c_49, 3);	 (* Memory init, receive request *)
val c_51 = one_step(c_50, 3);	 (* Memory serve, reply to MMU *)
val c_52 = one_step(c_51, 2);	 (* MMU ST1, receive descriptor, extend walk, prepare final memory access, ST2 init *)
val c_53 = one_step(c_52, 2);	 (* MMU ST2, prepare lookup *)
val c_54 = one_step(c_53, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_55 = one_step(c_54, 3);	 (* Memory init, receive request *)
val c_56 = one_step(c_55, 3);	 (* Memory serve, reply to MMU *)
val c_57 = one_step(c_56, 2);	 (* MMU ST2, finish walk, combine descriptors *)
val c_58 = one_step(c_57, 2);	 (* MMU ST1 finish, send fetch request to mem*)
val c_59 = one_step(c_58, 3);	 (* Memory init, receive request *)
val c_60 = one_step(c_59, 3);	 (* Memory serve, reply to core *)
val c_61 = one_step(c_60, 1);	 (* Core receive fetch result, go to decode phase *)
val c_62 = one_step(c_61, 1);	 (* Core decode memory operation, send load request *)
val c_63 = one_step(c_62, 2);	 (* MMU ST1 init *)
val c_64 = one_step(c_63, 2);	 (* MMU ST1, prepare first lookup, ST2 init *)
val c_65 = one_step(c_64, 2);	 (* MMU ST2, prepare first lookup *)
val c_66 = one_step(c_65, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_67 = one_step(c_66, 3);	 (* Memory init, receive request *)
val c_68 = one_step(c_67, 3);	 (* Memory serve, reply to MMU *)
val c_69 = one_step(c_68, 2);	 (* MMU ST2, receive descriptor, extend walk, finalize and return to ST1 *)
val c_70 = one_step(c_69, 2);	 (* MMU ST1, request next pte on level 1 *)
val c_71 = one_step(c_70, 3);	 (* Memory init, receive request *)
val c_72 = one_step(c_71, 3);	 (* Memory serve, reply to MMU *)
val c_73 = one_step(c_72, 2);	 (* MMU ST1, receive descriptor, extend walk, prepare next lookup, ST2 init *)
val c_74 = one_step(c_73, 2);	 (* MMU ST2, prepare lookup *)
val c_75 = one_step(c_74, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_76 = one_step(c_75, 3);	 (* Memory init, receive request *)
val c_77 = one_step(c_76, 3);	 (* Memory serve, reply to MMU *)
val c_78 = one_step(c_77, 2);	 (* MMU ST2, receive descriptor, extend walk, finalize and return to ST1 *)
val c_79 = one_step(c_78, 2);	 (* MMU ST1, request next pte on level 1 *)
val c_80 = one_step(c_79, 3);	 (* Memory init, receive request *)
val c_81 = one_step(c_80, 3);	 (* Memory serve, reply to MMU *)
val c_82 = one_step(c_81, 2);	 (* MMU ST1, receive descriptor, extend walk, prepare final memory access, ST2 init *)
val c_83 = one_step(c_82, 2);	 (* MMU ST2, prepare lookup *)
val c_84 = one_step(c_83, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_85 = one_step(c_84, 3);	 (* Memory init, receive request *)
val c_86 = one_step(c_85, 3);	 (* Memory serve, reply to MMU *)
val c_87 = one_step(c_86, 2);	 (* MMU ST2, finish walk, combine descriptors *)
val c_88 = one_step(c_87, 2);	 (* MMU ST1 finish, send load request to mem*)
val c_89 = one_step(c_88, 3);	 (* Memory init, receive request *)
val c_90 = one_step(c_89, 3);	 (* Memory serve, reply to core *)
val c_91 = one_step(c_90, 1);	 (* Core receive load data, write back, X2 := m_8("100010020") (="sys8vMRA") *)

*)