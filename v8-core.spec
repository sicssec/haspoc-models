--------------------------------------------------------------------
-- This a high-level definition of the ARMv8 instruction core     --
-- Author: Christoph Baumann, KTH CSC Stockholm		          --
--------------------------------------------------------------------


{-

val () = Runtime.LoadF "v8-base_types.spec, v8-mem_types.spec, v8-base.spec, v8-mem.spec, v8-core.spec";

-}

exception MEM_INPUT_MISMATCH

-- "local" oracle returns memory input for current state (defined in HOL/SML using input channel representation)
-- cannot depend on non-equality types, e.g., sets and maps, thus CORE_CONFIG does not work (contains register map)
type CORE_ORACLE = (MEM_REQUEST * CORE_STATE) -> (MEM_REQUEST option)


-------------------------------
-- helper functions
-------------------------------

MMU_REGS mmu_regs (va :: VA, tr :: nat) = 
{
  var R;
  R.sctlr <- SCTLR(tr);
  R.ttbr <- if (va<63> and tr < 2) then C.TTBR1_EL1 else TTBR0(tr);
  R.tcr1 <- C.TCR_EL1;
  R.tcr23 <- match tr {
      case 3 => C.TCR_EL3
      case 2 => C.TCR_EL2
      case _ => UNKNOWN};
  R.mair <- MAIR(tr);
  R.idmm <- C.ID_AA64MMFR0_EL1;
  R.hcr <- C.HCR_EL2;
  R.vttbr <- C.VTTBR_EL2;
  R.vtcr <- C.VTCR_EL2;

  return R
}

-- predicate for checking if memory access is necessary / possible
bool access_memory() = match Decode(C.CURR_INSTR) {
    case LoadStore (_) or MemoryBarrier (_) or System (SystemInstruction (_)) => true
    case _ => false
}

-- effective address for memory accesses
VA ea (n :: reg, postindex :: bool, offset :: dword, rn_unknown :: bool) = 
{
   address = if n == 31 then
                SP
             else if rn_unknown then
                UNKNOWN
             else
                X(n);
   return (if postindex then address else address + offset)
}

bool list write_data(t :: reg, t2 :: reg, size :: bits(N), pair :: bool, rt_unknown :: bool) with N in 8, 16, 32, 64, 128 = 
{

         tdata`N = match N { --`
	     case 8  => [X(t)`8] -- `
	     case 16 => [X(t)`16] -- `
	     case 32 => [X(t)`32] -- `
	     case _  => [X(t)`64] -- `
	 };

	 pairdata`N = match N { --`
	     case 64  => if BigEndian then
  		 [X(t)`32 : X(t2)`32]
	     else
	         [X(t2)`32 : X(t)`32]
	     case 128 => if BigEndian then
		 [X(t)`64 : X(t2)`64]
	     else
	         [X(t2)`64 : X(t)`64]
	     case _   => UNKNOWN 
		 };
	 
	 data = if rt_unknown then UNKNOWN
                  else if pair then pairdata
		  else tdata;

	 return [data]
}

AccType memop_type (memop :: MemOp) = match memop {
    case MemOp_PREFETCH => AccType_IFETCH
    case _ => AccType_NORMAL
}

unit run_curr() = {
	C.branch_hint <- None;
	Run (Decode (C.CURR_INSTR));
	when not (IsSome (C.branch_hint)) do C.PC <- C.PC + 4
}

-------------------
-- Core transitions
-------------------


(MMU_REQUEST option) CORE_ISSUE () =
{
    drop_result(Mem(C.PC, 4, AccType_IFETCH));
    
    var mmu_req = None :: MMU_REQUEST option;

    tr = if C.CURR_REQ.EL == 0 then 1 else C.CURR_REQ.EL;

    when (C.CURR_FAULT.typ == Fault_None) do {
         mmu_req <- Some(C.CURR_REQ, mmu_regs(C.CURR_REQ.va, tr));
	 C.S <- core_fetch
    };
    
    return mmu_req
}

bool match_req_rpl(req :: MEM_REQUEST, rpl :: MEM_REQUEST option) = 
{
    data = if req.write then Some(req.data) else None;
    match_reply(rpl, req.va, req.bytesize, req.acctype, data, req.EL, req.ns)
}

-- TODO: for non-atomic requests loop here to receive all parts
unit CORE_FETCH(reply :: MEM_REQUEST) = 
{
    C.CURR_REPLY <- Some(reply);

    if match_req_rpl(C.CURR_REQ, C.CURR_REPLY) then {
	    instr = Mem(C.PC, 4, AccType_IFETCH);
	    when (C.CURR_FAULT.typ == Fault_None) do C.CURR_INSTR <- [instr];
	    C.S <- core_dec}
    else #MEM_INPUT_MISMATCH
}


unit CORE_DECEXEC() = 
{
    when (C.CURR_FAULT.typ == Fault_None) do run_curr();
    C.S <- core_issue
}


VA * (bool list) * AccType * MemOp * nat decode_mem(instr :: word) = match Decode(instr) {
	case LoadStore (LoadStoreImmediate@8 (size, regsize_word, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, offset, n, t)) =>
	    ea(n, postindex, offset, false),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    1
	case LoadStore (LoadStoreImmediate@16 (size, regsize_word, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, offset, n, t)) =>
	    ea(n, postindex, offset, false),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    2
	case LoadStore (LoadStoreImmediate@32 (size, regsize_word, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, offset, n, t)) =>
	    ea(n, postindex, offset, false),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    4
	case LoadStore (LoadStoreImmediate@64 (size, regsize_word, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, offset, n, t)) =>
	    ea(n, postindex, offset, false),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    8

	case LoadStore( LoadStoreRegister@8 (size, regsize_word, memop, signed, m, extend_type, shift, n, t)) =>
	    ea(n, false, ExtendReg (m, extend_type, shift), false),    write_data(t, t, size, false, false),    AccType_NORMAL,    memop,    1
	case LoadStore( LoadStoreRegister@16 (size, regsize_word, memop, signed, m, extend_type, shift, n, t)) =>
	    ea(n, false, ExtendReg (m, extend_type, shift), false),    write_data(t, t, size, false, false),    AccType_NORMAL,    memop,    2
	case LoadStore( LoadStoreRegister@32 (size, regsize_word, memop, signed, m, extend_type, shift, n, t)) =>
	    ea(n, false, ExtendReg (m, extend_type, shift), false),    write_data(t, t, size, false, false),    AccType_NORMAL,    memop,    4
	case LoadStore( LoadStoreRegister@64 (size, regsize_word, memop, signed, m, extend_type, shift, n, t)) =>
	    ea(n, false, ExtendReg (m, extend_type, shift), false),    write_data(t, t, size, false, false),    AccType_NORMAL,    memop,    8

	case LoadStore (LoadLiteral@32 (size, memop, signed, offset, t)) =>
	    C.PC + offset,    UNKNOWN,    memop_type(memop),    memop,    4
	case LoadStore (LoadLiteral@64 (size, memop, signed, offset, t)) =>
	    C.PC + offset,    UNKNOWN,    memop_type(memop),    memop,    8

	-- only first request for pairs here
	case LoadStore (LoadStorePair@32 (size, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, offset, n, t, t2)) =>
	    ea(n, postindex, offset, false),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    4
	case LoadStore (LoadStorePair@64 (size, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, offset, n, t, t2)) =>
	    ea(n, postindex, offset, false),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    8

	case LoadStore (LoadStoreAcquire@8 (size, memop, acctype, excl, rn_unknown, rt_unknown, s, n, t)) => 
	    ea(n, false, 0, rn_unknown),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    1
	case LoadStore (LoadStoreAcquire@16 (size, memop, acctype, excl, rn_unknown, rt_unknown, s, n, t)) => 
	    ea(n, false, 0, rn_unknown),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    2
	case LoadStore (LoadStoreAcquire@32 (size, memop, acctype, excl, rn_unknown, rt_unknown, s, n, t)) => 
	    ea(n, false, 0, rn_unknown),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    4
	case LoadStore (LoadStoreAcquire@64 (size, memop, acctype, excl, rn_unknown, rt_unknown, s, n, t)) => 
	    ea(n, false, 0, rn_unknown),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    8

        case LoadStore (LoadStoreAcquirePair@64 (size, memop, acctype, rn_unknown, rt_unknown, s, n, t, t2)) =>
	    ea(n, false, 0, rn_unknown),    write_data(t, t2, size, true, rt_unknown),    acctype,    memop,    8
        case LoadStore (LoadStoreAcquirePair@128 (size, memop, acctype, rn_unknown, rt_unknown, s, n, t, t2)) =>
	    ea(n, false, 0, rn_unknown),    write_data(t, t2, size, true, rt_unknown),    acctype,    memop,    16
	case _ => UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN
}


MMU_REQUEST CORE_DECMEM () = 
{
    match Decode(C.CURR_INSTR) {
      case System (SystemInstruction (op1, op2, CRn, CRm, L, Rt)) => handle_sysop(op1, op2, CRn, CRm, L, Rt)
      case MemoryBarrier (opc, domain, types) => MemBarrier(opc, domain, types)
      case _ => {
	    
    address, data, acctype, memop, bytesize  = decode_mem(C.CURR_INSTR);

    address2, data2, acctype2, memop2, bytesize2, pair  = match Decode(C.CURR_INSTR) {
	case LoadStore (LoadStorePair@32 (size, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, offset, n, t, t2)) =>
	    ea(n, postindex, offset, false),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    4,    true
	case LoadStore (LoadStorePair@64 (size, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, offset, n, t, t2)) =>
	    ea(n, postindex, offset, false),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    8,    true
	case _ => UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, false
    };
    -- TODO: add other memory operations here

    if memop == MemOp_STORE 
    then Mem(address, bytesize, acctype) <- data 
    else drop_result(Mem(address, bytesize, acctype));

    -- TODO: currently this overwrites the first pair request, extend Mem with queue
    when pair do {
	if memop2 == MemOp_STORE 
	then Mem(address2, bytesize2, acctype2) <- data2 
	else drop_result(Mem(address2, bytesize2, acctype2))
    }

	  }
    };

    C.S <- core_mem;
    tr = if C.CURR_REQ.EL == 0 then 1 else C.CURR_REQ.EL;
    
    return (C.CURR_REQ, mmu_regs(C.CURR_REQ.va, tr))
}

-- TODO: loop here for receiving multiple data parts before execution
unit CORE_WB (reply :: MEM_REQUEST) = 
{
    C.CURR_REPLY <- Some(reply);
    C.CURR_FAULT <- reply.desc.fault;

    if match_req_rpl(C.CURR_REQ, C.CURR_REPLY) then {
	    when (C.CURR_FAULT.typ == Fault_None) do run_curr();
	    C.S <- core_issue}
    else #MEM_INPUT_MISMATCH
}


(MMU_REQUEST option) * bool core_sched (imem :: MEM_REQUEST option) =
{
    var in_mem = imem;
    var out_mmu = None;
    var consumed = false;

    when !(imem == None and (C.S == core_fetch or C.S == core_mem)) do {
	match C.S {
	    case core_issue => {ommu = CORE_ISSUE();
		                when (ommu != None) do out_mmu <- ommu}
	    case core_fetch => {CORE_FETCH(ValOf(imem)); consumed <- true}
	    case core_dec   => if access_memory() then out_mmu <- Some(CORE_DECMEM())
		               else CORE_DECEXEC()
	    case core_mem   => {CORE_WB(ValOf(imem)); consumed <- true}
	}
    };

    return (out_mmu, consumed)
}


-- core interface uses oracle to determine memory input, returns consumed input and new output
-- this function is called from HOL as an instantiation of a local scheduling function in the generic compositional theory
(MEM_REQUEST option) * (MMU_REQUEST option) core_interface (o :: CORE_ORACLE) =
{
	oracle_in = o(C.CURR_REQ, C.S);

	var input = oracle_in;

	(out, consumed) = core_sched(input);

	when !consumed do input <- None;	

	return (input, out)
}


-- TODO: for non-atomic memory accesses issue several requests in sequential manner, move state transitions into transition functions, only go to next state if all requests sent/answered or fault
