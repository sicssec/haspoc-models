-----------------------------------------------------------------------
-- This is an extension of:					     --	
-- 								     --
-- Formal Specification of the ARMv8-A instruction set architecture  --
-- (c) Anthony Fox, University of Cambridge                          --
-- 								     --
-- with system level functionality.				     --
-- Author: Christoph Baumann, KTH CSC Stockholm			     --
-----------------------------------------------------------------------

{-

val () = Runtime.LoadF "v8-base.spec, v8-mem.spec, v8-mmu.spec, v8-virt.spec, v8-excp.spec, v8-iset.spec";

val () = HolExport.sigDocs false;
val () = HolExport.monadicExport true;
val () = HolExport.spec ("v8-base.spec, v8-iset.spec", "hol/arm8");
val () = SMLExport.spec ("v8-base.spec, v8-iset.spec", "sml/arm8");

-}


-----------------------------------------------------------------------
-- Instruction semantics ----------------------------------------------
-----------------------------------------------------------------------

construct LogicalOp {LogicalOp_AND LogicalOp_ORR LogicalOp_EOR}

construct MemOp {MemOp_LOAD MemOp_STORE MemOp_PREFETCH}

construct MoveWideOp {MoveWideOp_N MoveWideOp_Z MoveWideOp_K}

construct RevOp {RevOp_RBIT RevOp_REV16 RevOp_REV32 RevOp_REV64}

construct SystemHintOp
{
   SystemHintOp_NOP SystemHintOp_YIELD SystemHintOp_WFE
   SystemHintOp_WFI SystemHintOp_SEV SystemHintOp_SEVL
}

construct PSTATEField {PSTATEField_DAIFSet PSTATEField_DAIFClr PSTATEField_SP}

-- ---------------------------------------------
-- ADR{P} <Xd>, <label>
-- ---------------------------------------------

define Address (page::bool, imm::dword, d::reg) =
{
   var base = C.PC;
   when page do base<11:0> <- 0;
   X(d) <- base + imm
}

-- ---------------------------------------------
-- 32-bit
-- ADC{S} <Wd>, <Wn>, <Wm>
-- SBC{S} <Wd>, <Wn>, <Wm>
--
-- 64-bit
-- ADC{S} <Xd>, <Xn>, <Xm>
-- SBC{S} <Xd>, <Xn>, <Xm>
-- ---------------------------------------------

define Data > AddSubCarry
       (sf::bits(N), sub_op::bool, setflags::bool, m::reg, n::reg, d::reg)
       with N in 32, 64 =
{
   operand1 = X(n);
   operand2 = X(m);
   operand2 = if sub_op then ~operand2 else operand2;
   result`N, nzcv = AddWithCarry (operand1, operand2, C.PSTATE.C); --`
   SetTheFlags (setflags, nzcv);
   X(d) <- result
}

-- ---------------------------------------------
-- 32-bit
-- ADD{S} <Wd>, <Wn>, <Wm>{, <extend> #<amount>}
-- SUB{S} <Wd>, <Wn>, <Wm>{, <extend> #<amount>}
--
-- 64-bit
-- ADD{S} <Xd>, <Xn>, <Xm>{, <extend> #<amount>}
-- SUB{S} <Xd>, <Xn>, <Xm>{, <extend> #<amount>}
-- ---------------------------------------------

define Data > AddSubExtendRegister
       (sf::bits(N), sub_op::bool, setflags::bool, m::reg,
        extend_type::ExtendType, imm3::bits(3), n::reg, d::reg)
       with N in 32, 64 =
{
   operand1 = if n == 31 then SP else X(n) :: bits(N);
   operand2 = ExtendReg (m, extend_type, [imm3]);
   operand2, carry_in = if sub_op then (~operand2, true) else (operand2, false);
   result`N, nzcv = AddWithCarry (operand1, operand2, carry_in); --`
     SetTheFlags (setflags, nzcv);
     if d == 31 and not setflags then
        SP <- result
     else
        X(d) <- result
}

-- ---------------------------------------------
-- 32-bit
-- ADD{S} <Wd|WSP>, <Wn|WSP>, #<imm>{, <shift>}
-- SUB{S} <Wd|WSP>, <Wn|WSP>, #<imm>{, <shift>}
--
-- 64-bit
-- ADD{S} <Xd|SP>, <Xn|SP>, #<imm>{, <shift>}
-- SUB{S} <Xd|SP>, <Xn|SP>, #<imm>{, <shift>}
-- ---------------------------------------------

define Data > AddSubImmediate
       (sf::bits(N), sub_op::bool, setflags::bool,
        imm::bits(N), n::reg, d::reg) with N in 32, 64 =
{
   operand1 = if n == 31 then SP else X(n);
   operand2 = imm;
   operand2, carry_in = if sub_op then (~operand2, true) else (operand2, false);
   result`N, nzcv = AddWithCarry (operand1, operand2, carry_in); --`
   SetTheFlags (setflags, nzcv);
   if d == 31 and not setflags then
      SP <- result
   else
      X(d) <- result
}

-- ---------------------------------------------
-- 32-bit
-- ADD{S} <Wd>, <Wn>, <Wm>{, <shift> #<amount>}
-- SUB{S} <Wd>, <Wn>, <Wm>{, <shift> #<amount>}
--
-- 64-bit
-- ADD{S} <Xd>, <Xn>, <Xm>{, <shift> #<amount>}
-- SUB{S} <Xd>, <Xn>, <Xm>{, <shift> #<amount>}
-- ---------------------------------------------

define Data > AddSubShiftedRegister
       (sf::bits(N), sub_op::bool, setflags::bool, shift_type::ShiftType,
        m::reg, imm::bits(6), n::reg, d::reg) with N in 32, 64 =
{
   operand1 = X(n);
   operand2 = ShiftReg (m, shift_type, [imm]);
   operand2, carry_in = if sub_op then (~operand2, true) else (operand2, false);
   result`N, nzcv = AddWithCarry (operand1, operand2, carry_in); --`
   SetTheFlags (setflags, nzcv);
   X(d) <- result
}

-- ---------------------------------------------
-- 32-bit
-- AND{S} <Wd|WSP>, <Wn>, #<imm>
-- EOR    <Wd|WSP>, <Wn>, #<imm>
-- ORR    <Wd|WSP>, <Wn>, #<imm>
--
-- 64-bit
-- AND{S} <Xd|SP>, <Xn>, #<imm>
-- EOR    <Xd|SP>, <Xn>, #<imm>
-- ORR    <Xd|SP>, <Xn>, #<imm>
-- ---------------------------------------------

define Data > LogicalImmediate
       (sf::bits(N), opc::LogicalOp, setflags::bool,
        imm::bits(N), n::reg, d::reg) with N in 32, 64 =
{
   operand1 = X(n);
   operand2 = imm;
   result = match opc
            {
               case LogicalOp_AND => operand1 && operand2
               case LogicalOp_ORR => operand1 || operand2
               case LogicalOp_EOR => operand1 ?? operand2
            };
   SetTheFlags (setflags, Msb(result), result == 0, false, false);
   if d == 31 and not setflags then
      SP <- result
   else
      X(d) <- result
}

-- ---------------------------------------------
-- 32-bit
-- AND{S} <Wd>, <Wn>, <Wm>{, <shift> #<ammount>}
-- BIC{S} <Wd>, <Wn>, <Wm>{, <shift> #<ammount>}
-- EON    <Wd>, <Wn>, <Wm>{, <shift> #<ammount>}
-- EOR    <Wd>, <Wn>, <Wm>{, <shift> #<ammount>}
-- ORN    <Wd>, <Wn>, <Wm>{, <shift> #<ammount>}
-- ORR    <Wd>, <Wn>, <Wm>{, <shift> #<ammount>}
--
-- 64-bit
-- AND{S} <Xd>, <Xn>, <Xm>{, <shift> #<ammount>}
-- BIC{S} <Xd>, <Xn>, <Xm>{, <shift> #<ammount>}
-- EON    <Xd>, <Xn>, <Xm>{, <shift> #<ammount>}
-- EOR    <Xd>, <Xn>, <Xm>{, <shift> #<ammount>}
-- ORN    <Xd>, <Xn>, <Xm>{, <shift> #<ammount>}
-- ORR    <Xd>, <Xn>, <Xm>{, <shift> #<ammount>}
-- ---------------------------------------------

define Data > LogicalShiftedRegister
       (sf::bits(N), opc::LogicalOp, invert::bool, setflags::bool,
        shift_type::ShiftType, shift_amount::nat, m::reg, n::reg, d::reg)
       with N in 32, 64 =
{
   operand1 = X(n);
   operand2 = ShiftReg (m, shift_type, shift_amount) :: bits(N);
   operand2 = if invert then ~operand2 else operand2;
   result = match opc
            {
               case LogicalOp_AND => operand1 && operand2
               case LogicalOp_ORR => operand1 || operand2
               case LogicalOp_EOR => operand1 ?? operand2
            };
   SetTheFlags (setflags, Msb(result), result == 0, false, false);
   X(d) <- result
}

-- ---------------------------------------------
-- 32-bit
-- ASRV <Wd>, <Wn>, <Wm>
-- LSLV <Wd>, <Wn>, <Wm>
-- LSRV <Wd>, <Wn>, <Wm>
-- RORV <Wd>, <Wn>, <Wm>
--
-- 64-bit
-- ASRV <Xd>, <Xn>, <Xm>
-- LSLV <Xd>, <Xn>, <Xm>
-- LSRV <Xd>, <Xn>, <Xm>
-- RORV <Xd>, <Xn>, <Xm>
-- ---------------------------------------------

define Data > Shift (sf::bits(N), shift_type::ShiftType, m::reg, n::reg, d::reg)
       with N in 32, 64 =
{
   operand2 = X(m) :: bits(N);
   result = ShiftReg (n, shift_type, [operand2] mod N) :: bits(N);
   X(d) <- result
}

-- ---------------------------------------------
-- 32-bit
-- MOVK <Wd>, #<imm>{, LSL #<shift>}
-- MOVN <Wd>, #<imm>{, LSL #<shift>}
-- MOVZ <Wd>, #<imm>{, LSL #<shift>}
--
-- 64-bit
-- MOVK <Xd>, #<imm>{, LSL #<shift>}
-- MOVN <Xd>, #<imm>{, LSL #<shift>}
-- MOVZ <Xd>, #<imm>{, LSL #<shift>}
-- ---------------------------------------------

define Data > MoveWide
       (sf::bits(N), opcode::MoveWideOp, hw::bits(2), imm::half, d::reg)
       with N in 32, 64 =
{
   pos = [hw : '0000'];
   var result = if opcode == MoveWideOp_K then X(d) else 0`N; --`
   result<pos+15:pos> <- imm;
   result = if opcode == MoveWideOp_N then ~result else result;
   X(d) <- result
}

-- ---------------------------------------------
-- 32-bit
-- BFM  <Wd>, <Wn>, #<immr>, #<imms>
-- SBFM <Wd>, <Wn>, #<immr>, #<imms>
-- UBFM <Wd>, <Wn>, #<immr>, #<imms>
--
-- 64-bit
-- BFM  <Xd>, <Xn>, #<immr>, #<imms>
-- SBFM <Xd>, <Xn>, #<immr>, #<imms>
-- UBFM <Xd>, <Xn>, #<immr>, #<imms>
-- ---------------------------------------------

define Data > BitfieldMove
       (sf::bits(N), inzero::bool, extend::bool, wmask::bits(N), tmask::bits(N),
        R::nat, S::nat, n::reg, d::reg)
       with N in 32, 64 =
{
   dst = if inzero then 0 else X(d);
   src = X(n);

   -- perform bitfield move on low bits
   bot = dst && ~wmask || ROR (src, R) && wmask;

   -- determine extension bits (sign, zero or dest register)
   top = if extend then Replicate ([src<S>]) else dst;

   -- combine extension bits and result bits
   X(d) <- top && ~tmask || bot && tmask
}

-- ---------------------------------------------
-- 32-bit
-- CCMN <Wn>, #<imm>, #<nzcv>, <cond>
-- CCMP <Wn>, #<imm>, #<nzcv>, <cond>
--
-- 64-bit
-- CCMN <Xn>, #<imm>, #<nzcv>, <cond>
-- CCMP <Xn>, #<imm>, #<nzcv>, <cond>
-- ---------------------------------------------

define Data > ConditionalCompareImmediate
       (sf::bits(N), sub_op::bool, imm::bits(N), cond::cond,
        nzcv::bool * bool * bool * bool, n::reg) with N in 32, 64 =
{
   operand1 = X(n);
   operand2 = imm;
   if ConditionHolds(cond) then
   {
      operand2, carry_in =
         if sub_op then (~operand2, true) else (operand2, false);
      _, flags = AddWithCarry (operand1, operand2, carry_in);
      SetTheFlags (true, flags)
   }
   else
      SetTheFlags (true, nzcv)
}

-- ---------------------------------------------
-- 32-bit
-- CSEL  <Wd>, <Wn>, <Wm>, <cond>
-- CSINC <Wd>, <Wn>, <Wm>, <cond>
-- CSINV <Wd>, <Wn>, <Wm>, <cond>
-- CSNEG <Wd>, <Wn>, <Wm>, <cond>
--
-- 64-bit
-- CSEL  <Xd>, <Xn>, <Xm>, <cond>
-- CSINC <Xd>, <Xn>, <Xm>, <cond>
-- CSINV <Xd>, <Xn>, <Xm>, <cond>
-- CSNEG <Xd>, <Xn>, <Xm>, <cond>
-- ---------------------------------------------

define Data > ConditionalSelect
       (sf::bits(N), else_inv::bool, else_inc::bool, cond::cond,
        m::reg, n::reg, d::reg)
       with N in 32, 64 =
{
   operand1 = X(n);
   operand2 = X(m);
   var result :: bits(N);
   if ConditionHolds (cond) then
      result <- operand1
   else
   {
      result <- operand2;
      when else_inv do result <- ~result;
      when else_inc do result <- result + 1
   };
   X(d) <- result
}

-- ---------------------------------------------
-- 32-bit
-- CLS <Wd>, <Wn>
-- CLZ <Wd>, <Wn>
--
-- 64-bit
-- CLS <Xd>, <Xn>
-- CLZ <Xd>, <Xn>
-- ---------------------------------------------

define Data > CountLeading
       (sf::bits(N), count_clz::bool, n::reg, d::reg) with N in 32, 64 =
{
   operand1 = X(n) :: bits(N);
   result = if count_clz then
               CountLeadingZeroBits (operand1)
            else
               CountLeadingSignBits (operand1);
   X(d) <- [result] :: bits(N)
}

-- ---------------------------------------------
-- 32-bit
-- EXTR <Wd>, <Wn>, <Wm>, #<lsb>
--
-- 64-bit
-- EXTR <Xd>, <Xn>, <Xm>, #<lsb>
-- ---------------------------------------------

define Data > ExtractRegister
       (sf::bits(N), imms::bits(6), m::reg, n::reg, d::reg) with N in 32, 64 =
{
   lsb = [imms];
   operand1 = X(n) :: bits(N);
   operand2 = X(m) :: bits(N);
   concat = [operand1] : [operand2] :: bool list;
   result = [concat >>+ lsb] :: bits(N);
   X(d) <- result
}

-- ---------------------------------------------
-- 32-bit
-- SDIV <Wd>, <Wn>, <Wm>
-- UDIV <Wd>, <Wn>, <Wm>
--
-- 64-bit
-- SDIV <Xd>, <Xn>, <Xm>
-- UDIV <Xd>, <Xn>, <Xm>
-- ---------------------------------------------

define Data > Division
       (sf::bits(N), unsigned::bool, m::reg, n::reg, d::reg) with N in 32, 64 =
{
   operand1 = X(n) :: bits(N);
   operand2 = X(m);
   result = if operand2 == 0 then
               0
            else if unsigned then
               operand1 div operand2
            else
               operand1 quot operand2;
   X(d) <- result
}

-- ---------------------------------------------
-- 32-bit
-- MADD <Wd>, <Wn>, <Wm>, <Wa>
-- MSUB <Wd>, <Wn>, <Wm>, <Wa>
--
-- 64-bit
-- MADD <Xd>, <Xn>, <Xm>, <Xa>
-- MSUB <Xd>, <Xn>, <Xm>, <Xa>
-- ---------------------------------------------

define Data > MultiplyAddSub
       (sf::bits(N), sub_op::bool, m::reg, a::reg, n::reg, d::reg)
       with N in 32, 64 =
{
   operand1 = X(n) :: bits(N);
   operand2 = X(m);
   operand3 = X(a);
   result = if sub_op then
               operand3 - operand1 * operand2
            else
               operand3 + operand1 * operand2;
   X(d) <- result
}

-- ---------------------------------------------
-- SMADDL <Xd>, <Wn>, <Wm>, <Xa>
-- UMADDL <Xd>, <Wn>, <Wm>, <Xa>
-- SMSUBL <Xd>, <Wn>, <Wm>, <Xa>
-- UMSUBL <Xd>, <Wn>, <Wm>, <Xa>
-- ---------------------------------------------

define Data > MultiplyAddSubLong
       (sub_op::bool, signed::bool, m::reg, a::reg, n::reg, d::reg) =
{
   operand1 = X(n)`32; --`
   operand2 = X(m)`32; --`
   operand3 = X(a)`64; --`
   product = ExtendWord (operand1, signed) * ExtendWord (operand2, signed);
   result = if sub_op then operand3 - product else operand3 + product;
   X(d) <- result
}

-- ---------------------------------------------
-- SMULH <Xd>, <Xn>, <Xm>
-- UMULH <Xd>, <Xn>, <Xm>
-- ---------------------------------------------

define Data > MultiplyHigh (signed::bool, m::reg, n::reg, d::reg) =
{
   operand1 = X(n)`64; --`
   operand2 = X(m)`64; --`
   result`128 = ExtendWord (operand1, signed) * ExtendWord (operand2, signed); --`
   X(d) <- result<127:64> 
}

-- ---------------------------------------------
-- 32-bit
-- RBIT  <Wd>, <Wn>
-- REV16 <Wd>, <Wn>
-- REV32 <Wd>, <Wn>
-- REV   <Wd>, <Wn>
--
-- 64-bit
-- RBIT  <Xd>, <Xn>
-- REV16 <Xd>, <Xn>
-- REV32 <Xd>, <Xn>
-- REV   <Xd>, <Xn>
-- ---------------------------------------------

define Data > Reverse (sf::bits(N), op::RevOp, n::reg, d::reg)
       with N in 32, 64 =
{
    if N == 32 then
    {
       v = X(n)`32; --`
       result =
          match op
          {
             case RevOp_RBIT => Reverse (v)
             case RevOp_REV16 => v<23:16> : v<31:24> : v<7:0> : v<15:8>
             case RevOp_REV32 => v<7:0> : v<15:8> : v<23:16> : v<31:24>
             case RevOp_REV64 => UNKNOWN -- should be unreachable
          };
       X(d) <- result
    }
    else
    {
       v = X(n)`64; --`
       result =
          match op
          {
             case RevOp_RBIT => Reverse (v)
             case RevOp_REV16 =>
                 v<55:48> : v<63:56> : v<39:32> : v<47:40> :
                 v<23:16> : v<31:24> : v<7:0> : v<15:8>
             case RevOp_REV32 =>
                 v<39:32> : v<47:40> : v<55:48> : v<63:56> :
                 v<7:0> : v<15:8> : v<23:16> : v<31:24>
             case RevOp_REV64 =>
                 v<7:0> : v<15:8> : v<23:16> : v<31:24> :
                 v<39:32> : v<47:40> : v<55:48> : v<63:56>
          };
       X(d) <- result
    }
}

-- ---------------------------------------------
-- 32-bit
-- CRC32B <Wd>, <Wn>, <Wm>
-- CRC32H <Wd>, <Wn>, <Wm>
-- CRC32W <Wd>, <Wn>, <Wm>
--
-- 64-bit
-- CRC32X <Wd>, <Wn>, <Xm>
-- ---------------------------------------------

define CRCExt > CRC (sz::bits(N), crc32c::bool, m::reg, n::reg, d::reg)
       with N in 8, 16, 32, 64 =
{
   acc = X(n) :: bits(32);
   val = X(m) :: bits(N);
   poly = if crc32c then 0x1EDC6F41 else 0x04C11DB7;
   tempacc = Reverse ([acc]::bool list) : Zeros (N);
   tempval = Reverse ([val]::bool list) : Zeros (32);
   X(d) <- Reverse (Poly32Mod2 ([tempacc ?? tempval], poly))
}

-- ---------------------------------------------
-- B.<cond> <label>
-- ---------------------------------------------

define Branch > BranchConditional (offset::dword, cond::cond) =
   when ConditionHolds(cond) do BranchTo (C.PC + offset, BranchType_JMP)

-- ---------------------------------------------
-- B  <label>
-- BL <label>
-- ---------------------------------------------

define Branch > BranchImmediate (offset::dword, branch_type::BranchType) =
{
   when branch_type == BranchType_CALL do X(30) <- C.PC + 4;
   BranchTo (C.PC + offset, branch_type)
}

-- ---------------------------------------------
-- BR  <label>
-- BLR <label>
-- RET <label>
-- ---------------------------------------------

define Branch > BranchRegister (n::reg, branch_type::BranchType) =
{
   target = X(n);
   when branch_type == BranchType_CALL do X(30) <- C.PC + 4;
   BranchTo (target, branch_type)
}

-- ---------------------------------------------
-- 32-bit
-- CBZ  <Wt>, <label>
-- CBNZ <Wt>, <label>
--
-- 64-bit
-- CBZ  <Xt>, <label>
-- CBNZ <Xt>, <label>
-- ---------------------------------------------

define Branch > CompareAndBranch
       (sf::bits(N), iszero::bool, offset::dword, t::reg) with N in 32, 64 =
{
   operand1 = X(t) :: bits(N);
   when operand1 == 0 do BranchTo (C.PC + offset, BranchType_JMP)
}

-- ---------------------------------------------
-- 32-bit
-- TBZ  <Wt>, #<imm>, <label>
-- TBNZ <Wt>, #<imm>, <label>
--
-- 64-bit
-- TBZ  <Xt>, #<imm>, <label>
-- TBNZ <Xt>, #<imm>, <label>
-- ---------------------------------------------

define Branch > TestBitAndBranch
       (sf::bits(N), bit_pos::bits(6), bit_val::bool, offset::dword, t::reg)
       with N in 32, 64 =
{
   operand1 = X(t) :: bits(N);
   when operand1<[bit_pos]> == bit_val do BranchTo (C.PC + offset, BranchType_JMP)
}

-- ---------------------------------------------------------
-- Immediate:
--
-- LDR    <Wt|Xt>, [<Xn|SP>], #<simm>
-- LDRB   <Wt|Xt>, [<Xn|SP>], #<simm>
-- LDRH   <Wt|Xt>, [<Xn|SP>], #<simm>
-- LDRSB  <Wt|Xt>, [<Xn|SP>], #<simm>
-- LDRSH  <Wt|Xt>, [<Xn|SP>], #<simm>
-- LDRSW  <Wt|Xt>, [<Xn|SP>], #<simm>
--
-- LDR    <Wt|Xt>, [<Xn|SP>, #<imm>]
-- LDRB   <Wt|Xt>, [<Xn|SP>, #<imm>]
-- LDRH   <Wt|Xt>, [<Xn|SP>, #<imm>]
-- LDRSB  <Wt|Xt>, [<Xn|SP>, #<imm>]
-- LDRSH  <Wt|Xt>, [<Xn|SP>, #<imm>]
-- LDRSW  <Wt|Xt>, [<Xn|SP>, #<imm>]
--
-- LDUR   <Wt|Xt>, [<Xn|SP>, #<simm>]
-- LDURB  <Wt|Xt>, [<Xn|SP>, #<simm>]
-- LDURH  <Wt|Xt>, [<Xn|SP>, #<simm>]
-- LDURSB <Wt|Xt>, [<Xn|SP>, #<simm>]
-- LDURSH <Wt|Xt>, [<Xn|SP>, #<simm>]
-- LDURSW <Wt|Xt>, [<Xn|SP>, #<simm>]
--
-- LDTR   <Wt|Xt>, [<Xn|SP>, #<simm>]
-- LDTRB  <Wt|Xt>, [<Xn|SP>, #<simm>]
-- LDTRH  <Wt|Xt>, [<Xn|SP>, #<simm>]
-- LDTRSB <Wt|Xt>, [<Xn|SP>, #<simm>]
-- LDTRSH <Wt|Xt>, [<Xn|SP>, #<simm>]
-- LDTRSW <Wt|Xt>, [<Xn|SP>, #<simm>]
--
-- STR    <Wt|Xt>, [<Xn|SP>], #<simm>
-- STRB   <Wt|Xt>, [<Xn|SP>], #<simm>
-- STRH   <Wt|Xt>, [<Xn|SP>], #<simm>
--
-- STR    <Wt|Xt>, [<Xn|SP>, #<imm>]
-- STRB   <Wt|Xt>, [<Xn|SP>, #<imm>]
-- STRH   <Wt|Xt>, [<Xn|SP>, #<imm>]
--
-- STUR   <Wt|Xt>, [<Xn|SP>, #<simm>]
-- STURB  <Wt|Xt>, [<Xn|SP>, #<simm>]
-- STURH  <Wt|Xt>, [<Xn|SP>, #<simm>]
--
-- STTR   <Wt|Xt>, [<Xn|SP>, #<simm>]
-- STTRB  <Wt|Xt>, [<Xn|SP>, #<simm>]
-- STTRH  <Wt|Xt>, [<Xn|SP>, #<simm>]
--
-- PRFM   <prfop>, [<Xn|SP>, #<pimm>]
-- PRFUM  <prfop>, [<Xn|SP>, #<simm>]
--
-- Register:
--
-- LDR    <Wt|Xt>, [<Xn|SP>, <Xm|Wm>{, <extend> {<amoutn>}}]
-- LDRB   <Wt|Xt>, [<Xn|SP>, <Xm|Wm>{, <extend> {<amoutn>}}]
-- LDRH   <Wt|Xt>, [<Xn|SP>, <Xm|Wm>{, <extend> {<amoutn>}}]
-- LDRSB  <Wt|Xt>, [<Xn|SP>, <Xm|Wm>{, <extend> {<amoutn>}}]
-- LDRSH  <Wt|Xt>, [<Xn|SP>, <Xm|Wm>{, <extend> {<amoutn>}}]
-- LDRSW  <Wt|Xt>, [<Xn|SP>, <Xm|Wm>{, <extend> {<amoutn>}}]
--
-- STR    <Wt|Xt>, [<Xn|SP>, <Xm|Wm>{, <extend> {<amoutn>}}]
-- STRB   <Wt|Xt>, [<Xn|SP>, <Xm|Wm>{, <extend> {<amoutn>}}]
-- STRH   <Wt|Xt>, [<Xn|SP>, <Xm|Wm>{, <extend> {<amoutn>}}]
-- ---------------------------------------------------------

unit LoadStoreSingle
       (size::bits(N), regsize_word::bool, memop::MemOp, acctype::AccType,
        signed::bool, wb_unknown::bool, rt_unknown::bool,
        wback::bool, postindex::bool, offset::dword, n::reg, t::reg)
       with N in 8, 16, 32, 64 =
{
   when n == 31 and memop <> MemOp_PREFETCH do CheckSPAlignment;
   address = if n == 31 then
                SP
             else
                X(n);
   address = if postindex then address else address + offset;
   match memop
   {
      case MemOp_STORE =>
      {
         data = if rt_unknown then
                   UNKNOWN`N --`
                else
                   X(t);
         Mem (address, N div 8, acctype) <- [data]
      }
      case MemOp_LOAD =>
      {
         data`N = [Mem (address, N div 8, acctype)]; --`
         if regsize_word then
            X(t) <- ExtendWord (data, signed)`32 --`
         else
            X(t) <- ExtendWord (data, signed)`64 --`
      }
      case MemOp_PREFETCH => nothing -- stub 
   };
   when wback do
   {
      address = if wb_unknown then
                   UNKNOWN
                else if postindex then
                   address + offset
                else address;
      if n == 31 then
         SP <- address
      else
         X(n) <- address
   }
}

define LoadStore > LoadStoreImmediate
       (size::bits(N), regsize_word::bool, memop::MemOp, acctype::AccType,
        signed::bool, wb_unknown::bool, rt_unknown::bool,
        wback::bool, postindex::bool, offset::dword, n::reg, t::reg)
       with N in 8, 16, 32, 64 =
   LoadStoreSingle
     (size, regsize_word, memop, acctype, signed, wb_unknown, rt_unknown,
      wback, postindex, offset, n, t)

define LoadStore > LoadStoreRegister
       (size::bits(N), regsize_word::bool, memop::MemOp,
        signed::bool, m::reg, extend_type::ExtendType, shift::nat,
        n::reg, t::reg)
       with N in 8, 16, 32, 64 =
{
   offset = ExtendReg (m, extend_type, shift);
   LoadStoreSingle
     (size, regsize_word, memop, AccType_NORMAL, signed, false, false,
      false, false, offset, n, t)
}

-- ---------------------------------------------
-- 32-bit
-- LDNP <Wt1>, <Wt2>, [<Xn|SP>{, #<imm>}]
-- LDP  <Wt1>, <Wt2>, [<Xn|SP>{, #<imm>}]!
-- LDP  <Wt1>, <Wt2>, [<Xn|SP>{, #<imm>}]
-- LDP  <Wt1>, <Wt2>, [<Xn|SP>], #<imm>
--
-- STNP <Wt1>, <Wt2>, [<Xn|SP>{, #<imm>}]
-- STP  <Wt1>, <Wt2>, [<Xn|SP>], #<imm>
--
-- 64-bit
-- LDNP  <Xt1>, <Xt2>, [<Xn|SP>{, #<imm>}]
-- LDP   <Xt1>, <Xt2>, [<Xn|SP>{, #<imm>}]!
-- LDP   <Xt1>, <Xt2>, [<Xn|SP>{, #<imm>}]
-- LDP   <Xt1>, <Xt2>, [<Xn|SP>], #<imm>
-- LDPSW <Xt1>, <Xt2>, [<Xn|SP>{, #<imm>}]!
-- LDPSW <Xt1>, <Xt2>, [<Xn|SP>{, #<imm>}]
-- LDPSW <Xt1>, <Xt2>, [<Xn|SP>], #<imm>
--
-- STNP  <Xt1>, <Xt2>, [<Xn|SP>{, #<imm>}]
-- STP   <Xt1>, <Xt2>, [<Xn|SP>], #<imm>
-- ---------------------------------------------

define LoadStore > LoadStorePair
       (size::bits(N), memop::MemOp, acctype::AccType, signed::bool,
        wb_unknown::bool, rt_unknown::bool, wback::bool,
        postindex::bool, offset::dword, n::reg, t::reg, t2::reg)
       with N in 32, 64 =
{
   dbytes = N div 8;
   when n == 31 do CheckSPAlignment;
   address = if n == 31 then
                SP
             else
                X(n);
   address = if postindex then address else address + offset;
   match memop
   {
      case MemOp_STORE =>
      {
         data1 = if rt_unknown and t == n then
                    UNKNOWN`N --`
                 else
                    X(t);
         data2 = if rt_unknown and t2 == n then
                    UNKNOWN`N --`
                 else
                    X(t2);
         Mem (address, dbytes, acctype) <- [data1];
         Mem (address + [dbytes], dbytes, acctype) <- [data2]
      }
      case MemOp_LOAD =>
      {
         data1`N, data2`N =
            if rt_unknown then
               UNKNOWN
            else
               [Mem (address, dbytes, acctype)],
               [Mem (address + [dbytes], dbytes, acctype)];
         if signed {- and N == 32 -} then
         {
            X(t)  <- ExtendWord (data1, signed)`64; --`
            X(t2) <- ExtendWord (data2, signed)`64 --`
         }
         else
         {
            X(t)  <- data1;
            X(t2) <- data2
         }
      }
      case _ => nothing -- should be unreachable
   };
   when wback do
   {
      address = if wb_unknown then
                   UNKNOWN
                else if postindex then
                   address + offset
                else address;
      if n == 31 then
         SP <- address
      else
         X(n) <- address
   }
}

-- ---------------------------------------------
-- LDAR   <Wt>, [<Xn|SP>{, #0}]
-- LDARB  <Wt>, [<Xn|SP>{, #0}]
-- LDARH  <Wt>, [<Xn|SP>{, #0}]
-- LDAXR  <Wt>, [<Xn|SP>{, #0}]
-- LDAXRB <Wt>, [<Xn|SP>{, #0}]
-- LDAXRH <Wt>, [<Xn|SP>{, #0}]
-- LDXR   <Wt>, [<Xn|SP>{, #0}]
-- LDXRB  <Wt>, [<Xn|SP>{, #0}]
-- LDXRH  <Wt>, [<Xn|SP>{, #0}]
--
-- LDAR   <Xt>, [<Xn|SP>{, #0}]
-- LDAXR  <Xt>, [<Xn|SP>{, #0}]
-- LDXR   <Xt>, [<Xn|SP>{, #0}]
--
-- STLR   <Wt>, [<Xn|SP>{, #0}]
-- STLRB  <Wt>, [<Xn|SP>{, #0}]
-- STLRH  <Wt>, [<Xn|SP>{, #0}]
-- STLXR  <Wt>, [<Xn|SP>{, #0}]
-- STLXRB <Wt>, [<Xn|SP>{, #0}]
-- STLXRH <Wt>, [<Xn|SP>{, #0}]
-- STXR   <Wt>, [<Xn|SP>{, #0}]
-- STXRB  <Wt>, [<Xn|SP>{, #0}]
-- STXRH  <Wt>, [<Xn|SP>{, #0}]
--
-- STLR   <Xt>, [<Xn|SP>{, #0}]
-- STLXR  <Xt>, [<Xn|SP>{, #0}]
-- STXR   <Xt>, [<Xn|SP>{, #0}]
-- STXRB  <Xt>, [<Xn|SP>{, #0}]
-- STXRH  <Xt>, [<Xn|SP>{, #0}]
-- ---------------------------------------------

-- Exclusive monitor stubs
bool ExclusiveMonitorPass (address::dword, n::nat) = UNKNOWN
unit SetExclusiveMonitors (address::dword, n::nat) = nothing
bits(1) ExclusiveMonitorStatus = UNKNOWN

define LoadStore > LoadStoreAcquire -- does not cover pair
       (size::bits(N), memop::MemOp, acctype::AccType, excl::bool,
       rn_unknown::bool, rt_unknown::bool, s::reg, n::reg, t::reg)
       with N in 8, 16, 32, 64 =
{
   dbytes = N div 8;
   when n == 31 do CheckSPAlignment;
   address = if n == 31 then
                SP
             else if rn_unknown then
                UNKNOWN
             else
                X(n);
   match memop
   {
      case MemOp_STORE =>
      {
         data = if rt_unknown then
                   UNKNOWN
                else
                   X(t)`N; --`
         if excl then
            if ExclusiveMonitorPass (address, dbytes) then
            {
               Mem (address, dbytes, acctype) <- [data];
               status = ExclusiveMonitorStatus;
               X(s) <- [status]`32 --`
            }
            else
               X(s) <- 1`32 --`
         else
            Mem (address, dbytes, acctype) <- [data]
      }
      case MemOp_LOAD =>
      {
         when excl do SetExclusiveMonitors (address, dbytes);
         data`N = [Mem (address, dbytes, acctype)]; --`
         if N == 64 then
            X(t) <- data
         else
            X(t) <- [data]`32 --`
      }
      case _ => nothing -- should be unreachable
   }
}

-- ---------------------------------------------
-- LDXP  <Wt1>, <Wt2>, [<Xn|SP>{, #0}]
-- LDAXP <Wt1>, <Wt2>, [<Xn|SP>{, #0}]
-- STLXP <Wt1>, <Wt2>, [<Xn|SP>{, #0}]
-- STXP  <Wt1>, <Wt2>, [<Xn|SP>{, #0}]
--
-- LDXP  <Xt1>, <Xt2>, [<Xn|SP>{, #0}]
-- LDAXP <Xt1>, <Xt2>, [<Xn|SP>{, #0}]
-- STLXP <Xt1>, <Xt2>, [<Xn|SP>{, #0}]
-- STXP  <Xt1>, <Xt2>, [<Xn|SP>{, #0}]
-- ---------------------------------------------

define LoadStore > LoadStoreAcquirePair
       (size::bits(N), memop::MemOp, acctype::AccType,
        rn_unknown::bool, rt_unknown::bool, s::reg, n::reg, t::reg, t2::reg)
       with N in 64, 128 =
{
   dbytes = N div 8;
   when n == 31 do CheckSPAlignment;
   address = if n == 31 then
                SP
             else if rn_unknown then
                UNKNOWN
             else
                X(n);
   match memop
   {
      case MemOp_STORE =>
      {
         data`N = --`
            if rt_unknown then
               UNKNOWN
            else if N == 64 then
               if BigEndian then
                  [X(t)`32 : X(t2)`32]
               else
                  [X(t2)`32 : X(t)`32]
            else -- N == 128
               if BigEndian then
                  [X(t)`64 : X(t2)`64]
               else
                  [X(t2)`64 : X(t)`64];
         if ExclusiveMonitorPass (address, dbytes) then
         {
            Mem (address, dbytes, acctype) <- [data];
            status = ExclusiveMonitorStatus;
            X(s) <- [status]`32 --`
         }
         else
            X(s) <- 1`32 --`
      }
      case MemOp_LOAD =>
      {
         SetExclusiveMonitors (address, dbytes);
         if rt_unknown then -- t == t2
            if N == 64 then
               X(t) <- UNKNOWN`32 --`
            else
               X(t) <- UNKNOWN`64 --`
         else if N == 64 then
         {
            data`N = [Mem (address, dbytes, acctype)]; --`
            if BigEndian then
            {
               X(t)  <- data<63:32>;
               X(t2) <- data<31:0>
            }
            else
            {
               X(t)  <- data<31:0>;
               X(t2) <- data<63:32>
            }
         }
         else -- N == 128
         {
            when not Aligned (address, dbytes) do #ALIGNMENT_FAULT;
            X(t)  <- [Mem (address, 8, acctype)]`64; --`
            X(t2) <- [Mem (address + 8, 8, acctype)]`64 --`
         }
      }
      case _ => nothing -- should be unreachable
   }
}

-- ---------------------------------------------
-- PRFM <prfop>, <label>
--
-- 32-bit
-- LDR   <Wt>, <label>
-- LDRSW <Wt>, <label>
--
-- 64-bit
-- LDR   <Xt>, <label>
-- LDRSW <Xt>, <label>
-- ---------------------------------------------

define LoadStore > LoadLiteral
       (size::bits(N), memop::MemOp, signed::bool, offset::dword, t::reg)
       with N in 32, 64 =
{
   address = C.PC + offset;
   match memop
   {
      case MemOp_LOAD =>
      {
         data`N = [Mem (address, N div 8, AccType_NORMAL)]; --`
         X(t) <- ExtendWord (data, signed)`64 --`
      }
      case _ => nothing -- should be MemOp_PREFETCH -- stub
   }
}

-----------------------------------------------------------------------
-- Stubs --------------------------------------------------------------
-----------------------------------------------------------------------

-- DMB, DSB, ISB
-- different implementation in sequential and detailed model
define MemoryBarrier (op::MemBarrierOp, domain::MBReqDomain, types::MBReqTypes) = MemBarrier(op, domain, types)
	
-- CLREX
define ClearExclusive (imm::bits(4)) = #NOT_MODELED ("ClearExclusive")

-- HINT
define Hint (op::SystemHintOp) = match op {
    case SystemHintOp_NOP   => nothing
    case SystemHintOp_YIELD => nothing -- YIELD may infect internal multithreading, not modeled here
    case SystemHintOp_WFE   => #NOT_MODELED ("WaitForEvent")
    case SystemHintOp_WFI   => #NOT_MODELED ("WaitForInterrupt")
    case SystemHintOp_SEV   => #NOT_MODELED ("SendEvent")
    case SystemHintOp_SEVL  => #NOT_MODELED ("SendEventLevel")
}
	 
-- BRK
define Debug > Breakpoint (imm::half) = #NOT_MODELED ("Breakpoint")

-- SCPS1, SCPS2, SCPS3
define Debug > DebugSwitch (target_level::bits(2)) =
   #NOT_MODELED ("DebugSwitch")

-- DRPS
define Debug > DebugRestore = #NOT_MODELED ("DebugRestore")

-- HLT
define Debug > Halt (imm16::half) = #NOT_MODELED ("Halt")



-- SYS
define System > SystemInstruction
       (sys_op1::bits(3), sys_op2::bits(3), sys_crn::bits(4), sys_crm::bits(4),
        has_result::bool, t::reg) = handle_sysop(sys_op1, sys_op2, sys_crn, sys_crm, has_result, t)	   
       
	   
-- MRS and MSR (register)
define System > MoveSystemRegister
       (read::bool, sys_op0::bits(2), sys_op1::bits(3), sys_op2::bits(3),
        sys_crn::bits(4), sys_crm::bits(4), t::reg) =
{
    when sys_op0 == 2 do #NOT_MODELED ("MoveDebugTraceLegacy");

    var d64 :: bits(64);
    EL0 = (C.PSTATE.EL < 1);
    EL10 = (C.PSTATE.EL < 2);
    EL210 = (C.PSTATE.EL < 3);
    EL1NS = (C.PSTATE.EL == 1 and C.SCR_EL3.NS);
    EL10NS = (EL10 and C.SCR_EL3.NS);
    EL210NS = (EL210 and C.SCR_EL3.NS);
    
    match sys_op1, sys_crn, sys_crm, sys_op2 {
	case '000', '0000', '0000', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- [C.&MIDR_EL1] else #UNDEFINED_FAULT ("ReadOnlyMIDR") }
	case '000', '0000', '0000', '101' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- [C.&MPIDR_EL1]; d64<39:31> <- '000000001'; d64<29:25> <- '00000'}
	     else #UNDEFINED_FAULT ("ReadOnlyMPIDR") }
	case '000', '0000', '0000', '110' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then #NOT_MODELED ("REVIDR") else #UNDEFINED_FAULT ("ReadOnlyREVIDR") }
	case '000', '0000', '0001', _     => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then #NOT_MODELED ("AArch32") else #UNDEFINED_FAULT ("ReadOnlyID_AArch32") }
	case '000', '0000', '0010', _     => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then #NOT_MODELED ("AArch32") else #UNDEFINED_FAULT ("ReadOnlyID_AArch32") }
	case '000', '0000', '0011', _     => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then #NOT_MODELED ("AArch32") else #UNDEFINED_FAULT ("ReadOnlyMVFRAArch32") }
	case '000', '0000', '0100', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- [C.&ID_AA64PFR0_EL1]; d64<63:28> <- 0} else #UNDEFINED_FAULT ("ReadOnlyID_AArch64") }
	case '000', '0000', '0100', _     => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- 0 else #UNDEFINED_FAULT ("ReadOnlyID_AArch64") } 
	case '000', '0000', '0101', _     => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- 0 else #UNDEFINED_FAULT ("ReadOnlyID_AArch64") } -- DFR and AFR not modeled
	case '000', '0000', '0110', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- [C.&ID_AA64ISAR0_EL1]; d64<63:20> <- 0; d64<3:0> <- 0}
	     else #UNDEFINED_FAULT ("ReadOnlyID_AArch64") }
	case '000', '0000', '0110', _     => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- 0 else #UNDEFINED_FAULT ("ReadOnlyID_AArch64") } 
	case '000', '0000', '0111', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- [C.&ID_AA64MMFR0_EL1]; d64<63:32> <- 0} else #UNDEFINED_FAULT ("ReadOnlyID_AArch64") } 
	case '000', '0000', '0111', _     => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- 0 else #UNDEFINED_FAULT ("ReadOnlyID_AArch64") } 
	case '000', '0001', '0000', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {if EL1NS and C.HCR_EL2.TRVM then #TRAP_TO_EL2
			   else {d64 <- [C.&SCTLR_EL1]; d64<31:30> <- '00'; d64<29:28> <- '11'; d64<27> <- true; d64<23:22> <- '11';
				 d64<21> <- false; d64<20> <- true; d64<17> <- false; d64<13> <- false; d64<11> <- true;
				 d64<10> <- false; d64<6> <- false} }
	     else {if EL1NS and C.HCR_EL2.TVM then #TRAP_TO_EL2 else C.&SCTLR_EL1 <- X(t)`32} } 
	case '000', '0001', '0000', '001' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if EL1NS and C.HCR_EL2.TACR then #TRAP_TO_EL2 else #NOT_MODELED ("ACTLR") } 
	case '000', '0001', '0000', '010' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("CPACR")
	case '000', '0010', '0000', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {if EL1NS and C.HCR_EL2.TRVM then #TRAP_TO_EL2 else d64 <- C.&TTBR0_EL1}
	     else {if EL1NS and C.HCR_EL2.TVM then #TRAP_TO_EL2 else C.&TTBR0_EL1 <- X(t)`64} }
	case '000', '0010', '0000', '001' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {if EL1NS and C.HCR_EL2.TRVM then #TRAP_TO_EL2 else d64 <- C.&TTBR1_EL1}
	     else {if EL1NS and C.HCR_EL2.TVM then #TRAP_TO_EL2 else C.&TTBR1_EL1 <- X(t)`64} }
	case '000', '0010', '0000', '010' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {if EL1NS and C.HCR_EL2.TRVM then #TRAP_TO_EL2
			   else {d64 <- C.&TCR_EL1; d64<63:39> <- 0; d64<35> <- false; d64<6> <- false} }
	     else {if EL1NS and C.HCR_EL2.TVM then #TRAP_TO_EL2 else C.&TTBR0_EL1 <- X(t)`64} }
	case '000', '0101', '0001', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {if EL1NS and C.HCR_EL2.TRVM then #TRAP_TO_EL2 else #NOT_MODELED ("AFSR0")}
	     else {if EL1NS and C.HCR_EL2.TVM then #TRAP_TO_EL2 else #NOT_MODELED ("AFSR0")} }
	case '000', '0101', '0001', '001' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {if EL1NS and C.HCR_EL2.TRVM then #TRAP_TO_EL2 else #NOT_MODELED ("AFSR1")}
	     else {if EL1NS and C.HCR_EL2.TVM then #TRAP_TO_EL2 else #NOT_MODELED ("AFSR1")} }
	case '000', '0110', '0000', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {if EL1NS and C.HCR_EL2.TRVM then #TRAP_TO_EL2 else #NOT_MODELED ("FAR")}
	     else {if EL1NS and C.HCR_EL2.TVM then #TRAP_TO_EL2 else #NOT_MODELED ("FAR")} }
	case '000', '0111', '0100', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- C.&PAR_EL1; if C.PAR_EL1.F then {d64<63:12> <- 0; d64<11:10> <- '10'; d64<7> <- false}
					      else {d64<55:48> <- 0; d64<11> <- true; d64<6:1> <- 0} }
	     else C.&PAR_EL1 <- X(t)`64 } --`
	case '000', '1010', '0010', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {if EL1NS and C.HCR_EL2.TRVM then #TRAP_TO_EL2 else d64 <- C.MAIR_EL1}
	     else {if EL1NS and C.HCR_EL2.TVM then #TRAP_TO_EL2 else C.MAIR_EL1 <- X(t)`64} }
	case '000', '1010', '0011', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {if EL1NS and C.HCR_EL2.TRVM then #TRAP_TO_EL2 else #NOT_MODELED ("AMAIR")}
	     else {if EL1NS and C.HCR_EL2.TVM then #TRAP_TO_EL2 else #NOT_MODELED ("AMAIR")} }
	case '000', '1100', '0000', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- C.&VBAR_EL1; d64<10:0> <- 0} else C.&VBAR_EL1 <- X(t)`64 } --`
	case '000', '1100', '0000', '001' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then #NOT_MODELED ("RVBAR") else #UNDEFINED_FAULT ("ReadOnlyRVBAR") } 
	case '000', '1100', '0000', '010' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("RMR")
	case '000', '1100', '0001', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then #NOT_MODELED ("ISR") else #UNDEFINED_FAULT ("ReadOnlyISR") } 
	case '000', '1101', '0000', '001' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("CONTEXTIDR")
	case '000', '1101', '0000', '100' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- C.TPIDR_EL1 else C.TPIDR_EL1 <- X(t)`64 } --`
	case '001', '0000', '0000', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {if EL1NS and C.HCR_EL2.TID2 then #TRAP_TO_EL2 else #NOT_MODELED ("CCSIDR")}
			   else #UNDEFINED_FAULT("ReadOnlyCCSIDR") }
	case '001', '0000', '0000', '001' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {if EL1NS and C.HCR_EL2.TID2 then #TRAP_TO_EL2 else #NOT_MODELED ("CLIDR")}
			   else #UNDEFINED_FAULT("ReadOnlyCLIDR") }
	case '001', '0000', '0000', '111' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {if EL1NS and C.HCR_EL2.TID2 then #TRAP_TO_EL2 else #NOT_MODELED ("AIDR")}
			   else #UNDEFINED_FAULT("ReadOnlyAIDR") }
	case '010', '0000', '0000', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if EL1NS and C.HCR_EL2.TID2 then #TRAP_TO_EL2 else #NOT_MODELED ("CSSELR") }
	case '011', '0000', '0000', '001' => if EL0 and !C.SCTLR_EL1.UCT then #TRAP_TO_EL1 else {
	     if EL10NS and C.HCR_EL2.TID2 then #TRAP_TO_EL2 else #NOT_MODELED ("CTR") }
	case '011', '0000', '0000', '111' => if read then #NOT_MODELED ("DCZID") else #UNDEFINED_FAULT("ReadOnlyDCZID")
	case '011', '1101', '0000', '010' => if read then d64 <- C.TPIDR_EL0 else C.TPIDR_EL0 <- X(t)`64 --`
        case '011', '1101', '0000', '011' => if read then d64 <- C.TPIDRRO_EL0 else {
	     if EL0 then #UNDEFINED_FAULT ("ReadOnlyTPIDRRO") else C.TPIDRRO_EL0 <- X(t)`64 } --`
	case '100', '0000', '0000', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- [C.&VPIDR_EL2] else C.&VPIDR_EL2 <- X(t)`32 } --`
	case '100', '0000', '0000', '101' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- C.&VMPIDR_EL2; d64<63:40> <- 0; d64<31> <- true; d64<29:25> <- 0} else C.&VMPIDR_EL2 <- X(t)`64 } --`
	case '100', '0001', '0000', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- [C.&SCTLR_EL2]; d64<31:30> <- 0; d64<29:28> <- '11'; d64<27:26> <- 0; d64<24> <- false;
			   d64<23:22> <- '11'; d64<21:20> <- 0; d64<18> <- true; d64<17> <- false; d64<16> <- true; d64<15:13> <- 0;
			   d64<11> <- true; d64<10:6> <- 0; d64<5:4> <- '11'}
	     else C.&SCTLR_EL2 <- X(t)`32 } --`
	case '100', '0001', '0000', '001' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("ACTLR")
	case '100', '0001', '0001', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- C.&HCR_EL2; d64<63:39> <- 0; d64<37:34> <- 0} else C.&HCR_EL2 <- X(t)`64 } --`
	case '100', '0001', '0001', '001' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("MDCR")
	case '100', '0001', '0001', '010' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("CPTR")
	case '100', '0001', '0001', '011' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- [C.HSTR_EL2]; d64<31:16> <- 0} else C.HSTR_EL2 <- X(t)`32 } --`
	case '100', '0001', '0001', '111' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("HACR")
	case '100', '0010', '0000', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- C.&TTBR0_EL2; d64<63:48> <- 0} else C.&TTBR0_EL2 <- X(t)`64 } --`
	case '100', '0010', '0000', '010' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- [C.&TCR_EL2]; d64<31> <- true; d64<30:24> <- 0; d64<23> <- true; d64<22:21> <- 0;
			   d64<19> <- false; d64<7:6> <- 0}
	     else C.&TCR_EL2 <- X(t)`32 } --`
	case '100', '0010', '0001', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- C.&VTTBR_EL2; d64<63:56> <- 0} else C.&VTTBR_EL2 <- X(t)`64 } --`
	case '100', '0010', '0001', '010' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- [C.&VTCR_EL2]; d64<31> <- true; d64<30:19> <- 0} else C.&VTCR_EL2 <- X(t)`32 } --`
	case '100', '0101', '0001', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("AFSR0")
	case '100', '0101', '0001', '001' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("AFSR1")
	case '100', '0101', '0010', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("ESR")
	case '100', '0110', '0000', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("FAR")
	case '100', '0110', '0000', '100' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("HPFAR")
	case '100', '1010', '0010', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- C.MAIR_EL2 else C.MAIR_EL2 <- X(t)`64 } --`
	case '100', '1010', '0011', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("AMAIR")
	case '100', '1100', '0000', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("VBAR")
	case '100', '1100', '0000', '001' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then #NOT_MODELED ("RVBAR") else #UNDEFINED_FAULT ("ReadOnlyRVBAR") } 
	case '100', '1100', '0000', '010' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("RMR")
	case '100', '1101', '0000', '010' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- C.TPIDR_EL2 else C.TPIDR_EL2 <- X(t)`64 } --`
	case '110', '0001', '0000', '000' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- [C.&SCTLR_EL3]; d64<31:30> <- 0; d64<29:28> <- '11'; d64<27:26> <- 0; d64<24> <- false;
			   d64<23:22> <- '11'; d64<21:20> <- 0; d64<18> <- true; d64<17> <- false; d64<16> <- true; d64<15:13> <- 0;
			   d64<11> <- true; d64<10:6> <- 0; d64<5:4> <- '11'}
	     else C.&SCTLR_EL3 <- X(t)`32 } --`
	case '110', '0001', '0000', '001' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("ACTLR")
	case '110', '0001', '0001', '000' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- [C.&SCR_EL3]; d64<31:14> <- 0; d64<6> <- false; d64<5:4> <- '11'}
	     else C.&SCR_EL3 <- X(t)`32 } --`
	case '110', '0001', '0001', '010' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("CPTR")
	case '110', '0001', '0011', '001' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("MDCR")
	case '110', '0010', '0000', '000' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- C.&TTBR0_EL3; d64<63:48> <- 0} else C.&TTBR0_EL3 <- X(t)`64 } --`
	case '110', '0010', '0000', '010' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then {d64 <- [C.&TCR_EL3]; d64<31> <- true; d64<30:24> <- 0; d64<23> <- true; d64<22:21> <- 0;
			   d64<19> <- false; d64<7:6> <- 0}
	     else C.&TCR_EL3 <- X(t)`32 } --`
	case '110', '0101', '0001', '000' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("AFSR0")
	case '110', '0101', '0001', '001' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("AFSR1")
	case '110', '0101', '0010', '000' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("ESR")
	case '110', '0110', '0000', '000' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("FAR")
	case '110', '1010', '0010', '000' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- C.MAIR_EL3 else C.MAIR_EL3 <- X(t)`64 } --`
	case '110', '1010', '0011', '000' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("AMAIR")
	case '110', '1100', '0000', '000' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("VBAR")
	case '110', '1100', '0000', '001' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then #NOT_MODELED ("RVBAR") else #UNDEFINED_FAULT ("ReadOnlyRVBAR") } 
	case '110', '1100', '0000', '010' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("RMR")
	case '110', '1101', '0000', '010' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- C.TPIDR_EL3 else C.TPIDR_EL3 <- X(t)`64 } --`
-- AArch32 registers:		
	case '100', '0011', '0000', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("AArch32")
	case '100', '0101', '0000', '001' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("AArch32")
	case '100', '0101', '0011', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("AArch32")
	case '110', '0001', '0001', '001' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("AArch32")
-- Counter/Timer control:
        case '000', '1110', '0001', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges")
                                             else #NOT_MODELED ("CounterTimer")
    	case '011', '1110', '0000', '000' => #NOT_MODELED ("CounterTimer")
    	case '011', '1110', '0000', '001' => #NOT_MODELED ("CounterTimer")
    	case '011', '1110', '0000', '010' => #NOT_MODELED ("CounterTimer")
    	case '011', '1110', '0010', '000' => #NOT_MODELED ("CounterTimer")
    	case '011', '1110', '0010', '001' => #NOT_MODELED ("CounterTimer")
    	case '011', '1110', '0010', '010' => #NOT_MODELED ("CounterTimer")
    	case '011', '1110', '0011', '000' => #NOT_MODELED ("CounterTimer")
    	case '011', '1110', '0011', '001' => #NOT_MODELED ("CounterTimer")
    	case '011', '1110', '0011', '010' => #NOT_MODELED ("CounterTimer")
    	case '100', '1110', '0000', '011' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges")
		                             else #NOT_MODELED ("CounterTimer")
    	case '100', '1110', '0001', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") 
	                                     else #NOT_MODELED ("CounterTimer")
    	case '100', '1110', '0010', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") 
	                                     else #NOT_MODELED ("CounterTimer")
        case '100', '1110', '0010', '001' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") 
	                                     else #NOT_MODELED ("CounterTimer")
    	case '100', '1110', '0010', '010' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") 
	                                     else #NOT_MODELED ("CounterTimer")
    	case '111', '1110', '0010', '000' => if EL210NS then #UNDEFINED_FAULT ("InsufficientPrivileges") 
	                                     else #NOT_MODELED ("CounterTimer")
    	case '111', '1110', '0010', '001' => if EL210NS then #UNDEFINED_FAULT ("InsufficientPrivileges") 
                                             else #NOT_MODELED ("CounterTimer")
    	case '111', '1110', '0010', '010' => if EL210NS then #UNDEFINED_FAULT ("InsufficientPrivileges")
	                                     else #NOT_MODELED ("CounterTimer")
-- Performance Monitors:
    	case '000', '1001', '1110', '001' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("PerfMon")
    	case '000', '1001', '1110', '010' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("PerfMon")
    	case '011', '1001', '1100', _     => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("PerfMon")
    	case '011', '1001', '1101', '000' => #NOT_MODELED ("PerfMon")
    	case '011', '1001', '1101', '001' => #NOT_MODELED ("PerfMon")
    	case '011', '1001', '1101', '010' => #NOT_MODELED ("PerfMon")
    	case '011', '1001', '1110', '000' => if EL0 and !read then #UNDEFINED_FAULT ("ReadOnlyPMUSERENR") 
	                                     else #NOT_MODELED ("PerfMon")
    	case '011', '1001', '1110', '011' => #NOT_MODELED ("PerfMon")
    	case '011', '1110', '1000', _     => #NOT_MODELED ("PerfMon")
    	case '011', '1110', '1001', _     => #NOT_MODELED ("PerfMon")
    	case '011', '1110', '1010', _     => #NOT_MODELED ("PerfMon")
    	case '011', '1110', '1011', x => if x == '111' then #UNDEFINED_FAULT ("Unallocated") else #NOT_MODELED ("PerfMon")
    	case '011', '1110', '1100', _     => #NOT_MODELED ("PerfMon")
    	case '011', '1110', '1101', _     => #NOT_MODELED ("PerfMon")
    	case '011', '1110', '1110', _     => #NOT_MODELED ("PerfMon")
    	case '011', '1110', '1111', _     => #NOT_MODELED ("PerfMon")
-- Special Purpose Registers:	     
    	case '000', '0100', '0000', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("SPSR")
    	case '000', '0100', '0000', '001' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("ELR")
    	case '000', '0100', '0001', '000' => if EL0 or !C.PSTATE.SPS then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- C.SP_EL0 else C.SP_EL0 <- X(t)`64 } --`
    	case '000', '0100', '0010', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- [[C.PSTATE.SPS]`1] else C.PSTATE.SPS <- (X(t)`32)<0> } --
    	case '000', '0100', '0010', '010' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- [C.PSTATE.EL:'00'] else #UNDEFINED_FAULT ("ReadOnlyCurrentEL") } --
    	case '011', '0100', '0010', '001' => if EL0 and !C.SCTLR_EL1.UMA then #TRAP_TO_EL1 else {
	     if read then d64 <- [[C.PSTATE.D]`1:[C.PSTATE.A]`1:[C.PSTATE.I]`1:[C.PSTATE.F]`1:'000000']
		     else {C.PSTATE.D <- (X(t)`32)<9>; C.PSTATE.A <- (X(t)`32)<8>; C.PSTATE.I <- (X(t)`32)<7>; 
                           C.PSTATE.F <- (X(t)`32)<6>} } --
    	case '011', '0100', '0010', '000' => if read then d64 <- [[C.PSTATE.N]`1:[C.PSTATE.Z]`1:[C.PSTATE.C]`1:[C.PSTATE.V]`1:(0`28)]
		     else {C.PSTATE.D <- (X(t)`32)<9>; C.PSTATE.A <- (X(t)`32)<8>; C.PSTATE.I <- (X(t)`32)<7>;
			   C.PSTATE.F <- (X(t)`32)<6>} --`
    	case '011', '0100', '0100', '000' => #NOT_MODELED ("FloatingPoint")
    	case '011', '0100', '0100', '001' => #NOT_MODELED ("FloatingPoint")
    	case '011', '0100', '0101', '000' => #NOT_MODELED ("Debug")
    	case '011', '0100', '0101', '001' => #NOT_MODELED ("Debug")
    	case '100', '0100', '0000', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("SPSR")
    	case '100', '0100', '0000', '001' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("ELR")
    	case '100', '0100', '0001', '000' => if EL10 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- C.SP_EL1 else C.SP_EL1 <- X(t)`64 } --`
    	case '100', '0100', '0011', '000' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("SPSR_irq")
    	case '100', '0100', '0011', '001' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("SPSR_abt")
    	case '100', '0100', '0011', '010' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("SPSR_und")
    	case '100', '0100', '0011', '011' => if EL0 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("SPSR_fiq")
    	case '110', '0100', '0000', '000' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("SPSR")
    	case '110', '0100', '0000', '001' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else #NOT_MODELED ("ELR")
    	case '110', '0100', '0001', '000' => if EL210 then #UNDEFINED_FAULT ("InsufficientPrivileges") else {
	     if read then d64 <- C.SP_EL2 else C.SP_EL2 <- X(t)`64 } --`
-- else
        case _    , '1011', _     , _     => #NOT_MODELED ("ImplementationDefinedReg")
        case _    , '1111', _     , _     => #NOT_MODELED ("ImplementationDefinedReg")
	case _    , _     , _     , _     => #UNDEFINED_FAULT ("Unallocated") 
    };

    when read do X(t) <- d64
}	   
       
-- MSR (immediate)
define System > MoveImmediateProcState (field::PSTATEField, operand::bits(4)) =
{
    when field != PSTATEField_SP and C.PSTATE.EL == 0 and !C.SCTLR_EL1.UMA do #TRAP_TO_EL1;
    
    match field {
	case PSTATEField_DAIFSet => {
		C.PSTATE.D <- C.PSTATE.D or operand<3>;
		C.PSTATE.A <- C.PSTATE.A or operand<2>;
		C.PSTATE.I <- C.PSTATE.I or operand<1>;
		C.PSTATE.F <- C.PSTATE.F or operand<0>
		}
	case PSTATEField_DAIFClr => {
		C.PSTATE.D <- C.PSTATE.D and !operand<3>;
		C.PSTATE.A <- C.PSTATE.A and !operand<2>;
		C.PSTATE.I <- C.PSTATE.I and !operand<1>;
		C.PSTATE.F <- C.PSTATE.F and !operand<0>
		}
	case PSTATEField_SP => {
		C.PSTATE.SPS <- operand<0>;
		when C.CISW.IMPL_SBZ_NOT_ZERO_UNPREDICTABLE and operand<3:1> != '000' do #UNPREDICTABLE
		}
    }	  
}
    
-- level 1, SVC
define System > SupervisorCall (imm16::half) =
   #NOT_MODELED ("SupervisorCall")

-- level 2, HVC
define System > HypervisorCall (imm16::half) =
   #NOT_MODELED ("HypervisorCall")

-- level 3, SMC
define System > SecureMonitorCall (imm16::half) =
   #NOT_MODELED ("SecureMonitorCall")

-- ERET
define System > ExceptionReturn = #NOT_MODELED ("ExceptionReturn")

define Unallocated = #UNDEFINED_FAULT ("Unallocated")

define Reserved = #UNDEFINED_FAULT ("Reserved")

define Run


-----------------------------------------------------------------------
-- Helper functions for decoding --------------------------------------
-----------------------------------------------------------------------

LogicalOp * bool DecodeLogicalOp (opc::bits(2)) =
   match opc
   {
      case 0b00 => LogicalOp_AND, false
      case 0b01 => LogicalOp_ORR, false
      case 0b10 => LogicalOp_EOR, false
      case 0b11 => LogicalOp_AND, true
   }

instruction NoOperation = Hint (SystemHintOp_NOP)

-- . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

instruction LoadStoreRegister
   (size::bits(2), regsize_word::bool, memop::MemOp,
    signed::bool, m::reg, extend_type::ExtendType, shift::nat, n::reg, t::reg) =
{
   a = regsize_word, memop, signed, m, extend_type, shift, n, t;
   match size
   {
      case 0b00 => LoadStore (LoadStoreRegister@8 (0, a))
      case 0b01 => LoadStore (LoadStoreRegister@16 (1, a))
      case 0b10 => LoadStore (LoadStoreRegister@32 (2, a))
      case 0b11 => LoadStore (LoadStoreRegister@64 (3, a))
   }
}

instruction LoadStoreImmediateN
   (size::bits(2), regsize_word::bool, memop::MemOp, acctype::AccType,
    signed::bool, wb_unknown::bool, rt_unknown::bool,
    wback::bool, postindex::bool, offset::dword, n::reg, t::reg) =
{
   a = regsize_word, memop, acctype, signed, wb_unknown, rt_unknown,
       wback, postindex, offset, n, t;
   match size
   {
      case 0b00 => LoadStore (LoadStoreImmediate@8 (0, a))
      case 0b01 => LoadStore (LoadStoreImmediate@16 (1, a))
      case 0b10 => LoadStore (LoadStoreImmediate@32 (2, a))
      case 0b11 => LoadStore (LoadStoreImmediate@64 (3, a))
   }
}

instruction LoadStoreImmediate
   (size::bits(2), opc::bits(2), acctype::AccType,
    wback::bool, postindex::bool, offset::dword, Rn::reg, Rt::reg) =
{
   memop, regsize_word, signed =
      if not opc<1> then -- store or zero-extending load
         (if opc<0> then MemOp_LOAD else MemOp_STORE), size <> '11', false
      else if size == '11' then
         MemOp_PREFETCH, UNKNOWN, UNKNOWN
      else -- sign-extending load
         MemOp_LOAD, opc<0>, true;
   wb_unknown = false;
   rt_unknown = false;
   if memop == MemOp_LOAD and wback and Rn == Rt and Rn <> 31 then
      -- this is constrained unpredictable, so take your pick
      -- SML will invoke case 0
      match UNKNOWN ("LoadImmediate unpredictable") :: bits(2)
      {
         case 0 =>
         {
            wback = false;
            LoadStoreImmediateN
              (size, regsize_word, memop, acctype, signed, wb_unknown,
               rt_unknown, wback, postindex, offset, Rn, Rt)
         }
         case 1 =>
         {
            wb_unknown = true;
            LoadStoreImmediateN
              (size, regsize_word, memop, acctype, signed, wb_unknown,
               rt_unknown, wback, postindex, offset, Rn, Rt)
         }
         case 2 => Unallocated
         case 3 => NoOperation
      }
   else if memop == MemOp_STORE and wback and Rn == Rt and Rn <> 31 then
      -- this is constrained unpredictable, so take your pick
      -- SML will invoke case 0
      match UNKNOWN ("StoreImmediate unpredictable") :: bits(2)
      {
         case 0 =>
            LoadStoreImmediateN
              (size, regsize_word, memop, acctype, signed, wb_unknown,
               rt_unknown, wback, postindex, offset, Rn, Rt)
         case 1 =>
         {
            rt_unknown = true;
            LoadStoreImmediateN
              (size, regsize_word, memop, acctype, signed, wb_unknown,
               rt_unknown, wback, postindex, offset, Rn, Rt)
         }
         case 2 => Unallocated
         case 3 => NoOperation
      }
   else
      LoadStoreImmediateN
        (size, regsize_word, memop, acctype, signed, wb_unknown,
         rt_unknown, wback, postindex, offset, Rn, Rt)
}

-- . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

instruction LoadStorePairN
   (sf::bits(1), memop::MemOp, acctype::AccType, signed::bool,
    wb_unknown::bool, rt_unknown::bool, wback::bool, postindex::bool,
    offset::dword, n::reg, t::reg, t2::reg) =
{
   a = memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex,
       offset, n, t, t2;
   if sf == '1' then
      LoadStore (LoadStorePair@64 (1, a))
   else
      LoadStore (LoadStorePair@32 (0, a))
}

instruction LoadStorePair
   (sf::bits(1), memop::MemOp, acctype::AccType, signed::bool, wback::bool,
    postindex::bool, offset::dword, Rn::reg, Rt::reg, Rt2::reg) =
{
   wb_unknown = false;
   rt_unknown = false;
   if memop == MemOp_LOAD and wback and (Rn == Rt or Rn == Rt2) and Rn <> 31
      then
      -- this is constrained unpredictable, so take your pick
      -- SML will invoke case 0
      match UNKNOWN ("LoadPair unpredictable") :: bits(2)
      {
         case 0 =>
         {
            wback = false;
            LoadStorePairN
              (sf, memop, acctype, signed, wb_unknown, rt_unknown, wback,
               postindex, offset, Rn, Rt, Rt2)
         }
         case 1 =>
         {
            wb_unknown = true;
            LoadStorePairN
              (sf, memop, acctype, signed, wb_unknown, rt_unknown, wback,
               postindex, offset, Rn, Rt, Rt2)
         }
         case 2 => Unallocated
         case 3 => NoOperation
      }
   else if memop == MemOp_STORE and wback and (Rn == Rt or Rn == Rt2) and
           Rn <> 31
      then
      -- this is constrained unpredictable, so take your pick
      -- SML will invoke case 0
      match UNKNOWN ("StorePair unpredictable") :: bits(2)
      {
         case 0 =>
            LoadStorePairN
              (sf, memop, acctype, signed, wb_unknown, rt_unknown, wback,
               postindex, offset, Rn, Rt, Rt2)
         case 1 =>
         {
            rt_unknown = true;
            LoadStorePairN
              (sf, memop, acctype, signed, wb_unknown, rt_unknown, wback,
               postindex, offset, Rn, Rt, Rt2)
         }
         case 2 => Unallocated
         case 3 => NoOperation
      }
   else if memop == MemOp_LOAD and Rt == Rt2 then
      -- this is constrained unpredictable, so take your pick
      -- SML will invoke case 0
      match UNKNOWN ("LoadPair Rt = Rt2 unpredictable") :: bits(2)
      {
         case 0 =>
         {
            rt_unknown = true;
            LoadStorePairN
              (sf, memop, acctype, signed, wb_unknown, rt_unknown, wback,
               postindex, offset, Rn, Rt, Rt2)
         }
         case 1 => Unallocated
         case _ => NoOperation
      }
   else
      LoadStorePairN
        (sf, memop, acctype, signed, wb_unknown, rt_unknown, wback,
         postindex, offset, Rn, Rt, Rt2)
}

-- . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

instruction LoadStoreAcquireN
   (size::bits(2), memop::MemOp, acctype::AccType, excl::bool, pair::bool,
    rn_unknown::bool, rt_unknown::bool, s::reg, n::reg, t::reg, t2::reg) =
{
   if pair then
   {
      a = memop, acctype, rn_unknown, rt_unknown, s, n, t, t2;
      if size == 2 then
         LoadStore (LoadStoreAcquirePair@64 (2, a))
      else
         LoadStore (LoadStoreAcquirePair@128 (3, a))
   }
   else
   {
      a = memop, acctype, excl, rn_unknown, rt_unknown, s, n, t;
      match size
      {
         case 0b00 => LoadStore (LoadStoreAcquire@8 (0, a))
         case 0b01 => LoadStore (LoadStoreAcquire@16 (1, a))
         case 0b10 => LoadStore (LoadStoreAcquire@32 (2, a))
         case 0b11 => LoadStore (LoadStoreAcquire@64 (3, a))
      }
   }
}

instruction LoadStoreAcquire
   (size::bits(2), memop::MemOp, acctype::AccType, excl::bool, pair::bool,
    Rs::reg, Rn::reg, Rt::reg, Rt2::reg) =
{
   rt_unknown, rn_unknown, ast =
      if memop == MemOp_LOAD and pair and Rt == Rt2 then
         -- this is constrained unpredictable, so take your pick
         -- SML will invoke case 0
         match UNKNOWN ("LoadAcquire unpredictable") :: bits(2)
         {
            case 0 => true, false, None
            case 1 => false, false, Some (Unallocated)
            case _ => false, false, Some (NoOperation)
         }
      else (false, false, None);
   rt_unknown, rn_unknown, ast =
      if memop == MemOp_STORE and excl and Rs == Rt or (pair and Rs == Rt2) then
         -- this is constrained unpredictable, so take your pick
         -- SML will invoke case 0
         match UNKNOWN ("StoreAcquire Rs = Rt unpredictable") :: bits(2)
         {
            case 0 => true, rn_unknown, ast
            case 1 => rt_unknown, rn_unknown, ast
            case 2 => rt_unknown, rn_unknown, Some (Unallocated)
            case 3 => rt_unknown, rn_unknown, Some (NoOperation)
         }
      else
         (rt_unknown, rn_unknown, ast);
   rt_unknown, rn_unknown, ast =
      if memop == MemOp_STORE and excl and Rs == Rn and Rn <> 31 then
         -- this is constrained unpredictable, so take your pick
         -- SML will invoke case 0
         match UNKNOWN ("StoreAcquire Rs = Rn unpredictable") :: bits(2)
         {
            case 0 => rt_unknown, true, ast
            case 1 => rt_unknown, rn_unknown, ast
            case 2 => rt_unknown, rn_unknown, Some (Unallocated)
            case 3 => rt_unknown, rn_unknown, Some (NoOperation)
         }
      else
         (rt_unknown, rn_unknown, ast);
   match ast
   {
      case Some (i) => i
      case None =>
         LoadStoreAcquireN
           (size, memop, acctype, excl, pair, rn_unknown, rt_unknown,
            Rs, Rn, Rt, Rt2)
   }
}

-----------------------------------------------------------------------
-- Instruction decoding -----------------------------------------------
-----------------------------------------------------------------------

pattern
{
   op S sf         ` 1
   imm3            ` 3
   CRm             ` 4
   Rs Rm Rn
   Rd Rt Rt2       ` 5
   imm5            ` 5
   imm6 immr imms  ` 6
   imm7            ` 7
   imm9            ` 9
   imm12           ` 12
   imm14           ` 14
   imm16           ` 16
   imm19           ` 19
}

instruction Decode (w::word) =
   match w
   {
      ------------------------
      -- Address
      ------------------------
      case 'op immlo`2 10000 immhi Rd' =>
      {
         page = [op];
         imm = if page then SignExtend (immhi : immlo : 0`12) --`
                       else SignExtend (immhi : immlo);
         Address (page, imm, Rd)
      }

      ------------------------
      -- AddSubShiftedRegister
      ------------------------
      case '_ op S 01011 11    0 Rm imm6 Rn Rd' => Reserved
      case '0 op S 01011 shift 0 Rm imm6 Rn Rd' =>
         if imm6<5> then Reserved
         else Data (AddSubShiftedRegister@32
                      (0, [op], [S], DecodeShift (shift), Rm, imm6, Rn, Rd))
      case '1 op S 01011 shift 0 Rm imm6 Rn Rd' =>
         Data (AddSubShiftedRegister@64
                 (1, [op], [S], DecodeShift (shift), Rm, imm6, Rn, Rd))

      ------------------------
      -- AddSubExtendRegister
      ------------------------
      case '0 op S 01011 00 1 Rm option imm3 Rn Rd' =>
         Data (AddSubExtendRegister@32
                 (0, [op], [S], Rm, DecodeRegExtend (option), imm3, Rn, Rd))
      case '1 op S 01011 00 1 Rm option imm3 Rn Rd' =>
         Data (AddSubExtendRegister@64
                 (1, [op], [S], Rm, DecodeRegExtend (option), imm3, Rn, Rd))

      ------------------------
      -- AddSubImmediate
      ------------------------
      case '_ op S 10001 1 _`1 imm12 Rn Rd' => Reserved
      case '0 op S 10001 shift imm12 Rn Rd' =>
      {
         imm = if shift == '00' then [imm12] else [imm12] << 12;
         Data (AddSubImmediate@32 (0, [op], [S], imm, Rn, Rd))
      }
      case '1 op S 10001 shift imm12 Rn Rd' =>
      {
         imm = if shift == '00' then [imm12] else [imm12] << 12;
         Data (AddSubImmediate@64 (1, [op], [S], imm, Rn, Rd))
      }

      ------------------------
      -- AddSubCarry
      ------------------------
      case '0 op S 11010000 Rm 000000 Rn Rd' =>
         Data (AddSubCarry@32 (0, [op], [S], Rm, Rn, Rd))
      case '1 op S 11010000 Rm 000000 Rn Rd' =>
         Data (AddSubCarry@64 (1, [op], [S], Rm, Rn, Rd))

      ------------------------
      -- LogicalShiftedRegister
      ------------------------
      case '0 opc`2 01010 shift N Rm imm6 Rn Rd' =>
         if imm6<5> then Reserved
         else
         {
            invert = N == '1';
            opc, setflags = DecodeLogicalOp (opc);
            Data (LogicalShiftedRegister@32
                      (0, opc, invert, setflags, DecodeShift (shift), [imm6],
                       Rm, Rn, Rd))
         }
      case '1 opc`2 01010 shift N Rm imm6 Rn Rd' =>
      {
         invert = N == '1';
         opc, setflags = DecodeLogicalOp (opc);
         Data (LogicalShiftedRegister@64
                   (1, opc, invert, setflags, DecodeShift (shift), [imm6],
                    Rm, Rn, Rd))
      }

      ------------------------
      -- LogicalImmediate
      ------------------------
      case '0 opc`2 100100 1 immr`6 imms`6 Rn Rd' => Reserved
      case '0 opc`2 100100 N immr`6 imms`6 Rn Rd' => -- N must be 0
         match DecodeBitMasks (N, imms, immr, true)
         {
            case Some (imm, _) =>
            {
               opc, setflags = DecodeLogicalOp (opc);
               Data (LogicalImmediate@32 (0, opc, setflags, imm, Rn, Rd))
            }
            case None => Reserved
         }
      case '1 opc`2 100100 N immr`6 imms`6 Rn Rd' =>
         match DecodeBitMasks (N, imms, immr, true)
         {
            case Some (imm, _) =>
            {
               opc, setflags = DecodeLogicalOp (opc);
               Data (LogicalImmediate@64 (1, opc, setflags, imm, Rn, Rd))
            }
            case None => Reserved
         }

      ------------------------
      -- Shift
      ------------------------
      case '0 00 11010110 Rm 0010 op2 Rn Rd' =>
         Data (Shift@32 (0, DecodeShift (op2), Rm, Rn, Rd))
      case '1 00 11010110 Rm 0010 op2 Rn Rd' =>
         Data (Shift@64 (1, DecodeShift (op2), Rm, Rn, Rd))

      ------------------------
      -- MoveWide
      ------------------------
      case '0 opc`2 100101 1 _ imm16 Rd' => Unallocated
      case 'sf 01 100101 hw imm16 Rd' => Unallocated
      case 'sf opc 100101 hw imm16 Rd' =>
      {
         opcode = match opc
                  {
                     case 0b00 => MoveWideOp_N
                     case 0b10 => MoveWideOp_Z
                     case 0b11 => MoveWideOp_K
                     case 0b01 => UNKNOWN -- unreachable
                  };
         if sf == '1' then
            Data (MoveWide@64 (1, opcode, hw, imm16, Rd))
         else
            Data (MoveWide@32 (0, opcode, hw, imm16, Rd))
      }

      ------------------------
      -- BitfieldMove
      ------------------------
      case '_`1 11  100110 N immr imms Rn Rd' => Unallocated
      case '0 opc 100110 N immr imms Rn Rd' =>
         if N == '1' or immr<5> or imms<5> then
            Reserved
         else
         {
            inzero, extend =
               match opc
               {
                  case 0b00 => true, true   -- SBFM
                  case 0b01 => false, false -- BFM
                  case 0b10 => true, false  -- UBFM
                  case _ => UNKNOWN
               };
            match DecodeBitMasks (N, imms, immr, false)
            {
               case Some (wmask, tmask) =>
                  Data
                   (BitfieldMove@32
                      (0, inzero, extend, wmask, tmask, [immr], [imms], Rn, Rd))
               case None => Reserved
            }
         }
      case '1 opc 100110 N immr imms Rn Rd' =>
         if N == '0' then
            Reserved
         else
         {
            inzero, extend =
               match opc
               {
                  case 0b00 => true, true   -- SBFM
                  case 0b01 => false, false -- BFM
                  case 0b10 => true, false  -- UBFM
                  case _ => UNKNOWN
               };
            match DecodeBitMasks (N, imms, immr, false)
            {
               case Some (wmask, tmask) =>
                  Data
                   (BitfieldMove@64
                      (1, inzero, extend, wmask, tmask, [immr], [imms], Rn, Rd))
               case None => Reserved
            }
         }

      ------------------------
      -- ConditionalCompareImmediate
      ------------------------
      case '0 op 111010010 imm5 cond 10 Rn 0 n`1 z`1 c`1 v`1' =>
      {
         nzcv = [n], [z], [c], [v];
         Data (ConditionalCompareImmediate@32 (0, [op], [imm5], cond, nzcv, Rn))
      }
      case '1 op 111010010 imm5 cond 10 Rn 0 n`1 z`1 c`1 v`1' =>
      {
         nzcv = [n], [z], [c], [v];
         Data (ConditionalCompareImmediate@64 (1, [op], [imm5], cond, nzcv, Rn))
      }

      ------------------------
      -- ConditionalSelect
      ------------------------
      case '0 op 011010100 Rm cond 0 o2 Rn Rd' =>
         Data (ConditionalSelect@32 (0, [op], [o2], cond, Rm, Rn, Rd))
      case '1 op 011010100 Rm cond 0 o2 Rn Rd' =>
         Data (ConditionalSelect@64 (1, [op], [o2], cond, Rm, Rn, Rd))

      ------------------------
      -- CountLeading
      ------------------------
      case '0 10110101100000000010 op Rn Rd' =>
         Data (CountLeading@32 (0, [op], Rn, Rd))
      case '1 10110101100000000010 op Rn Rd' =>
         Data (CountLeading@64 (1, [op], Rn, Rd))

      ------------------------
      -- ExtractRegister
      ------------------------
      case '0 00100111 N 0 Rm imms Rn Rd' =>
         if N <> 0 then
            Unallocated
         else if imms<5> then
            Reserved
         else
            Data (ExtractRegister@32 (0, imms, Rm, Rn, Rd))
      case '1 00100111 N 0 Rm imms Rn Rd' =>
         if N <> 1 then
            Unallocated
         else
            Data (ExtractRegister@64 (1, imms, Rm, Rn, Rd))

      ------------------------
      -- Division
      ------------------------
      case '0 0011010110 Rm 00001 o1 Rn Rd' =>
         Data (Division@32 (0, o1 == '0', Rm, Rn, Rd))
      case '1 0011010110 Rm 00001 o1 Rn Rd' =>
         Data (Division@64 (1, o1 == '0', Rm, Rn, Rd))

      ------------------------
      -- MultiplyAddSub
      ------------------------
      case '0 0011011000 Rm o0 Ra Rn Rd' =>
         Data (MultiplyAddSub@32 (0, o0 == '1', Rm, Ra, Rn, Rd))
      case '1 0011011000 Rm o0 Ra Rn Rd' =>
         Data (MultiplyAddSub@64 (1, o0 == '1', Rm, Ra, Rn, Rd))

      ------------------------
      -- MultiplyAddSubLong
      ------------------------
      case '1 00 11011 U 01 Rm o0 Ra Rn Rd' =>
         Data (MultiplyAddSubLong ([o0], U == '0', Rm, Ra, Rn, Rd))

      ------------------------
      -- MultiplyHigh
      ------------------------
      case '1 00 11011 U 10 Rm 0 (11111) Rn Rd' =>
         Data (MultiplyHigh (U == '0', Rm, Rn, Rd))

      ------------------------
      -- Reverse
      ------------------------
      case '0 1011010110000000000 11 Rn Rd' => Unallocated
      case '0 1011010110000000000 opc Rn Rd' =>
         Data (Reverse@32 (0, [opc], Rn, Rd))
      case '1 1011010110000000000 opc Rn Rd' =>
         Data (Reverse@64 (1, [opc], Rn, Rd))

      ------------------------
      -- CRC
      ------------------------
      case '0 0011010110 Rm 010 C 00 Rn Rd' =>
         CRCExt (CRC@8 (0, [C], Rm, Rn, Rd))
      case '0 0011010110 Rm 010 C 01 Rn Rd' =>
         CRCExt (CRC@16 (1, [C], Rm, Rn, Rd))
      case '0 0011010110 Rm 010 C 10 Rn Rd' =>
         CRCExt (CRC@32 (2, [C], Rm, Rn, Rd))
      case '1 0011010110 Rm 010 C 11 Rn Rd' =>
         CRCExt (CRC@64 (3, [C], Rm, Rn, Rd))

      ------------------------
      -- BranchConditional
      ------------------------
      case '01010100 imm19 0 cond' =>
         Branch (BranchConditional (SignExtend (imm19 : '00'), cond))

      ------------------------
      -- BranchImmediate
      ------------------------
      case 'op 00101 imm26' =>
      {
         branch_type = if op == '1' then BranchType_CALL else BranchType_JMP;
         Branch (BranchImmediate (SignExtend (imm26 : '00'), branch_type))
      }

      ------------------------
      -- BranchRegister
      ------------------------
      case '110101100 00 11111000000 Rn 00000' =>
         Branch (BranchRegister (Rn, BranchType_JMP))
      case '110101100 01 11111000000 Rn 00000' =>
         Branch (BranchRegister (Rn, BranchType_CALL))
      case '110101100 10 11111000000 Rn 00000' =>
         Branch (BranchRegister (Rn, BranchType_RET))
      case '110101100 _  11111000000 Rn 00000' => Unallocated

      ------------------------
      -- CompareAndBranch
      ------------------------
      case 'sf 011010 op imm19 Rt' =>
      {
         iszero = op == '0';
         offset = SignExtend (imm19 : '00');
         if sf == '1' then
            Branch (CompareAndBranch@64 (1, iszero, offset, Rt))
         else
            Branch (CompareAndBranch@32 (0, iszero, offset, Rt))
      }

      ------------------------
      -- TestBitAndBranch
      ------------------------
      case 'sf 011011 op b40 imm14 Rt' =>
      {
         bit_pos = sf : b40;
         bit_val = [op];
         offset = SignExtend (imm14 : '00');
         if sf == '1' then
            Branch (TestBitAndBranch@64 (1, bit_pos, bit_val, offset, Rt))
         else
            Branch (TestBitAndBranch@32 (0, bit_pos, bit_val, offset, Rt))
      }

      ------------------------
      -- LoadStoreImmediate
      ------------------------
      case '10 111000 11 0 imm9 _`2 Rn Rt' => Unallocated
      case '11 111000 11 0 imm9 _`2 Rn Rt' => Unallocated
      case '11 111000 1 _ 0 imm9 1 0 Rn Rt' => Unallocated
      -- Pre-index and post-index
      -- LDR, LDRB, LDRH, LDRSB, LDRSH, LDRSW
      -- STR, STRB, STRH
      case 'size 111000 opc 0 imm9 P 1 Rn Rt' =>
      {
         wback = true;
         postindex = P == '0';
         offset = SignExtend (imm9);
         acctype = AccType_NORMAL;
         LoadStoreImmediate
           (size, opc, acctype, wback, postindex, offset, Rn, Rt)
      }
      -- Unscaled offset and unprivileged
      -- LDUR, LDURB, LDURH, LDURSB, LDURSH, LDURSW
      -- STUR, STURB, STURH
      -- LDTR, LDTRB, LDTRH, LDTRSB, LDTRSH, LDTRSW
      -- STTR, STTRB, STTRH
      -- PRFUM
      case 'size 111000 opc 0 imm9 U 0 Rn Rt' =>
      {
         wback = false;
         postindex = false;
         offset = SignExtend (imm9);
         acctype = if U == '1' then AccType_UNPRIV else AccType_NORMAL;
         LoadStoreImmediate
           (size, opc, acctype, wback, postindex, offset, Rn, Rt)
      }
      -- Unsigned offset
      -- LDR, LDRB, LDRH, LDRSB, LDRSH, LDRSW
      -- STR, STRB, STRH
      -- PRFM
      case 'size 111001 opc imm12 Rn Rt' =>
      {
         wback = false;
         postindex = false;
         offset = LSL (SignExtend (imm12), [size]);
         acctype = AccType_NORMAL;
         LoadStoreImmediate
           (size, opc, acctype, wback, postindex, offset, Rn, Rt)
      }

      ------------------------
      -- LoadStoreRegister
      ------------------------
      case 'size 111000 opc 1 Rm option`3 S 10 Rn Rt' =>
         if not option<1> then
            Unallocated
         else
         {
            scale = [size];
            extend_type = DecodeRegExtend (option);
            shift = if S == '1' then scale else 0;
            memop, regsize_word, signed =
               if not opc<1> then -- store or zero-extending load
                  (if opc<0> then MemOp_LOAD else MemOp_STORE), size <> '11',
                  false
               else if size == '11' then
                  MemOp_PREFETCH, UNKNOWN, UNKNOWN
               else -- sign-extending load
                  MemOp_LOAD, opc<0>, true;
            LoadStoreRegister
              (size, regsize_word, memop, signed, Rm, extend_type, shift,
               Rn, Rt)
         }

      ------------------------
      -- LoadLiteral
      ------------------------
      case 'opc 011000 imm19 Rt' =>
      {
         offset = SignExtend (imm19 : '00');
         match opc
         {
            case 0b00 =>
               LoadStore (LoadLiteral@32 (0, MemOp_LOAD, false, offset, Rt))
            case 0b01 =>
               LoadStore (LoadLiteral@64 (1, MemOp_LOAD, false, offset, Rt))
            case 0b10 =>
               LoadStore (LoadLiteral@32 (2, MemOp_LOAD, true, offset, Rt))
            case 0b11 =>
               LoadStore (LoadLiteral@32 (3, MemOp_PREFETCH, false, offset, Rt))
         }
      }

      ------------------------
      -- LoadStorePair
      ------------------------
      case '_`1 1 10100 _`2 0 imm7 Rt2 Rn Rt' => Unallocated
      case '11 10100 _ imm7 Rt2 Rn Rt' => Unallocated
      case 'sf S 10100 P W L imm7 Rt2 Rn Rt' =>
      {
         wback = W == '1';
         signed = S == '1';
         postindex = P == '0';
         nontemporal = not (wback or signed) and postindex;
         postindex = not nontemporal and postindex;
         acctype = if nontemporal then AccType_STREAM else AccType_NORMAL;
         scale = 0n2 + [sf];
         offset = LSL (SignExtend (imm7), scale);
         memop = if L == '1' then MemOp_LOAD else MemOp_STORE;
         LoadStorePair
            (sf, memop, acctype, signed, wback, postindex, offset, Rn, Rt, Rt2)
      }

      ------------------------
      -- LoadStoreAcquire
      ------------------------
      case 'size`2 001000 1 L 0 Rs 0 Rt2 Rn Rt' => Unallocated
      case 'size`2 001000 1 L 1 Rs _`1 Rt2 Rn Rt' => Unallocated
      case '0 _`1 001000 o2`1 L 1 Rs o0`1 Rt2 Rn Rt' => Unallocated
      case 'size 001000 o2 L o1 Rs o0 Rt2 Rn Rt' =>
      {
         memop = if L == '1' then MemOp_LOAD else MemOp_STORE;
         acctype = if o0 == '1' then AccType_ORDERED else AccType_ATOMIC;
         excl = o2 == '0';
         pair = o1 == '1';
         LoadStoreAcquire (size, memop, acctype, excl, pair, Rs, Rn, Rt, Rt2)
      }

      ------------------------
      -- MoveSystemRegister
      ------------------------
      case '1101010100 L`1 1 o0`1 op1 CRn CRm op2 Rt' =>
         System (MoveSystemRegister ([L], 2 + [o0], op1, op2, CRn, CRm, Rt))

      ------------------------
      -- MoveImmediateProcState
      ------------------------
      case '1101010100000 000 0100 CRm 101 11111' =>
         System (MoveImmediateProcState (PSTATEField_SP, CRm))
      case '1101010100000 011 0100 CRm 110 11111' =>
         System (MoveImmediateProcState (PSTATEField_DAIFSet, CRm))
      case '1101010100000 011 0100 CRm 111 11111' =>
         System (MoveImmediateProcState (PSTATEField_DAIFClr, CRm))

      ------------------------
      -- MemoryBarrier
      ------------------------
      case '11010101000000110011 CRm 1 11 11111' => Unallocated
      case '11010101000000110011 CRm 1 opc 11111' =>
      {
         domain = [CRm<3:2>];
         domain, types =
            match CRm<1:0>
            {
               case 0b00 => MBReqDomain_FullSystem, MBReqTypes_All
               case 0b01 => domain, MBReqTypes_Reads
               case 0b10 => domain, MBReqTypes_Writes
               case 0b11 => domain, MBReqTypes_All
            };
         MemoryBarrier ([opc], domain, types)
      }

      ------------------------
      -- Breakpoint
      ------------------------
      case '11010100001 imm16 00000' => Debug (Breakpoint (imm16))

      ------------------------
      -- Hint
      ------------------------
      case '11010101000000110010 CRm op2 11111' =>
      {
         op = if CRm == 0 and op2 <+ 6 then [op2] else SystemHintOp_NOP;
         Hint (op)
      }

      ------------------------
      -- ClearExclusive
      ------------------------
      case '11010101000000110011 CRm 01011111' => ClearExclusive (CRm)

      ------------------------
      -- DebugRestore
      ------------------------
      case '11010110101111110000001111100000' => Debug (DebugRestore)

      ------------------------
      -- DebugSwith
      ------------------------
      case '11010100101 imm16 000 LL' => Debug (DebugSwitch (LL))

      ------------------------
      -- Halt
      ------------------------
      case '11010100010 imm16 00000' => Debug (Halt (imm16))

      ------------------------
      -- ExceptionReturn
      ------------------------
      case '11010110100111110000001111100000' => System (ExceptionReturn)

      ------------------------
      -- SupervisorCall
      -- HyperviorCall
      -- SecureMonitorCall
      ------------------------
      case '11010100000 imm16 00001' => System (SupervisorCall (imm16))
      case '11010100000 imm16 00010' => System (HypervisorCall (imm16))
      case '11010100000 imm16 00011' => System (SecureMonitorCall (imm16))

      ------------------------
      -- System
      ------------------------
      case '1101010100 L 01 op1 CRn CRm op2 Rt' =>
         System (SystemInstruction (op1, op2, CRn, CRm, [L], Rt))

      ------------------------
      -- Unallocated
      ------------------------
      case _ => Unallocated
   }

clear patterns

-----------------------------------------------------------------------
-- Next state ---------------------------------------------------------
-----------------------------------------------------------------------

word Fetch = [Mem (C.PC, 4, AccType_IFETCH)]

unit Next =
{
   C.branch_hint <- None;
   Run (Decode (Fetch));
   when not (IsSome (C.branch_hint)) do C.PC <- C.PC + 4
}
