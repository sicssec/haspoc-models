-----------------------------------------------------------------------
-- This a definition of the ARMv8 virtualization extensions for      --
-- hypervisors and the stage 2 address translation                   --
-- Author: Christoph Baumann, KTH CSC Stockholm			     --
-----------------------------------------------------------------------


-- Notes:
-- 2014-11-05	factoring out into separate file


{-

val () = Runtime.LoadF "v8-base.spec, v8-mem.spec, v8-mmu.spec, v8-virt.spec";

-}


---------------------------
-- Concatenated Page Tables
---------------------------

nat n_for_SL0 = 
{
    var tsz = [MMU.VTCR.T0SZ]::nat;

    when (tsz < 16 and MMU.ISW.IMPL_VT0SZ_LESS_THAN_IS_16_FOR_SL0) do tsz <- 16; -- min 16

    return VAlen(tsz)
}


-- faulty SL0 settings connected to implemented physical address size, and VTCR.T0SZ
bool SL0fault (b :: nat) = 
{
    sl0 = [MMU.VTCR.SL0] :: nat;
    n = n_for_SL0;
    impn = ps2len(MMU.IDMM.PAR<2:0>, 48);
    min_level, margin = match b
    {
	case 9  => 2, 4
	case 11 => 1, 4
	case _  => 1, 1
    };
    border_2 = (min_level + 2)*b + b+3 + margin;
    not_supported = sl0 > 2 or sl0 == 2 and impn <= border_2;
    bits_resolved = (min_level + sl0)*b + b+3;
    size_mismatch = n > bits_resolved + 4 or n <= bits_resolved - b;
    return not_supported or size_mismatch
}

-- when we should concatenate first level page tables, assumes that n is not to big, would cause translation fault before anyway
bool concatenated (sl0 :: nat, n :: nat, b :: nat) =
{
    min_level = if b==9 then 2 else 1;
    return n > (min_level + sl0)*b + b+3
}


-------------------------
-- Stage 2 specific stuff
-------------------------

bool Disabled_ST2 = !(MMU.HCR.DC or MMU.HCR.VM)

ADR PTO_ST2 (w::Walk) = return page_table_origin(nflb(w.n,w.br,w.conc),MMU.VTTBR,MMU.ISW.IMPL_TTBR_MISALIGNED_TREAT_AS_ZERO)

bits(3) PS_ST2 = if MMU.VTCR.PS >+ MMU.IDMM.PAR<2:0> then MMU.IDMM.PAR<2:0> else MMU.VTCR.PS


nat TSZ_ST2 = 
{
    var tsz = [MMU.VTCR.T0SZ] :: nat;

    when (tsz < 16 and !MMU.ISW.IMPL_VT0SZ_LESS_THAN_16_FAULT) do tsz <- 16; -- min 16
    return tsz
}

-- TG field defining translation granule size
bits(2) TG_ST2 = 
{
    tg = MMU.VTCR.TG0;
    
    return (if TGImplemented(MMU.IDMM,tg) then tg else MMU.ISW.IMPL_VTCR_TG_DEFAULT)
}

MemoryAttributes VTCRtoAttr(acctype :: AccType, el :: nat) = 
{
    sh, orgn, irgn = MMU.VTCR.SH0, MMU.VTCR.ORGN0, MMU.VTCR.IRGN0;

    CD = (el < 2) and match acctype {
	case AccType_IFETCH => MMU.HCR.ID 
	case _ => MMU.HCR.CD
    };

    var attr::MemoryAttributes;
    attr.outer <- ShortConvertAttrsHints(orgn, !CD);
    attr.inner <- ShortConvertAttrsHints(irgn, !CD);
    attr.shareable <- sh<1>;
    attr.outershareable <- sh == '10';
    attr.typ <- MemType_Normal;

    return attr
}

Walk InitWalk_ST2(va :: VA, acctype :: AccType, wr :: bool, el ::  nat) = 
{
   var w = init_walk(va, acctype, wr, el, 2, TG_ST2, PS_ST2, TSZ_ST2, VTCRtoAttr(acctype, el), PAlen, false, true, false);
   w.conc <- concatenated([MMU.VTCR.SL0], w.n, w.br);
   return w
}

-- mapping IPA when ST2 address translation disabled
Walk FlatMap_ST2 (w :: Walk) = 
{
    var walk = w;

    -- set attributes so that they do not affect stage 1 memory type, WB cacheable, non-shareable
    walk.attr.typ <- MemType_Normal;
    walk.attr.inner <- ShortConvertAttrsHints('11', true);
    walk.attr.outer <- ShortConvertAttrsHints('11', true);
    walk.attr.shareable <- false;
    walk.attr.outershareable <- false;

    walk.adr <- w.va<47:0>;
    walk.c <- true;

    return walk
}

-- combining stage 1 and 2 cache attributes
MemAttrHints combine_cacheability (st1 :: MemAttrHints, st2 :: MemAttrHints) = 
{
    var attr;

    attr.attrs <- st1.attrs && st2.attrs;
    attr.hints <- st1.hints;
    attr.transient <- st1.transient;
    
    return attr
}

-- combining stage 1 and 2 memory attributes
MemoryAttributes combine_attributes (st1 :: MemoryAttributes, st2 :: MemoryAttributes) = 
{
    var attr;

    attr <- match st1.typ
    {
	case MemType_Device => st1
	case MemType_Normal => st2
    };

    when (st1.typ == MemType_Device and st1.typ  == st2.typ) do
    {
	attr.device <- match st1.device, st2.device
	{
	    case DeviceType_nGnRnE, _ or _ , DeviceType_nGnRnE => DeviceType_nGnRnE
	    case DeviceType_nGnRE,  _ or _ , DeviceType_nGnRE  => DeviceType_nGnRE
	    case DeviceType_nGRE,   _ or _ , DeviceType_nGRE   => DeviceType_nGRE
	    case DeviceType_GRE, DeviceType_GRE                => DeviceType_GRE
	}
    };

    when (st1.typ == MemType_Normal and st1.typ  == st2.typ) do
    {
	attr.inner <- combine_cacheability(st1.inner, st2.inner);
	attr.outer <- combine_cacheability(st1.outer, st2.outer);
	attr.shareable <- st1.shareable or st2.shareable;
	attr.outershareable <- st1.outershareable or st2.outershareable
    };
    
    return attr
}

-- combine descriptors into new ST1 descriptor, only called if st1 is non-faulty
AddressDescriptor combine_descriptors ( st1 :: AddressDescriptor, st2 :: AddressDescriptor ) = 
{
    var desc = st2;
    when !IsFault(st2) do {
	desc.memattrs <- combine_attributes(st1.memattrs, st2.memattrs);
	-- reset to ST1 NoFault, needed to identify memory requests in scheduling
	desc.fault.secondstage <- false
    };
    return desc
}

---------------------
-- Atomic transitions
---------------------

-- Take current descriptor from ST1 and perform walk init
unit MMU_Init_ST2 (PTW :: bool) = 
{
    when (MMU.ST2 != wait) do #MMU_SCHED_FAULT;

    va = [MMU.DESC1.paddress]`64; --`
    write = MMU.W1.wr;
    acctype = if PTW then AccType_PTW else MMU.W1.acctype;
    EL = MMU.W1.el;

    MMU.W2 <- InitWalk_ST2(va, acctype, write, EL);
    MMU.F2 <- NoFaultfromWalk(MMU.W2);

    MMU.ST2 <- init
}


-- Flat map transition when MMU stage 2 disabled
unit MMU_FlatMap_ST2 () = 
{
    when (MMU.ST2 != init) do #MMU_SCHED_FAULT;

    -- modify relevant parameters
    MMU.W2.n <- 48;
    MMU.W2.m <- PAlen;
    MMU.W2.l <- 0;

    -- flat map, check if input address size > implemented physical address
    MMU.W2 <- FlatMap_ST2(MMU.W2);
    MMU.F2 <- check_address_size(NoFaultfromWalk(MMU.W2), MMU.W2.va, PAlen);

    MMU.DESC2 <- ADfromResult(MMU.W2,MMU.F2);

    MMU.ST2 <- final
}

-- preparation of initial VTTBR lookup / SL0 checking
unit  MMU_StartWalk_ST2 () = 
{
    when (MMU.ST2 != init) do #MMU_SCHED_FAULT;

    (w,f) = prepare_initial_lookup(MMU.W2, PTO_ST2(MMU.W2), SL0fault(MMU.W2.br), true, MMU.ISW.IMPL_VT0SZ_LESS_THAN_16_FAULT, false);
    MMU.W2 <- w;
    MMU.F2 <- f;
    if (f.typ == Fault_None) then
    {
	MMU.DESC2 <- ADfromWalk(MMU.W2);
	MMU.ST2 <- mem
    }
    else 
    {
	MMU.DESC2 <- ADfromResult(MMU.W2,MMU.F2);

	MMU.ST2 <- final
    }
}

-- request descriptor from memory
MEM_REQUEST MMU_FetchDescriptor_ST2 () = 
{
    when (MMU.ST2 != mem) do #MMU_SCHED_FAULT;

    var req :: MEM_REQUEST;

    req.va <- MMU.W2.va;
    req.write <- false;
    req.desc <- MMU.DESC2;
    req.bytesize <- 8;
    req.acctype <- AccType_PTW;
    req.ns <- true;
    
    MMU.ST2 <- walk;

    return req
}

unit MMU_ExtendWalk_ST2 (r :: MEM_REQUEST) = 
{
    when (MMU.ST2 != walk) do #MMU_SCHED_FAULT;

    MMU.F2 <- r.desc.fault;
    when (MMU.F2.typ == Fault_None) do {
	desc = cast_desc([r.data]);
	(w, f) = walk_extend(MMU.W2, desc);
	MMU.W2 <- w;
	MMU.F2 <- f;
	when (f.typ == Fault_None and !w.c) do {
   	    MMU.DESC2 <- ADfromWalk(w);
	    MMU.ST2 <- mem
	};
	when (f.typ == Fault_None and w.c) do {
	    MMU.F2 <- check_walk_result(w, 8, MMU.HCR.PTW)
	}
    };
    when (MMU.F2.typ != Fault_None or MMU.W2.c) do {
	MMU.DESC2 <- ADfromResult(MMU.W2,MMU.F2);
	MMU.ST2 <- final
    }
}

(MEM_REQUEST option) MMU_Complete_ST2 () = 
{

    MMU.MEMREQ.desc <- combine_descriptors(MMU.DESC1, MMU.DESC2);

    MMU.ST2 <- wait;
    
    var res = Some(MMU.MEMREQ);
    when MMU.MEMREQ.desc.fault.typ != Fault_None do {
	MMU.ST2RPL <- res;
	res <- None};	
    
    return res
}


---------------------------------
-- MMU automaton for ST1 and ST2
---------------------------------


-- automaton for Stage 2, does not directly reply to core, either sends out final memory request or informs ST1 of fault
(MEM_REQUEST option) * bool MMU_ST2_sched (lookup :: MEM_REQUEST option, omem :: MEM_REQUEST option) = 
{
    var l;
    when IsSome(lookup) do {
	lu = ValOf(lookup);
	l <- lu
    };
    var out_mem = None;
    var consumed = false;

    when IsSome(omem) do MMU.MEMREQ <- ValOf(omem);

    PTW = (MMU.ST1 == walk);
    
    when !(MMU.ST2 == walk and lookup == None) do
    {
	match MMU.ST2
	{
	    case wait => MMU_Init_ST2(PTW)
	    case init => if Disabled_ST2 then MMU_FlatMap_ST2() else MMU_StartWalk_ST2()
	    case mem  => out_mem <- Some(MMU_FetchDescriptor_ST2())
	    case walk => {MMU_ExtendWalk_ST2(l); consumed <- true}
	    case final => out_mem <- MMU_Complete_ST2()
	}
    };

    return (out_mem, consumed)
}
   

-- combined automaton
(MEM_REQUEST option) * (MEM_REQUEST option) * bool MMU_sched (req :: MMU_REQUEST option, lookup :: MEM_REQUEST option) = 
{
    var out_mem = None;
    var out_mmu = None;
    var consumed1 = false;
    
    -- only make another step of ST1 when ST2 is not busy
    when (MMU.ST2 == wait) do
    {
	l = if IsSome(MMU.ST2RPL) then MMU.ST2RPL else lookup; 
	ommu, omem, cons = MMU_ST1_sched(req,l);
	out_mem <- omem;
	out_mmu <- ommu;
	consumed1 <- cons and !(IsSome(MMU.ST2RPL));
	MMU.ST2RPL <- None
    };

    
    ns10 = (MMU.CURR_REQ.EL < 2 and MMU.CURR_REQ.ns);
    
    -- only step if ST1 is loading a descriptor or has the IPA result, only step in Non-Secure EL1/0
    var step2 = ns10 and ((MMU.ST2 != wait) or IsSome(out_mem));
    var consumed2 = false;
    
    -- if ST2 is stepped, ST1 may have sent a request to memory in this step, out_mem is overwritten but ST2 saves the original message
    when step2 do
    {
	omem, cons = MMU_ST2_sched(lookup, out_mem);
	out_mem <- omem;
	when (MMU.ST1 == wait and IsSome(MMU.ST2RPL)) do {
	    out_mmu <- MMU.ST2RPL;
	    MMU.ST2RPL <- None};
	consumed2 <- cons
    };


    return (out_mmu, out_mem, consumed1 or consumed2)
}


   

-- MMU interface
(MMU_REQUEST option) * (MEM_REQUEST option) * (MEM_REQUEST option) * (MEM_REQUEST option) mmu_interface (o :: MMU_ORACLE) = 
{
    icore, imem = o(MMU);
    var in_core = icore;
    icorer, icoreR = get_mmureq(icore);
    var in_mem = imem;

    var out_core = None;
    var out_mem = None;
    var consumed = false;

    when !(MMU.S == mmu_wait and icore == None) do {
	o_mmu, o_mem, cons = MMU_sched(icore, imem);
	out_core <- o_mmu;
	out_mem <- o_mem;
	consumed <- cons;
	
	match MMU.S {
	    case mmu_wait => {
		    MMU.CURR_REQ <- icorer;
		    MMU.S <- mmu_trans;
		    in_mem <- None -- nothing consumed from memory input
		    }
	    case mmu_trans => {
		    when (MMU.ST1 == wait and MMU.ST2 == wait) do MMU.S <- mmu_wait;
		    in_core <- None -- nothing consumed from core input
		    }
	}
    };

    -- no messages consumed at all?
    when (!consumed) do {in_core <- None; in_mem <- None};
 
    return (in_core, in_mem, out_core, out_mem)
}
