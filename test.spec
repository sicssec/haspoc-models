record foo {
    bar :: nat
}

declare foobar :: nat -> (bool * foo)

foo constr(bar :: nat) = {
    var rec :: foo;
    rec.bar <- bar;
    return rec
}
    

construct T { bla }

nat f (x :: nat) = {var bla = x; return bla}


